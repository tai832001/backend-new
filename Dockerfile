FROM openjdk:latest
COPY target/mfdn-project-server-0.0.1-SNAPSHOT.jar mfdnapp
ENTRYPOINT ["java","-jar","/mfdnapp"]