package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.NotificationAllDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Notification;
import com.example.demo.service.NotificationService;
import com.example.demo.service.OrderPitchService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/noti")
public class NotificationController {
	@Autowired
	private NotificationService notificationService;

	@Autowired
	private UserService service;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Autowired
	private OrderPitchService orderPitchService;

	@MessageMapping("/all/notification")
	public void sendMessToRoom(@Payload NotificationAllDTO allDTO) {
		Account account = service.findUserByUsername(allDTO.getSender());
		Account receive = service.findUserByUsername(allDTO.getReceive());
		if (allDTO.getStatus() == 2) {
			Notification notification2 = notificationService.getNotiMess(account.getiDAccount(),
					receive.getiDAccount());
			if (notification2 != null) {
				notificationService.deleteNoti(notification2);
			}
		}
		Notification notification = notificationService.save(new Notification(allDTO.getNote(), account, receive,
				allDTO.getStatus(), allDTO.isActive(), allDTO.isFlag(), allDTO.getDate(), allDTO.getTime()));
		String destination = "/topic/" + receive.getUsername() + "notification";
		messagingTemplate.convertAndSend(destination, notification);
	}

	@GetMapping("/listallnotibyaccountid")
	public Map<String, Object> listNoti(@RequestParam("page") int page) {
		Account account = service.getCurrentUser();
		return notificationService.listNoti(page, account.getiDAccount());
	}

	@GetMapping("/getnumbernoti")
	public int getNotiNumber() {
		Account account = service.getCurrentUser();
		return notificationService.getNumNoti(account.getiDAccount());
	}

	@GetMapping("/getnamepitchnoti")
	public String getNamePitchNoti(@RequestParam("id") String id) {
		return orderPitchService.getNamePitchById(id);
	}

	@GetMapping("/getidactive")
	public long getidnotiactive(@RequestParam("id") long id) {
		return notificationService.getByID(id);
	}

}
