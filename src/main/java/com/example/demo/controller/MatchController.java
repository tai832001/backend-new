package com.example.demo.controller;

import java.sql.Time;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.UserReponsitory;
import com.example.demo.dto.MatchDTO;
import com.example.demo.dto.MatchHomeDTO;
import com.example.demo.dto.NotificationDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.ChatMess;
import com.example.demo.entity.Match;
import com.example.demo.entity.Message;
import com.example.demo.entity.PitchDetail;
import com.example.demo.service.MatchService;
import com.example.demo.service.PitchDetailService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/match")
public class MatchController {

	@Autowired
	private MatchService matchService;

	@Autowired
	private PitchDetailService detailService;

	@Autowired
	private UserService userService;
	
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

	@PostMapping("/create")
	public Match createMatch(@RequestBody Match match) {
		PitchDetail pitchDetail = detailService.getPitchDetailByID(match.getiDPitchDetail());
		return matchService.saveMatch(new Match(match.getiDMatchOwner(), match.getDateStart(), null, null, null, false,
				match.getiDPitchDetail(), match.getDateCreated(), match.getDescription(), true,
				userService.getCurrentUser(), null, pitchDetail, match.getTimeRange()));
	}

	@GetMapping("/getall")
	public List<MatchDTO> getAll() {
		return matchService.listMatch(matchService.getAll());
	}

	@GetMapping("/getallbypage")
	public List<MatchDTO> getAllByPage(@RequestParam("page") int page) {
		return matchService.listMatch(matchService.listMatchPyPage(page).getContent());
	}
	
	@GetMapping("/getallbypagesrearch")
	public List<MatchDTO> getAllByPageSearch(@RequestParam("page") int page, @RequestParam("search") String search) {
		System.out.println(search);
		return matchService.listMatch(matchService.listMatchPyPageSearch(page, search).getContent());
	}
	
	@GetMapping("/getnumberofpage")
	public int getNumberOfPage() {
		return matchService.numberOfPage();
	}
	
	@MessageMapping("/notification")
    public void sendMessToRoom(@Payload NotificationDTO chatMess) {
        System.out.println(chatMess);
        Account account = userService.findUserByUsername(chatMess.getSender());
        Match match = matchService.findById(chatMess.getIdmatch());
        match.setAccountOpponent(account);
        match.setiDOpponent(account.getiDAccount()); 
        match.setTimeStart(chatMess.getContent());
        matchService.saveMatch(match);
        String destination = "/topic/" + chatMess.getRoom()+"match";
        messagingTemplate.convertAndSend(destination, chatMess);
    }
	
	@GetMapping("/findbyid")
	public Match find(@RequestParam("id") String id) {
		return matchService.findById(id);
	}
	
	@PostMapping("/editmatch")
	public Match editMatch(@RequestBody Match match) {
		return matchService.saveMatch(match);
	}
	
	
	@GetMapping("/accept")
	public boolean acceptMatch(@RequestParam("idm") String idm) {
		Match match = matchService.findById(idm);
		match.setPitch(true);
		try {
			matchService.saveMatch(match);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	@GetMapping("/reject")
	public boolean rejectMatch(@RequestParam("idm") String idm) {
		Match match = matchService.findById(idm);
		match.setiDOpponent(null);
		match.setAccountOpponent(null);
		match.setTimeStart(null);
		try {
			matchService.saveMatch(match);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	@DeleteMapping("/delete/{id}")
	public boolean deleteMatch(@PathVariable("id") String id) {
		return matchService.deleteMatch(id);
	}
	
	@GetMapping("/getallhomematch")
	public List<MatchHomeDTO> getAllHomeMatch(@RequestParam("username") String username) {
		return matchService.listHomeMatch(matchService.homePageMatch(username).getContent());
	}
	
	@GetMapping("/getmymatch")
	public List<MatchHomeDTO> getMyMatch(@RequestParam("page") int page) {
		Account account = userService.getCurrentUser();
		return matchService.listMyMatch(matchService.myPageMatch(page ,account.getUsername()).getContent());
	}
	
}
