package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.DateCheckDTO;
import com.example.demo.dto.DistrictDTO;
import com.example.demo.dto.PitchDTO;
import com.example.demo.dto.PitchDetailDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.AuthenRegister;
import com.example.demo.entity.DataUser;
import com.example.demo.entity.LoginResponse;
import com.example.demo.entity.OrderPrice;
import com.example.demo.entity.OwnData;
import com.example.demo.service.AuthenticationService;
import com.example.demo.service.DataUserService;
import com.example.demo.service.DistrictService;
import com.example.demo.service.OrderPriceService;
import com.example.demo.service.PitchDetailService;
import com.example.demo.service.PitchService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

	@Autowired
	private AuthenticationService authenticationService;

	@Autowired
	private UserService userService;

	@Autowired
	private DataUserService dataUserService;
	
	@Autowired
	PitchService pitchService;
	

	@Autowired
	DistrictService districtService;
	
	 @Autowired
	    PitchDetailService pitchDetailService;
	
	@Autowired
	private OrderPriceService orderPriceService;

	@PostMapping("/register")
	public Account registerUser(@RequestBody OwnData register) {
		System.out.println(register);
		try {
			dataUserService.saveUser(new DataUser(register.getUsername(), register.getPassword(), register.getEmail()));
			return authenticationService.registerUser(register.getUsername(), register.getPassword(),
					register.getFirstname() + " " + register.getLastname(), register.getPhone(), register.getAddress(),
					register.getBirthday(), register.getEmail());
		} catch (Exception e) {
			return null;
		}

	}

	@PostMapping("/loginGoogle")
	public LoginResponse loginUserGoogle(@RequestBody OwnData register) {
		DataUser dataUser = dataUserService.findUserByEmail(register.getEmail());
		if (dataUser != null) {
			return authenticationService.loginUser(dataUser.getUsername(), dataUser.getPassword());
		} else {
			dataUserService.saveUser(new DataUser(register.getUsername(), register.getPassword(), register.getEmail()));
			Account account = authenticationService.registerUserGoogle(register.getUsername(), register.getPassword(),
					register.getFirstname() + " " + register.getLastname(), register.getPhone(), register.getAddress(),
					register.getBirthday(), register.getEmail(), true);
			return authenticationService.loginUser(account.getUsername(), register.getPassword());
		}
	}

	@PostMapping("/login")
	public LoginResponse loginUser(@RequestBody AuthenRegister login) {
		System.out.println(login.getUsername() + login.getPassword());
		return authenticationService.loginUser(login.getUsername(), login.getPassword());
	}

	@PostMapping("/check")
	public int checkTK(@RequestBody AuthenRegister login) {
		return userService.validateUser(login.getUsername(), login.getPassword());
	}

	@GetMapping("/checkname")
	public Account findUser(@RequestParam(name = "username") String name) {

		return userService.findUserByUsername(name);
	}

	@GetMapping("/checkemail")
	public Account findUsermail(@RequestParam(name = "email") String email) {

		return userService.findUserByEmail(email);
	}

	@GetMapping("/checkphone")
	public Account findUserPhone(@RequestParam(name = "phone") String phone) {

		return userService.findUserByPhone(phone);
	}

	@PostMapping("/send")
	public int sendMail(@RequestParam(name = "email") String email) {
		System.out.println(email);
		int random = authenticationService.generateRandomSixDigitNumber();
		return authenticationService.sendMail(email, random);
	}

	@PostMapping("/forgotpassword")
	public int forgotPass(@RequestBody Account account) {
		return authenticationService.updatePassword(account.getEmail(), account.getPassword());
	}
	 @GetMapping("/getallhome")
	    public List<DistrictDTO> getAllDistrict() {
	        List<DistrictDTO> list = districtService.listDTO(districtService.getAllDistrictHome());
	        for (DistrictDTO district : list) {
	            System.out.println(district);
	        }
	        return list;
	    }
	 @GetMapping("/getbydistrictdistinct")
		public List<PitchDTO> getAllByDistrict(@RequestParam("id") String idDistrict) {
			return pitchService.findPitchByDistrict(idDistrict);
		}
	 @PostMapping("/orderprice/get")
		public List<OrderPrice> getAllPrice(@RequestBody DateCheckDTO dateCheckDTO) {
			return orderPriceService.getOrder(dateCheckDTO.getDatestart(), dateCheckDTO.getDateend());
		}
	 
	 @GetMapping("/getByPitchIDdtoDistinct")
	    public List<PitchDetailDTO> getByPitchDTODistinct(@RequestParam("idp") String idp) {
	       return pitchDetailService.getPitchDetailByPitchIdDTODistinct(idp);
	    }
	    
}
