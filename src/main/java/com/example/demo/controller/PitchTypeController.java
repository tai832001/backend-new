package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.PitchType;
import com.example.demo.service.PitchTypeService;

@RestController
@RequestMapping("/pitchtype")
public class PitchTypeController {

    @Autowired
    private PitchTypeService pitchTypeService;

    @GetMapping("/getall")

    public List<PitchType> getAll() {
        return pitchTypeService.getAll();
    }
}
