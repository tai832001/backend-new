package com.example.demo.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.PitchDetailDTO;
import com.example.demo.entity.PitchDetail;
import com.example.demo.entity.PitchType;
import com.example.demo.service.PitchDetailService;
import com.example.demo.service.PitchTypeService;

@RestController
public class PitchDetailController {

    @Autowired
    PitchDetailService pitchDetailService;

    @Autowired
    PitchTypeService pitchTypeService;

    @PostMapping("/pitchDetail/create")
    public PitchDetail create(@RequestBody PitchDetail pitchDetail) {
        PitchDetail savedPitchDetail = pitchDetailService.save(pitchDetail);
        PitchType pType = pitchTypeService.findByID(savedPitchDetail.getiDPitchType());
        savedPitchDetail.setPitchType(pType);
        return savedPitchDetail;

    }

    
    @GetMapping("/pitchDetail/getByPitchID")
    public List<PitchDetail> getByPitch(@RequestParam("idp") String idp) {
       return pitchDetailService.getPitchDetailByPitchId(idp);
    }
    
    @GetMapping("/pitchDetail/getByPitchDetailId")
    public PitchDetail getByPitchDetail(@RequestParam("idp") String idp) {
       return pitchDetailService.getPitchDetailByID(idp);
    }
    
    @GetMapping("/pitchDetail/getByPitchIDdto")
    public List<PitchDetailDTO> getByPitchDTO(@RequestParam("idp") String idp) {
       return pitchDetailService.getPitchDetailByPitchIdDTO(idp);
    }
    
    @GetMapping("/pitchDetail/getpitchdetail")
    public List<PitchDetail> getByPitchDTO2() {
       return pitchDetailService.getByIDPD();
    }
    
    @GetMapping("/pitchDetail/getByPitchIDdtoDistinct")
    public List<PitchDetailDTO> getByPitchDTODistinct(@RequestParam("idp") String idp) {
       return pitchDetailService.getPitchDetailByPitchIdDTODistinct(idp);
    }
    
    @GetMapping("/pitchDetail/getByPitchIDDistinct")
    public List<PitchDetail> getByPitchDistinct(@RequestParam("idp") String idp) {
       return pitchDetailService.getPitchDetailByPitchIdDistinct(idp);
    }
}
