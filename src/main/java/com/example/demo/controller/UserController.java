package com.example.demo.controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.GetTeamJoinUserDTO;
import com.example.demo.dto.GetTournamentsByFilterDTO;
import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.dto.MessDTO;
import com.example.demo.dto.PartyDTO;
import com.example.demo.dto.ScheduleOfUserDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.ChatMess;
import com.example.demo.entity.Conversation;
import com.example.demo.entity.District;
import com.example.demo.entity.EditEntity;
import com.example.demo.entity.Friend;
import com.example.demo.entity.FriendRequest;
import com.example.demo.entity.Message;
import com.example.demo.entity.Parti;
import com.example.demo.entity.PassChangeEntity;
import com.example.demo.entity.ResponseMess;
import com.example.demo.entity.Teams;
import com.example.demo.entity.Tournaments;
import com.example.demo.service.ConversationService;
import com.example.demo.service.DistrictService;
import com.example.demo.service.FriendRequestService;
import com.example.demo.service.FriendService;
import com.example.demo.service.MessService;
import com.example.demo.service.PartiServiceImp;
import com.example.demo.service.SendImageToAzureService;
import com.example.demo.service.TeamsService;
import com.example.demo.service.TournamentService;
import com.example.demo.service.TournamentTeamService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private PartiServiceImp partiService;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

	@Autowired
	DistrictService districtService;

	@Autowired
	private MessService messService;

	@Autowired
	private FriendService friendService;

	@Autowired
	private ConversationService conversationService;

	@Autowired
	private SendImageToAzureService azureService;

	@Autowired
	private FriendRequestService friendRequestService;
	
	@Autowired
	private TeamsService teamsService;

	@Autowired
	private JwtDecoder jwtDecoder;
	
	@Autowired
	private TournamentService tournamentService;
	
	@Autowired
	private TournamentTeamService tournamentTeamService;

	@GetMapping("/profile")
	public Account findUser(@RequestParam(name = "username") String name) {
		System.out.println(name);
		return userService.findUserByUsername(name);
	}

	@GetMapping("/finduser")
	public EditEntity findUserByUsername(@RequestParam(name = "username") String name) {
		Account account = userService.findUserByUsername(name);
		EditEntity editEntity = new EditEntity(account.getUsername(), account.getFullname(), account.getEmail(),
				account.getAddress(), account.getPhone(), account.getBirthday());
		return editEntity;
	}

	@PostMapping("/editprofile")
	public boolean editProfile(@RequestBody EditEntity editEntity) {
		Account account = userService.saveEditForUser(editEntity);
		System.out.println(account.getBirthday());
		try {
			userService.saveUser(account);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@PostMapping("/findconver")
	public int findConver(@RequestBody PartyDTO partyDTO) {
		int idConver = partiService.findConverIdByParty(new PartyDTO(partyDTO.getUser(), partyDTO.getFriend()));
		if (idConver == -1) {
			Conversation conversation = conversationService
					.saveConver(new Conversation(partyDTO.getUser() + partyDTO.getFriend(), LocalDateTime.now()));
			saveParty(new Parti(conversation, userService.findUserByUsername(partyDTO.getUser())));
			saveParty(new Parti(conversation, userService.findUserByUsername(partyDTO.getFriend())));
			return conversation.getConId();
		} else {
			return idConver;
		}
	}

	@PostMapping("/saveparty")
	public void saveParty(@RequestBody Parti parti) {
		partiService.saveParty(parti);
	}

	@PostMapping("/updatepass")
	public int update(@RequestBody PassChangeEntity entity) {
		System.out.println(entity);
		Account account = userService.getCurrentUser();
		return userService.changePassword(account.getUsername(), entity.getPassword(), entity.getNewpass());
	}

	@GetMapping("/parti")
	public List<Parti> findParty(@RequestParam(name = "id") String id) {
		System.out.println(id);
		return partiService.listParty(id);
	}

	@GetMapping("/partimess")
	public List<MessDTO> findPartyOrderByMess(@RequestParam(name = "id") String id) {
		System.out.println(id);
		return partiService.listMess(id);
	}

	@GetMapping("/conver")
	public List<Parti> findConver(@RequestParam(name = "id") String id) {
		System.out.println(id);
		return partiService.listUserInConver(id);
	}

	@GetMapping("/mess")
	public List<ResponseMess> findMessByConver(@RequestParam(name = "id") String id,
			@RequestParam(name = "page") int page) {
		return messService.listMess(messService.getMessByConversation(id, page));
	}

	@GetMapping("/numberpage")
	public int findMessPage(@RequestParam(name = "id") String id) {
		return messService.numberOfPage(id);
	}

	@PostMapping("/saveimage")
	public int saveImage(@RequestParam(value = "image", required = false) MultipartFile image,
			@RequestParam("username") String username) {
		try {
			if (image != null) {

				String linkImg = azureService.sendImageToFireBas(image);
				System.out.println(linkImg);
				return userService.updateImage(username, linkImg);
			} else {
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 1;
		}
	}

	@GetMapping("/friend")
	public List<Account> listFriend(@RequestParam("username") String username) {
		return friendService.findFriend(userService.findUserByUsername(username));
	}

	@PostMapping("/checkfriend")
	public boolean checkFriend(@RequestBody PartyDTO partyDTO) {
		return friendService.checkFriend(userService.findUserByUsername(partyDTO.getUser()), partyDTO.getFriend());
	}

	@MessageMapping("/sendMessage")
	public void sendMessToRoom(@Payload ChatMess chatMess) {
		System.out.println(chatMess);
		String destination = "/topic/" + chatMess.getRoom();
		messagingTemplate.convertAndSend(destination, chatMess);
		messService.addMess(new Message(conversationService.findById(chatMess.getRoom()).get(),
				userService.findUserByUsername(chatMess.getSender()), chatMess.getContent(), LocalDateTime.now()));
	}

	@GetMapping("/listfriend")
	public List<Account> listFriendSearch(@RequestParam("fullname") String fullname) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Jwt jwt = (Jwt) authentication.getPrincipal();
		Map<String, Object> claims = jwtDecoder.decode(jwt.getTokenValue()).getClaims();
		if (fullname == "") {
			return null;
		}
		return userService.listAccount(claims.get("sub").toString(), fullname);
	}

	@PostMapping("/addfriend")
	public FriendRequest addFriend(@RequestBody PartyDTO friendRequest) {
		return friendRequestService.addFriend(new FriendRequest(userService.findUserByUsername(friendRequest.getUser()),
				userService.findUserByUsername(friendRequest.getFriend()), LocalDateTime.now()));
	}

	@PostMapping("/checkfriendrequest")
	public boolean checkFriendRequest(@RequestBody PartyDTO partyDTO) {
		return friendRequestService.checkFriendRequest(userService.findUserByUsername(partyDTO.getUser()),
				partyDTO.getFriend());

	}

	@GetMapping("/friendrequest")
	public List<Account> listFriendRequest(@RequestParam("username") String username) {
		return friendRequestService.findFriend(userService.findUserByUsername(username));
	}

	@PostMapping("/friendrequestaccept")
	public boolean checkFriendAccept(@RequestBody PartyDTO partyDTO) {
		List<Account> list = friendRequestService.findFriend(userService.findUserByUsername(partyDTO.getUser()));
		return friendRequestService.checkFriendAccept(list, partyDTO.getFriend());
	}

	@PostMapping("/acceptfriend")
	public Friend acceptFriend(@RequestBody PartyDTO partyDTO) {
		FriendRequest friendRequest = friendRequestService
				.findFriendRequest(userService.findUserByUsername(partyDTO.getUser()), partyDTO.getFriend());
		friendRequestService.rejectFriend(friendRequest);
		return friendService.addFriend(new Friend(userService.findUserByUsername(partyDTO.getUser()),
				userService.findUserByUsername(partyDTO.getFriend()), LocalDateTime.now()));
	}

	@PostMapping("/rejectfriend")
	public boolean rejectFriend(@RequestBody PartyDTO partyDTO) {
		FriendRequest friendRequest = friendRequestService
				.findFriendRequest(userService.findUserByUsername(partyDTO.getUser()), partyDTO.getFriend());
		return friendRequestService.rejectFriend(friendRequest);
	}

	@GetMapping("/findfriendbyuser")
	public List<Account> findFriendByUser(@RequestParam("username") String username) {
		return friendService.findFriendByUser(username);
	}

	@GetMapping("/home")
	public List<Account> home() {
		return userService.getAll();
	}
	 
    @GetMapping("/teams/getbyid")
    public Teams getTeamById() {
        return teamsService.getTeamById(null);
    }
	   
    @GetMapping("/teams/getall")
    public List<Teams> getTeamByUserId() {
        return teamsService.getTeamByUserId();
    }
	    
    @PostMapping("/teams/save")
    public Teams saveTeam(@RequestBody Teams teams) {
      teams.setiDAccount(userService.getCurrentUser().getiDAccount());
        return teamsService.saveTeam(teams);
    }
	 /*
	  * Lấy data của tournament và Team của user hiện tại
	  */
	@GetMapping("/fetchDataInit")
	public Object[] fetchDataInit(@RequestParam("tmId") String tmId) throws Exception {
		return tournamentService.fetchDataInit(tmId);
	}
	
	@PostMapping("/saveTournamentTeam")
	public void saveTournamentTeam(@RequestParam("tmId") String tmId, @RequestParam("idTeam") String idTeam) throws Exception {
		tournamentTeamService.saveTournamentTeamWithId(tmId, idTeam);
	}

	@GetMapping("/getDistrict")
	public List<District> getDistricts() {
		return tournamentService.getDistricts();
	}

	@PostMapping("/getTournamentFilter")
	public List<GetTournamentsByFilterRespDTO> getTournamentFilter(@RequestBody GetTournamentsByFilterDTO dto) {
		return tournamentService.getTournamentFilter(dto.getLocation(), dto.getText(), dto.getPage());
	}
	
	@PostMapping("/checkpassword")
	public int findUserPhone(@RequestParam(name = "password") String password) {
		return userService.checkPassword(password);
	}
	
	@GetMapping("/checkemailnotmy")
	public Account checkEmailNotMy(@RequestParam(name = "email") String email) {
		Account account = userService.getCurrentUser();
		return userService.getByEmailNotMy(email, account.getEmail());
	}
	@GetMapping("/tournament/getTournamentUserJoin")
    public List<GetTournamentUserJoinDTO> getTournamentUserJoin() {
        return tournamentService.getTournamentUserJoin(userService.getCurrentUser().getiDAccount());
    }
	
	@GetMapping("/tournament/cancelTournament")
    public void cancelTournament(@RequestParam("tmtId") String tmtId) {
        tournamentService.cancelTournament(tmtId);
    }
	
	@GetMapping("/tournament/getScheduleOfUser")
    public List<ScheduleOfUserDTO> getScheduleOfUser(@RequestParam("tmId") String tmId) {
        return tournamentService.getScheduleOfUser(tmId);
    }

    @GetMapping("/tournament/getTournamentSchedulesById")
    public List<ListScheduleBasicDTO> getTournamentSchedulesById(@RequestParam("tmId") String tmId) {
    	List<ListScheduleBasicDTO> rs = tournamentService.getTournamentSchedulesById(tmId);
    	
    	return rs;
    }

	@GetMapping("/tournament/getTournamentInfo")
	public Object[] getTournamentInfo(@RequestParam("tmId") String tmId) throws Exception {
		return tournamentService.getTournamentById(tmId);
	}

	@GetMapping("/tournament/getById")
	public Tournaments getTournamentById(@RequestParam("tmId") String tmId) throws Exception {
		return tournamentService.getByID(tmId);
	}

	@GetMapping("/tournament/getTeamJoin")
	public List<GetTeamJoinUserDTO> getTeamJoin(@RequestParam("tmId") String tmId){
		return tournamentService.getTeamJoin(tmId);
	}
}
