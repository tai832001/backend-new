package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Event;
import com.example.demo.service.EventService;

@RestController
@RequestMapping("/outstanding")
public class OutstandingController {
	
	@Autowired
	private EventService eventService;
	
	@GetMapping("/getall")
	public List<Event> test1() {
		return eventService.getOuststanding();
	}

}
