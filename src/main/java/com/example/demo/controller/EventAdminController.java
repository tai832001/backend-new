package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.Event;
import com.example.demo.service.EventService;

@RestController
@RequestMapping("/admin/event")
public class EventAdminController {

	@Autowired
	private EventService eventService;

	@PostMapping("/save")
	public List<Event> test2(@RequestParam(name = "image", required = false) MultipartFile file,
			@RequestParam("nameEvent") String nameEvent, @RequestParam(name = "from", required = false) String from,
			@RequestParam(name = "to", required = false) String to, @RequestParam("address") String address,
			@RequestParam("description") String description, @RequestParam(name = "outstanding") boolean outstanding)
			throws IOException, SQLException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		try {
			LocalDate dateFrom = LocalDate.parse(from, formatter);
			LocalDate dateTo = LocalDate.parse(to, formatter);
			Event newEvent = new Event(nameEvent, dateFrom, dateTo, address, description, true, outstanding);
			eventService.save(newEvent, file);
		} catch (Exception e) {
			return null;
		}
		return eventService.getAll();
	}

	@GetMapping("/getall")
	public List<Event> test1() {
		return eventService.getAll();
	}

	@PostMapping("/delete")
	public List<Event> test3(@RequestParam("idEvent") String idEvent) {
		eventService.deleteEvent(idEvent);
		List<Event> list = eventService.getAll();
		return list;
	}

	@GetMapping("/find")
	public Optional<Event> test4(@RequestParam("idevent") String iDEvent) {
		return eventService.findById(iDEvent);
	}

	@PostMapping("/update")
	public List<Event> test5(@RequestParam(name = "image", required = false) MultipartFile file,
			@RequestParam("nameEvent") String nameEvent, @RequestParam(name = "from", required = false) LocalDate from,
			@RequestParam(name = "to", required = false) LocalDate to, @RequestParam("address") String address,
			@RequestParam("description") String description, @RequestParam("outstanding") boolean outstanding,
			@RequestParam("iDEvent") String id, @RequestParam("imageString") String image)
			throws IOException, SQLException {
		try {
			Event newEvent = new Event(id, nameEvent, from, to, address, description, true, outstanding);
			eventService.updateEvent(newEvent, file, image);
		} catch (Exception e) {
			return null;
		}
		return eventService.getAll();
	}
}
