package com.example.demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.service.PayPalService;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FinishPaymentController {
	
	@Autowired
	private PayPalService palService;
	
	@GetMapping(value = "/result")
	public String completePayment(HttpServletRequest request, 
			@RequestParam(name = "vnp_OrderInfo") String vnp_OrderInfo,
			@RequestParam(name = "vnp_Amount") Integer vnp_Amount,
			@RequestParam(name = "vnp_BankCode", defaultValue = "") String vnp_BankCode,
			@RequestParam(name = "vnp_BankTranNo") String vnp_BankTranNo,
			@RequestParam(name = "vnp_CardType") String vnp_CardType,
			@RequestParam(name = "vnp_PayDate") String vnp_PayDate,
			@RequestParam(name = "vnp_ResponseCode") String vnp_ResponseCode,
			@RequestParam(name = "vnp_TransactionNo") String vnp_TransactionNo,
			@RequestParam(name = "vnp_TxnRef") String vnp_TxnRef
			) {
		Map<String, String> response = new HashMap<>();
		
		String year = vnp_PayDate.substring(0, 4);
		String month = vnp_PayDate.substring(4, 6);
		String date = vnp_PayDate.substring(6, 8);
		String hour = vnp_PayDate.substring(8, 10);
		String minutes = vnp_PayDate.substring(10, 12);
		String second = vnp_PayDate.substring(12, 14);
		
		String timePay = date + "-" + month + "-" + year + " " + hour + ":" + minutes + ":" + second;
		
		response.put("vnp_OrderInfo", vnp_OrderInfo);
		response.put("vnp_Amount", vnp_Amount.toString());
		response.put("vnp_BankCode", vnp_BankCode);
		response.put("vnp_BankTranNo", vnp_BankTranNo);
		response.put("vnp_CardType", vnp_CardType);
		response.put("vnp_PayDate", timePay);
		response.put("vnp_ResponseCode", vnp_ResponseCode);
		response.put("vnp_TransactionNo", vnp_TransactionNo);
		response.put("vnp_TxnRef", vnp_TxnRef);
		
		HttpSession session = request.getSession();
		session.setAttribute("datapayment", response);
		
//		return "redirect:http://103.28.35.104/booking/ref/payment/success";
		return "redirect:http://localhost:3000/booking/ref/payment/success";
	}
}
