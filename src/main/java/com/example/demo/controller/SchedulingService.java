package com.example.demo.controller;

import java.time.LocalDateTime;
import java.time.Year;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import com.example.demo.utils.DateUtils;

@Service
public class SchedulingService {
	
	@Autowired
	private TaskScheduler taskScheduler;
	
	@Autowired
	private DateUtils dateUtil;
	
	private Map<String, ScheduledFuture<?>> jobsMap = new HashMap<>();
	
	public void scheduleATask(String jobId, Date date) {
		String cronExpression = getCronFromDate(date);
		
        if(jobsMap.get(jobId) != null) {
        	jobsMap.get(jobId).cancel(true);
        }
        
        ScheduledFuture<?> scheduledTask = 
        		taskScheduler
        			.schedule(new Runnable() {
						@Override
						public void run() {
							int yearInTask = dateUtil.getYearInDate(date);
							int currentYear = Year.now().getValue();;

							if(yearInTask == currentYear) {
								// xử lí khi đến giờ
								System.out.println("SCHEDULING ACTIVE...");
							}
						}
					}, new CronTrigger(cronExpression, TimeZone.getTimeZone(TimeZone.getDefault().getID())));
        jobsMap.put(jobId, scheduledTask);
    }

    public void removeScheduledTask(String jobId) {
        ScheduledFuture<?> scheduledTask = jobsMap.get(jobId);
        if(scheduledTask != null) {
            scheduledTask.cancel(true);
            jobsMap.remove(jobId);
        }
    }
    
    public String getCronFromDate(Date date) {
    	// giay, phut, gio, ngay trong thang, thang, ngay trong tuan
    	LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), java.time.ZoneId.systemDefault());
    	
    	int second = localDateTime.getSecond();
        int minute = localDateTime.getMinute();
        int hour = localDateTime.getHour();
        int dayOfMonth = localDateTime.getDayOfMonth();
        int month = localDateTime.getMonthValue();
        int dayOfWeek = localDateTime.getDayOfWeek().getValue();
        
        StringBuilder cron = new StringBuilder();
        cron.append(String.format("%02d ", second));
        cron.append(String.format("%02d ", minute));
        cron.append(String.format("%02d ", hour));
        cron.append(String.format("%02d ", dayOfMonth));
        cron.append(String.format("%02d ", month));
        cron.append(String.format("%02d", dayOfWeek));
        
        return cron.toString();
    }
}
