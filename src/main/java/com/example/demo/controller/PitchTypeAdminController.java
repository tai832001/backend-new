package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.excel.IPitchTypeImportExcel;

@RestController
@RequestMapping("/admin/pitchtype")
public class PitchTypeAdminController {

	@Autowired
	IPitchTypeImportExcel pitchTypeImportExcel;
	
	@PostMapping("/save")
	public Map<String, Object> pitchType(@RequestParam("file") MultipartFile file) throws IOException, SQLException {
		Map<String, Object> mapList = pitchTypeImportExcel.readExcel(file);
		return mapList;
	}
}
