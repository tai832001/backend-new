package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Price;
import com.example.demo.service.PriceService;

@RestController
@RequestMapping("price")
public class PriceController {

	@Autowired
	PriceService priceService;

	@GetMapping("getByPitchID")
	public List<Price> getByPitchID() {
		return priceService.getByAccountID();
	}

	@GetMapping("getByPitchIDAndPD")
	public List<Price> getByPitchIDAndPD(@RequestParam("idpd") String idpd, @RequestParam("idp") String idp) {
		return priceService.getByPitchAndPD(idpd, idp);
	}

	@GetMapping("getByPitchDetailID")
	public List<Price> getByPitchDetailID(@RequestParam("id") String id) {
		return priceService.getByPitchDetailID(id);
	}
}
