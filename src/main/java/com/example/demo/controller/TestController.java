// package com.example.demo.controller;
//
// import java.io.IOException;
// import java.sql.SQLException;
// import java.util.HashSet;
// import java.util.Set;
//
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.context.SecurityContextHolder;
// import org.springframework.security.oauth2.jwt.Jwt;
// import org.springframework.security.oauth2.jwt.JwtDecoder;
// import org.springframework.stereotype.Controller;
// import org.springframework.ui.Model;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.ResponseBody;
//
// import com.example.demo.entity.Account;
// import com.example.demo.entity.Role;
// import com.example.demo.excel.IAccountImportExcel;
// import com.example.demo.excel.IDistrictImportExcel;
// import com.example.demo.excel.IPitchImportExcel;
// import com.example.demo.excel.IPitchTypeImportExcel;
// import com.example.demo.excel.ITimeRangeImportExcel;
// import com.example.demo.service.PitchService;
// import com.example.demo.service.UserService;
//
// import jakarta.servlet.http.HttpServletResponse;
//
// @Controller
// public class TestController {
//
// @Autowired
// private IPitchTypeImportExcel e;
//
// @Autowired
// private IPitchImportExcel ex;
//
// @Autowired
// private UserService u;
//
// @Autowired
// private IPitchTypeImportExcel acc;
//
// @Autowired
// private PitchService p;
//
// @Autowired
// private IAccountImportExcel exp;
//
// @Autowired
// private JwtDecoder jwtDecoder;
//
// @RequestMapping("/test")
// @ResponseBody
// public String test(Model model, HttpServletResponse response,
// @RequestParam("role") String roleDownload)
// throws IOException, SQLException {
// Account account = u.findUserByUsername(roleDownload);
// Set<Role> role = new HashSet<>();
// role = (Set<Role>) account.getAuthorities();
// String splitRole = role.toString().substring(1, role.toString().length() -
// 1);
// if ("ADMIN".equals(splitRole)) {
// exp.exportExcel(response);
// return null;
// }
//
// return "ExportAccount";
// }
//
// private String getAuthor() {
// Authentication authentication =
// SecurityContextHolder.getContext().getAuthentication();
// Jwt jwt = (Jwt) authentication.getPrincipal();
// return jwt.getTokenValue();
// }
//
// // @RequestMapping("/test1")
// // @ResponseBody
// // public List<Account> test1(Pageable pageable) throws IOException,
// // SQLException {
// // // System.out.println(e.readExcel("D:/MFDN/Excel/District.xlsx"));
// //// System.out.println(ex.readExcel("D:/MFDN/Excel/Pitch.xlsx"));
// // return u.getAccount(pageable);
// // }
// @Autowired
// private IDistrictImportExcel d;
//
// @Autowired
// private ITimeRangeImportExcel t;
// @RequestMapping("/getData")
// @ResponseBody
// public String testt() throws IOException, SQLException {
//// System.out.println(e.readExcel("D:/MFDN/data/District.xlsx"));
//// System.out.println(d.readExcelFromLocal("D:/MFDN/data/District.xlsx"));
//// System.out.println(t.readExcel("D:/MFDN/data/District.xlsx"));
//
//
// return "hello";
// }
//
// }
