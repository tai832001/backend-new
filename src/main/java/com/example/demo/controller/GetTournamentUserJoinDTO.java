package com.example.demo.controller;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GetTournamentUserJoinDTO {
	private String idTournaments;
    private String statusTeam;
	private LocalDate dateStart;
	private LocalDate dateTo;
	private String nameTouraments;
	private String nameTeam;
	private Boolean flagTournament;
}
