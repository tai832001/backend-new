package com.example.demo.controller;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GetTournamentsByFilterRespDTO {
	private String tourImage;
	private String nameTourament;
	private String idTournament;
	private String pitchName;
	private LocalDate dateStart;
	private LocalDate dateTo;
	private String address;
	private String nameDistrict;
	private Long records;
}
