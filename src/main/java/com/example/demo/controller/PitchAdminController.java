package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.PitchAdminDTO;
import com.example.demo.entity.Account;
import com.example.demo.excel.IDistrictImportExcel;
import com.example.demo.excel.IPitchAdminExcel;
import com.example.demo.excel.IPitchImportExcel;
import com.example.demo.excel.IPitchTypeImportExcel;
import com.example.demo.service.PitchService;

@RestController
@RequestMapping("/admin/pitch")
public class PitchAdminController {

	@Autowired
	private PitchService p;
	
	@Autowired
    private IPitchImportExcel pitchExcel;
	
	@Autowired
    private IPitchAdminExcel pitchAdminExcel;

	@GetMapping("/getall/{iDDictrict}")
	public List<PitchAdminDTO> getAll(@PathVariable("iDDictrict") String iDDistrict) {
		return p.getPitchFromDistrict(iDDistrict);
	}
	
	@PostMapping("/delete")
    public List<PitchAdminDTO> test3(@RequestParam("iDPitch") String iDPitch, @RequestParam("iDDictrict") String iDDictrict) {
		p.deletePitch(iDPitch);
    	return p.getPitchFromDistrict(iDDictrict);
    }
	
    @PostMapping("/save")
    public String test2(@RequestParam("file") MultipartFile file) throws IOException, SQLException {
		return pitchExcel.readExcel(file);
    }
    
    @PostMapping("/saveAdmin")
    public String test4(@RequestParam("file") MultipartFile file) throws IOException, SQLException {
		return pitchAdminExcel.readExcel(file);
    }
}
