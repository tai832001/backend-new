package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.TournamentAdminDTO;
import com.example.demo.entity.Tournaments;
import com.example.demo.service.TournamentService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/tournament")
public class TournamentController {
    @Autowired
    TournamentService tournamentService;
    
    @Autowired
    UserService userService;
    
    @GetMapping("/get")
    public List<Tournaments> getByAccountID() {
        return tournamentService.getByAccountID(userService.getCurrentUser().getiDAccount());
    }
    
    @GetMapping("/getbyid")
    public Tournaments getByID(@RequestParam("id") String id) {
        return tournamentService.getByID(id);
    }

    @GetMapping("/getNotYet")
    public List<TournamentAdminDTO> getTournamentNotYet() {
        return tournamentService.getTournamentNotYet();
    }
}
