package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.StatisticAdminDTO;
import com.example.demo.service.StatisticAdminService;

@RestController
@RequestMapping("/admin/statistic")
public class StatisticAdminController {

	@Autowired
	private StatisticAdminService statisticAdminService;
	
	@RequestMapping("/getHigh")
	public Map<String,Object> getTop3Highest(@RequestParam(name = "month", required = false) String month,
			@RequestParam(name = "year", required = false) String year){
		Map<String, Object> mapList = new HashMap<>();
		List<StatisticAdminDTO> list = statisticAdminService.getTop3Highest(month, year);
		List<StatisticAdminDTO> list2 = statisticAdminService.getTop3Lowest(month, year);
		mapList.put("high", list);
		mapList.put("low", list2);
		return mapList;
	}
}
