package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.DistrictDTO;
import com.example.demo.entity.District;
import com.example.demo.excel.IDistrictImportExcel;
import com.example.demo.service.DistrictService;

@RestController
@RequestMapping("/admin/district")
public class DistrictController {

    @Autowired
    DistrictService districtService;

    @Autowired
    IDistrictImportExcel districtExcel;

    @GetMapping("/getall")
    public List<District> getAll() {
        return districtService.getAll();
    }

    @PostMapping("/save")
    public Map<String, Object> saveDistrict(@RequestParam("file") MultipartFile file)
            throws EncryptedDocumentException, IOException, SQLException {
    	Map<String, Object> mapList = districtExcel.readExcel(file);
        return mapList;
    }

   
}
