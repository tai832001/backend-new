package com.example.demo.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.dto.OrderPitchDTO;
import com.example.demo.dto.RecentOrderDTO;
import com.example.demo.dto.RevenueStatisticsPitchDTO;
import com.example.demo.dto.SaveTournamentSchedule;
import com.example.demo.dto.StatisticBasicDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.District;
import com.example.demo.entity.Pitch;
import com.example.demo.entity.PitchDetail;
import com.example.demo.entity.PitchType;
import com.example.demo.entity.Price;
import com.example.demo.entity.TimeRange;
import com.example.demo.entity.TournamentSchedule;
import com.example.demo.entity.TournamentTeams;
import com.example.demo.entity.Tournaments;
import com.example.demo.service.DistrictService;
import com.example.demo.service.OrderPitchService;
import com.example.demo.service.PitchDetailService;
import com.example.demo.service.PitchService;
import com.example.demo.service.PitchTypeService;
import com.example.demo.service.PriceService;
import com.example.demo.service.SendImageToAzureService;
import com.example.demo.service.TeamsService;
import com.example.demo.service.TimeRangeService;
import com.example.demo.service.TournamentScheduleService;
import com.example.demo.service.TournamentService;
import com.example.demo.service.TournamentTeamService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/owner")
public class OwnerController {

    @Autowired
    TournamentTeamService tournamentTeamService;

    @Autowired
    TournamentScheduleService tournamentScheduleService;

    @Autowired
    PitchDetailService pitchDetailService;

    @Autowired
    PitchService pitchService;

    @Autowired
    UserService userService;

    @Autowired
    PitchTypeService pitchTypeService;

    @Autowired
    DistrictService districtService;

    @Autowired
    TimeRangeService timeRangeService;

    @Autowired
    PriceService priceService;

    @Autowired
    TournamentService tournamentService;
    
    @Autowired
    OrderPitchService orderPitchService;
    
    @Autowired
    SendImageToAzureService azureService;
    
    @Autowired
    TeamsService teamsService;
    

    private final ModelMapper mapper = new ModelMapper();

    @GetMapping("/getTeamInTournament")
    public Object[] getTeamInTournament(@RequestParam("tmId") String id) {
        List<TournamentTeams> tournamentTeams = tournamentTeamService.getTeamInTournament(id);
        List<PitchDetail> pitchDetailsByTournamentId = pitchDetailService.getByTournamentId(id);

        return new Object[] { tournamentTeams, pitchDetailsByTournamentId };
    }

    @PostMapping("/saveTournamentSchedule")
    public TournamentSchedule saveTournamentSchedule(@RequestBody SaveTournamentSchedule dataSend) {
        TournamentSchedule tournamentSchedule = mapper.map(dataSend, TournamentSchedule.class);

        return tournamentScheduleService.save(tournamentSchedule);
    }

    @DeleteMapping("/deleteTournamentScheduleById")
    public void deleteTournamentScheduleById(@RequestParam("tmsId") String tmsId) {
        tournamentScheduleService.deleteTournamentScheduleById(tmsId);
    }

    @GetMapping("/getMatchById")
    public ListScheduleBasicDTO getTournamentScheduleByMatchId(@RequestParam("matchId") String matchId) {
        return tournamentScheduleService.getTournamentScheduleByMatchId(matchId);
    }

    // pitch
    @GetMapping("/pitch/get")
    public List<Pitch> getPitchByAccountID() {
        List<Pitch> listPitchs = new ArrayList<>();
        listPitchs = pitchService.getPitchByCurentUserId(userService.getCurrentUser().getiDAccount());
        return (listPitchs);
    }

 	@PostMapping("/pitch/create")
	public Pitch savePitch(@RequestParam("name") String name,@RequestParam("iDDistrict") String id,@RequestParam("address") String address, @RequestParam("imagepitch") MultipartFile image) {
		String link = azureService.sendImageToFireBas(image);
		Pitch pitch = new Pitch();
		pitch.setName(name);
		pitch.setiDDistrict(id);
		pitch.setAddress(address);
		pitch.setIdOwner(userService.getCurrentUser().getiDAccount());
		pitch.setFlag(true);
		pitch.setPitchImage(link);
		return pitchService.Save(pitch);
	}

 	  @PostMapping("/pitch/update")
 	    public Pitch updatePitch(@RequestParam("name") String name,@RequestParam("iDDistrict") String id,@RequestParam("address") String address,
 	           @RequestParam("iDPitch") String pitchId,@RequestParam("flag") Boolean flag,@RequestParam("pitchImage") String linkimage,
 	          @RequestParam(value = "imagepitch", required = false) MultipartFile image) {
 	          Pitch pitch = new Pitch();
 	        pitch.setName(name);
 	        pitch.setiDDistrict(id);
 	        pitch.setAddress(address);
 	        pitch.setiDPitch(pitchId);
 	        pitch.setFlag(flag);
 	        pitch.setIdOwner(userService.getCurrentUser().getiDAccount());
 	        pitch.setPitchImage(linkimage);
 	        if(image == null) {
 	            return pitchService.Save(pitch);
 	        }else {
 	           String link = azureService.sendImageToFireBas(image);
 	           pitch.setPitchImage(link);
 	            return pitchService.Save(pitch);
 	        }
 	    }
 	  
      @PostMapping("/pitch/delete")
      public Pitch deletePitch(@RequestBody Pitch pitch) {
           return pitchService.Save(pitch);
      }
 	  
    // pitchdetail
    @GetMapping("/pitchDetail/getByPage")
    public Map<String, Object> getPagePitchDetailByPitchId(@RequestParam("id") String id,
            @RequestParam("search") String search, @RequestParam("page") int page) {
        int pageSize = 3;
        Map<String, Object> pitchDetails = pitchDetailService.getPagePitchDetailByPitchId(id, search, page, pageSize);
        return pitchDetails;
    }

    @PostMapping("/pitchDetail/create")
    public PitchDetail create(@RequestBody PitchDetail pitchDetail) {
        PitchDetail savedPitchDetail = pitchDetailService.save(pitchDetail);
        PitchType pType = pitchTypeService.findByID(savedPitchDetail.getiDPitchType());
        savedPitchDetail.setPitchType(pType);
        return savedPitchDetail;

    }

    // pitchtype
    @GetMapping("/pitchtype/getall")
    public List<PitchType> getAllPitchType() {
        return pitchTypeService.getAll();
    }

    // district

    @GetMapping("/district/getall")
    public List<District> getAllDistrict() {
        return districtService.getAll();
    }
    // timerange

    @GetMapping("/time/getall")
    public List<TimeRange> getAllList() {
        return timeRangeService.getAll();
    }
    // price

    @PostMapping("/price/save")
    public List<Price> save(@RequestBody List<Price> list) {
        return priceService.saveAll(list);
    }
    // tournamentteams

    @GetMapping("/tournamentteam/get")
    public Map<String, Object> getByIdTournament(@RequestParam("id") String id, @RequestParam("page") int page) {
        return tournamentTeamService.getByIdTournament(id, page);
    }

    @PostMapping("/tournamentteam/save")
    public TournamentTeams save(@RequestBody TournamentTeams data) {
        return tournamentTeamService.save(data);
    }
    @PostMapping("/tournament/save")
    public Tournaments saveTournament(@RequestParam(value = "file", required = false) MultipartFile image,
            @RequestParam(value = "image", required = false) String linkImage,
            @RequestParam(value = "iDTournaments", required = false) String iDTournaments,
            @RequestParam(value = "iDPitch", required = false) String iDPitch,
            @RequestParam(value = "nameTouraments", required = false) String nameTouraments,
            @RequestParam(value = "quality", required = false) int quality,
            @RequestParam(value = "dateStart", required = false) LocalDate dateStart,
            @RequestParam(value = "dateTo", required = false) LocalDate dateTo,
            @RequestParam(value = "fees", required = false) float fees,
            @RequestParam(value = "description", required = false) String description,
            @RequestParam(value = "limitAge", required = false) int limitAge,
            @RequestParam(value = "flag", required = false) boolean flag) {
        Tournaments tournaments = new Tournaments();
        if(iDTournaments != null) {
            tournaments.setiDTournaments(iDTournaments);
        }
        tournaments.setiDAccount(userService.getCurrentUser().getiDAccount());
        tournaments.setiDPitch(iDPitch);
        tournaments.setNameTouraments(nameTouraments);
        tournaments.setQuality(quality);
        tournaments.setDateStart(dateStart);
        tournaments.setDateTo(dateTo);
        tournaments.setFees(fees);
        tournaments.setDescription(description);
        tournaments.setLimitAge(limitAge);
        tournaments.setFlag(flag);
        if(image == null) {
            tournaments.setImage(linkImage);
        } else {
            String link = azureService.sendImageToFireBas(image);
            tournaments.setImage(link);
        }
        return tournamentService.save(tournaments);
    }
    
    @GetMapping("/tournament/get")
    public List<Tournaments> getByAccountID() {
        return tournamentService.getByAccountID(userService.getCurrentUser().getiDAccount());
    }

	@GetMapping("/getStatisticByPitch")
	public List<RevenueStatisticsPitchDTO> getStatisticByPitch(@RequestParam("year") int year) {
		return pitchService.revenueStatisticsPitch(year);
	}

	@GetMapping("/getRecentOrder")
	public List<RecentOrderDTO> getRecentOrder(@RequestParam("page") Integer page) {
		return pitchService.getRecentOrder(page);
	}

	@GetMapping("/getStatisticBasic")
	public StatisticBasicDTO getStatisticBasic() {
		return pitchService.getStatisticBasic();
	}

    @GetMapping("/pitchDetail/getAll")
    public ResponseEntity<List<PitchDetail>> getByUser() {
        List<PitchDetail> rs = pitchDetailService.getByUser();
        return new ResponseEntity<>(rs, HttpStatus.OK);
    }

	@GetMapping("/orderPitch/getByOwner")
	public List<OrderPitchDTO> getOrderPitchByOwner() {
		Account account = userService.getCurrentUser();

		return orderPitchService.getByUser(account);
	}
	
	@GetMapping("/teams/getallteams")
	public Map<String, Object> getTeamsByTournamentID(@RequestParam("id") String id,@RequestParam("page") int page){
	    return teamsService.getTeamsByTournamentID(id, page);
	}

    @GetMapping("/tournament/getTournamentSchedulesById")
    public List<ListScheduleBasicDTO> getTournamentSchedulesById(@RequestParam("tmId") String tmId) {
    	List<ListScheduleBasicDTO> rs = tournamentService.getTournamentSchedulesById(tmId);
    	
    	return rs;
    }
}
