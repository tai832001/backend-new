package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.DateCheckDTO;
import com.example.demo.dto.NotificationAllDTO;
import com.example.demo.dto.NotificationDTO;
import com.example.demo.dto.OrderPitchDTO;
import com.example.demo.dto.OrderWithPriceDTO;
import com.example.demo.dto.SaveOrderDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Match;
import com.example.demo.entity.OrderPitch;
import com.example.demo.entity.OrderPrice;
import com.example.demo.entity.Price;
import com.example.demo.service.OrderPitchService;
import com.example.demo.service.OrderPriceService;
import com.example.demo.service.PriceService;
import com.example.demo.service.UserService;

@RestController
public class OrderPitchController {

	@Autowired
	OrderPitchService orderPitchService;

	@Autowired
	UserService userService;

	@Autowired
	private PriceService priceService;

	@Autowired
	private OrderPriceService orderPriceService;


	@GetMapping("/orderPitch/getAll")
	public List<OrderPitchDTO> getByUser() {
		Account account = userService.getCurrentUser();

		return orderPitchService.getByUser(account);
	}

	@PostMapping("/orderPitch/addbooking")
	public String addpitch(@RequestBody SaveOrderDTO saveOrderDTO) {
		OrderPitch orderPitch = new OrderPitch();
		Account account = userService.getCurrentUser();
		orderPitch.setAccount(account);
		orderPitch.setiDAccount(account.getiDAccount());
		orderPitch.setDeposit(saveOrderDTO.getDeposit());
		orderPitch.setDateOrder(saveOrderDTO.getDateOrder());
		orderPitch.setFlag(true);
		List<OrderPrice> set = new ArrayList<>();
		int index1 = 0;
		int index = 0;
		for (String id : saveOrderDTO.getListPrice()) {
			Price price = priceService.getByID(id);
			boolean a = orderPitchService.checkOrderPitch(price, saveOrderDTO.getDateStart().get(index1));
			index1++;
			if (a== false) {
				return null;
			}
		}
		for (String id : saveOrderDTO.getListPrice()) {
			Price price = priceService.getByID(id);
			set.add(new OrderPrice(orderPitch, price, saveOrderDTO.getDateStart().get(index)));
			index++;
		}
		orderPitch.setOrderPrices(set);
		OrderPitch orderPitch2 = orderPitchService.save(orderPitch);
		return orderPitch2.getOrderPrices().get(0).getPrice().getPitchDetail().getiDPitch();
	}

	@PostMapping("/orderPitch/get")
	public List<OrderWithPriceDTO> getAll(@RequestBody SaveOrderDTO saveOrderDTO) {
		return orderPitchService.listOrder();
	}

	@PostMapping("/orderPitch/orderprice/get")
	public List<OrderPrice> getAllPrice(@RequestBody DateCheckDTO dateCheckDTO) {
		return orderPriceService.getOrder(dateCheckDTO.getDatestart(), dateCheckDTO.getDateend());
	}
	   @GetMapping("/owner/orderPitch/getPage")
	    public  Map<String, Object> getPageOrderPitchByOwnerID(@RequestParam("page") int page, @RequestParam("pid") String pid) {
	        Account account = userService.getCurrentUser();
	        
	        return orderPitchService.getPageOrderPitchByOwnerID(account.getiDAccount(), pid, page);
	    }
	   
	    @PostMapping("/owner/orderPitch/manageorder")
	    public OrderPitch manageOrder(@RequestBody OrderWithPriceDTO orderPitch) {
	    	OrderPitch orderPitch2 = orderPitchService.find(orderPitch.getIDOrder());
	    	orderPitch2.setStatus(orderPitch.getStatus());
	        return orderPitchService.save(orderPitch2);
	    }
	    @GetMapping("/orderPitch/getbyorderpitchid")
		public OrderWithPriceDTO getByOrderID(@RequestParam("id") String id) {
			return orderPitchService.findDto(id);
		}

	    @PostMapping("/orderPitch/excuse")
		public OrderPitch getByOrderID(@RequestBody OrderWithPriceDTO order) {
	    	OrderPitch orderPitch = new OrderPitch(order.getIDOrder(), order.getIDAccount(), order.getIDPrice(), order.getDeposit(), order.getDateStart(), order.getDateOrder(), 4, order.isFlag(), order.getOrderPrices(), order.getAccount());
			return orderPitchService.save(orderPitch);
		}
	@GetMapping("/orderPitch/history")
	public List<OrderWithPriceDTO> getHistory(){
		Account account = userService.getCurrentUser();
		return  orderPitchService.findHistory(account.getiDAccount());
		
		
	}
}
