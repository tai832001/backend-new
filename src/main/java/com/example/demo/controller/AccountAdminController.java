package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.Account;
import com.example.demo.excel.IAccountImportExcel;
import com.example.demo.service.UserService;

import jakarta.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/admin")
public class AccountAdminController {

	@Autowired
	private UserService userService;

	@Autowired
	private IAccountImportExcel acc;

	@GetMapping("/account")
	public List<Account> test1() {
		return userService.getAccount();
	}

	@PostMapping("/account/save")
	public Map<String, Object> test2(@RequestParam("file") MultipartFile file) throws IOException, SQLException {
		Map<String, Object> mapList = acc.readExcel(file);
		mapList.put("listUpdate", userService.getAccount());
		return mapList;
	}

	@PostMapping("/account/delete/{iDAccount}")
	public List<Account> test3(@PathVariable("iDAccount") String iDAccount) {
		userService.deleteAccount(iDAccount);
		List<Account> list = userService.getAccount();
		return list;
	}
	
}
