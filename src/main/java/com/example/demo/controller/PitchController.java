package com.example.demo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.CreatePitchDTO;
import com.example.demo.dto.PitchDTO;
import com.example.demo.entity.Pitch;
import com.example.demo.service.PitchDetailService;
import com.example.demo.service.PitchService;
import com.example.demo.service.PitchTypeService;
import com.example.demo.service.SendImageToAzureService;
import com.example.demo.service.UserService;

@RestController
@RequestMapping("/pitch")
public class PitchController {
	@Autowired
	PitchService pitchService;

	@Autowired
	PitchDetailService pitchDetailService;

	@Autowired
	PitchTypeService pitchTypeService;

	@Autowired
	UserService userService;
	
	@Autowired
	SendImageToAzureService azureService;

	@GetMapping("/get")
	public List<Pitch> getPitchByAccountID() {
		List<Pitch> listPitchs = new ArrayList<>();
		listPitchs = pitchService.getPitchByCurentUserId(userService.getCurrentUser().getiDAccount());
		return (listPitchs);
	}

	@GetMapping("/getbydistrict")
	public List<Pitch> getPitchByDistrict(@RequestParam("id") String idDistrict) {
		return pitchService.getPitchByDistrict(idDistrict);
	}

	@GetMapping("/getall")
	public List<PitchDTO> getAll() {
		System.out.println("next");
		return pitchService.getAllPitch();
	}

	@GetMapping("/getbydistrictdistinct")
	public List<PitchDTO> getAllByDistrict(@RequestParam("id") String idDistrict) {
		return pitchService.findPitchByDistrict(idDistrict);
	}
}
