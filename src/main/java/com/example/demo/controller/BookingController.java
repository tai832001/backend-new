package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.config.PaypalPaymentIntent;
import com.example.demo.config.PaypalPaymentMethod;
import com.example.demo.dto.PayPalDTO;
import com.example.demo.dto.SendVnPayDTO;
import com.example.demo.dto.VnPayDTO;

import com.example.demo.service.PayPalService;
import com.example.demo.service.VNpayService;
import com.paypal.api.payments.Payment;


import com.paypal.api.payments.Links;
import com.paypal.base.rest.PayPalRESTException;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/payment")
public class BookingController {

	@Autowired
	private VNpayService npayService;
	
	@Autowired
	private PayPalService palService;
	
	@Autowired
    private SimpMessagingTemplate messagingTemplate;

	@PostMapping("/sendprice")
	public VnPayDTO sendUlr(HttpServletRequest request, @RequestBody SendVnPayDTO sendVnPayDTO) {
		String vnpayUrl = npayService.createOrder(sendVnPayDTO.getAmount(), sendVnPayDTO.getOrderInfo());
		VnPayDTO dto = new VnPayDTO("Ok", "Successfully", vnpayUrl);
		return dto;
	}
	
	@PostMapping("/sendpricepaypal")
	public VnPayDTO sendUlrPaypal(HttpServletRequest request, @RequestBody PayPalDTO order) {
		try {
			Payment payment = palService.createPayment(order.getPrice(), order.getCurrency(), PaypalPaymentMethod.paypal, 
					PaypalPaymentIntent.sale, order.getDescription(), "http://103.28.35.104/booking/ref/payment/error",
					"http://localhost:3000/booking/ref/payment/transaction");
			for(Links link:payment.getLinks()) {
				if(link.getRel().equals("approval_url")) {
					VnPayDTO dto = new VnPayDTO("Ok", "Successfully", link.getHref());
					return dto;
				}
			}
		} catch (PayPalRESTException e) {
			e.printStackTrace();
		}
		return null;
	}
	@GetMapping("/paypalinfo")
    public String successPay(@RequestParam("paymentId") String paymentId, @RequestParam("PayerID") String payerId) {
        try {
            Payment payment = palService.executePayment(paymentId, payerId);
            System.out.println(payment.toJSON());
            if (payment.getState().equals("approved")) {
                return payment.toJSON();
            }
        } catch (PayPalRESTException e) {
         System.out.println(e.getMessage());
        }
        return "";
    }
	
	@MessageMapping("/notificationbooking")
    public void sendMessToRoom(@Payload String iDpitch) {
		System.out.println(iDpitch);
        String destination = "/topic/" + iDpitch +"booking";
        messagingTemplate.convertAndSend(destination, iDpitch);
    }
	
}
