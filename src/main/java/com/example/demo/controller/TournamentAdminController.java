package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.TournamentAdminDTO;
import com.example.demo.service.TournamentService;

@RestController
@RequestMapping("/admin/tournament")
public class TournamentAdminController {

	@Autowired
	private TournamentService tournamentService;
	
	@GetMapping("/getAll")
	public List<TournamentAdminDTO> getAll(){
		return tournamentService.getAll();
	}
	
	@PostMapping("/delete/{iDTournament}")
	public List<TournamentAdminDTO> test3(@PathVariable("iDTournament") String iDTournament) {
		tournamentService.deleteTournament(iDTournament);
		List<TournamentAdminDTO> list = tournamentService.getAll();
		return list;
	}
}
