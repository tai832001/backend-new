package com.example.demo.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.dto.StatisticAdminDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Pitch;
import com.example.demo.entity.Role;
import com.example.demo.excel.IAccountImportExcel;
import com.example.demo.excel.IDistrictImportExcel;
import com.example.demo.excel.IPitchAdminExcel;
import com.example.demo.excel.IPitchImportExcel;
import com.example.demo.excel.IPitchTypeImportExcel;
import com.example.demo.excel.ITimeRangeImportExcel;
import com.example.demo.service.PitchService;
import com.example.demo.service.StatisticAdminService;
import com.example.demo.service.UserService;

import jakarta.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/download")
public class DownloadController {

	@Autowired
	private IPitchTypeImportExcel e;

	@Autowired
	private IPitchImportExcel ex;

	@Autowired
	private UserService u;

	@Autowired
	private IPitchTypeImportExcel acc;

	@Autowired
	private PitchService p;

	@Autowired
	private IAccountImportExcel exp;

	@Autowired
	private IPitchImportExcel expPitch;
	
	@Autowired
	private IPitchAdminExcel pitchAdminExcel;

	@Autowired
	private IDistrictImportExcel districtExcel;
	
	@Autowired
	private ITimeRangeImportExcel timeRangeImportExcel;

    @Autowired
    UserService userService;
    
    @Autowired
    PitchService pitchService;
    
	@RequestMapping("/account")
	@ResponseBody
	public String account(Model model, HttpServletResponse response, @RequestParam("role") String roleDownload)
			throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole)) {
			exp.exportExcel(response);
			return null;
		}

		return "ExportAccount";
	}

	@RequestMapping("/pitch")
	@ResponseBody
	public String pitch(HttpServletResponse response, @RequestParam("role") String roleDownload,
			@RequestParam("id") String id) throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole) || "OWNER".equals(splitRole)) {
			expPitch.exportExcel(response, id);
			return null;
		}
		return "ExportPitch";
	}

	@RequestMapping("/district")
	@ResponseBody
	public String district(HttpServletResponse response, @RequestParam("role") String roleDownload)
			throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole)) {
			districtExcel.exportExcel(response);
			return null;
		}

		return "ExportDistrict";
	}

	@RequestMapping("/pitchtype")
	@ResponseBody
	public String pitchType(HttpServletResponse response, @RequestParam("role") String roleDownload)
			throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole)) {
			e.exportExcel(response);
			return null;
		}

		return "ExportPitchType";
	}

	@RequestMapping("/timerange")
	@ResponseBody
	public String timeRange(HttpServletResponse response, @RequestParam("role") String roleDownload)
			throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole)) {
			timeRangeImportExcel.exportExcel(response);
			return null;
		}

		return "ExportPitchType";
	}
	
	@RequestMapping("/pitchAdmin")
	@ResponseBody
	public String pitchAdmin(HttpServletResponse response, @RequestParam("role") String roleDownload,
			@RequestParam("idpitch") String iDPitch) throws IOException, SQLException {
		Account account = u.findUserByUsername(roleDownload);
		Set<Role> role = new HashSet<>();
		role = (Set<Role>) account.getAuthorities();
		String splitRole = role.toString().substring(1, role.toString().length() - 1);
		if ("ADMIN".equals(splitRole) || "OWNER".equals(splitRole)) {
			pitchAdminExcel.exportExcel(response, iDPitch);
			return null;
		}

		return "ExportPitch";
	}


}
