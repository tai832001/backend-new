package com.example.demo.dto;

import java.time.LocalDate;
import java.util.List;

import com.example.demo.entity.Account;
import com.example.demo.entity.OrderPrice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderWithPriceDTO {
	private String iDOrder;
	private String iDAccount;
	private String iDPrice;
	private float deposit;
	private LocalDate dateStart;
	private LocalDate dateOrder;
	private int status;
	private boolean flag;
	private List<OrderPrice> orderPrices;
	private Account account;
}
