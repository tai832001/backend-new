package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaveTournamentSchedule {
	private String idMatch;
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd/MM/yyyy", timezone="EST")
	private LocalDate dateStart;
    private Time timeStart;
    private String idTeam1;
    private String idTeam2;
    private Integer result1;
    private Integer result2;
    private String idTournaments;
    private String idPitchDetail;
}
