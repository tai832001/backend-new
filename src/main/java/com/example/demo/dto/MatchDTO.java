package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import java.util.Set;

import com.example.demo.entity.TimeRange;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MatchDTO {
	private String idMatch;
	private LocalDate dateStart;
	private String username;
	private String description;
	private String fullname;
	private String namePitch;
	private String address;
	private String district;
	private int size;
	private String grassType;
	private Set<TimeRange> timeRange;
	private Time time;
	private String oppe;
	private String nameOppe;
}
