package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class OrderPitchDTO {
	private String iDOrder;
	private LocalDate dateStart;
	private Time timeFrom;
	private Time timeTo;
	private String namePitch;
	private String idPitchDetail;
	private String fullname;
	private int status;
}
