package com.example.demo.dto;

import java.util.List;

import com.example.demo.entity.Account;
import com.example.demo.entity.District;
import com.example.demo.entity.PitchDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PitchDTO {
	private String iDPitch;
	private String name;
	private String address;
	private boolean flag;
	private District district;
	private String img;
	private Account account;
	private List<PitchDetail> listPitchDetail;

}
