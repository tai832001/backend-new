package com.example.demo.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StatisticAdminDTO {
	private String name;
	private String fullname;
	private String iDAccount;
	private String address;
	private String nameDistrict;
	private Double total;
	
	
}
