package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ListScheduleBasicDTO {
	private String idMatch;
	private String nameTouraments;
	private LocalDate dateStart;
	private Time timeStart;
	private String idTeam1;
	private String nameTeam1;
	private String idTeam2;
	private String nameTeam2;
	private Integer result1;
	private Integer result2;
	private String idPitchDetail;
	private String namePitch;
}
