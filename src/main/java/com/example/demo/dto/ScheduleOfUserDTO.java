package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ScheduleOfUserDTO {
	private String idMatch;
	private LocalDate dateStart;
	private Time timeStart;
	private String namePitch;
	private String nameTeam1;
	private String nameTeam2;
	private Integer result1;
	private Integer result2;
}
