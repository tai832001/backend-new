package com.example.demo.dto;

import java.util.ArrayList;
import java.util.List;

public class RevenueStatisticsPitchDTO {
	private String iDPitch;
	private String pitchName;
	private List<RevenueStatisticsPitchDTO1> listDateAndSum;
	public String getiDPitch() {
		return iDPitch;
	}
	public void setiDPitch(String iDPitch) {
		this.iDPitch = iDPitch;
	}
	public String getPitchName() {
		return pitchName;
	}
	public void setPitchName(String pitchName) {
		this.pitchName = pitchName;
	}
	public List<RevenueStatisticsPitchDTO1> getListDateAndSum() {
		if(listDateAndSum == null ) {
			return new ArrayList<>();
		}
		return listDateAndSum;
	}
	public void setListDateAndSum(List<RevenueStatisticsPitchDTO1> listDateAndSum) {
		this.listDateAndSum = listDateAndSum;
	}
	@Override
	public int hashCode() {
		return iDPitch.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == this)
            return true;
        if (!(obj instanceof RevenueStatisticsPitchDTO))
            return false;
        
        RevenueStatisticsPitchDTO other = (RevenueStatisticsPitchDTO) obj;
        
        boolean check = (this.iDPitch == null && other.iDPitch == null)
                || (this.iDPitch != null && this.iDPitch.equals(other.iDPitch));
        
        return check;
	}
}
