package com.example.demo.dto;

import java.time.LocalDate;

import org.springframework.web.multipart.MultipartFile;

public class EventDTO {
	
	private String iDEvent;
	private String nameEvent;
	private LocalDate from;
	private LocalDate to;
	private String address;
	private String description;
	private boolean flag;
	private boolean outstanding;
	private MultipartFile image;
}
