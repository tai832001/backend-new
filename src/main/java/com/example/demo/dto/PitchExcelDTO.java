package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PitchExcelDTO {

	private String iDPitch;
	private String iDPitchDetail;
	private String name;
	private String iDDistrict;
	private String address;
	private String namePitch;
	private String iDPitchType;
	private String iDOwner;
}
