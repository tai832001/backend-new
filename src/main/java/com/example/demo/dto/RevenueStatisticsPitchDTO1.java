package com.example.demo.dto;

import java.sql.Date;

public class RevenueStatisticsPitchDTO1 {
	private Date dateOrder;
	private Double totalPrice;
	private Long numOrder;
	
	public Date getDateOrder() {
		return dateOrder;
	}
	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getNumOrder() {
		return numOrder;
	}
	public void setNumOrder(Long numOrder) {
		this.numOrder = numOrder;
	}
}
