package com.example.demo.dto;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TournamentAdminDTO {

	private String iDTournaments;
	private String nameTouraments;
	private String fullName;
	private String name;
	private LocalDate dateStart;
	private LocalDate dateTo;
	private String image;
	private String description;
	private String status;

}
