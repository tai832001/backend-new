package com.example.demo.dto;

import java.util.List;

import com.example.demo.entity.Pitch;
import com.example.demo.entity.PitchType;
import com.example.demo.entity.Price;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PitchDetailDTO {
	private String iDPitchDetail;
	private String namePitch;
	private boolean flag;
	private Pitch pitch;
	private PitchType pitchType;
	private List<Price> listPrice;
}
