package com.example.demo.dto;

import java.time.LocalDate;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SaveOrderDTO {

	private String iDOrder;

	private float deposit;

	private List<LocalDate> dateStart;

	private LocalDate dateOrder;

	private List<String> listPrice;
	
	
}
