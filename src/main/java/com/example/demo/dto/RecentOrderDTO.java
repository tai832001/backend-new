package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AllArgsConstructor
public class RecentOrderDTO {
	private String idOrder;
	private String imageAccount;
	private String fullname;
	private String phone;
	private String pitchName;
	private String pitchDetailName;
	private int status;
	private LocalDate dateStart;
	private Time timeFrom;
	private Time timeTo;
	private float price;
	private Long numRecords;
}
