package com.example.demo.dto;

import org.springframework.web.multipart.MultipartFile;


public class CreatePitchDTO {
	private String name;
	private String iDDistrict;
	private String address;
	private MultipartFile imagepitch;
	
	public CreatePitchDTO() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getiDDistrict() {
		return iDDistrict;
	}

	public void setiDDistrict(String iDDistrict) {
		this.iDDistrict = iDDistrict;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public MultipartFile getImagepitch() {
		return imagepitch;
	}

	public void setImagepitch(MultipartFile imagepitch) {
		this.imagepitch = imagepitch;
	}

	@Override
	public String toString() {
		return "CreatePitchDTO [name=" + name + ", iDDistrict=" + iDDistrict + ", address=" + address + ", imagepitch="
				+ imagepitch + "]";
	}
	
}
