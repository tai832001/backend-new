package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GetTeamJoinUserDTO {
    private String nameTeam;
	private String fullname;
	private String phone;
	private Long wins;
	private Long looses;
	private Long draws;
	private String statusTeam;
}
