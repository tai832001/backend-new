package com.example.demo.dto;

import java.util.List;

import com.example.demo.entity.Pitch;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DistrictDTO {
	private String iDDistrict;
	private String nameDistrict;
	private boolean flag;
	private List<Pitch> name;
}
