package com.example.demo.dto;

import java.time.LocalDate;
import java.util.Set;

import com.example.demo.entity.TimeRange;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MatchHomeDTO {
	private String idMatch;
	private String address;
	private LocalDate dateStart;
	private String username;
	private String fullname;
	private String namePitch;
	private String description;
	private String district;
	private int size;
	private String grassType;
	private Set<TimeRange> timeRange;
}
