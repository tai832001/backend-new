package com.example.demo.dto;

import java.math.BigInteger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PitchAdminDTO {

	private String iDPitch;
	private String name;
	private String address;
	private String idOwner;
	private Long countPitch;
	
	
	}
