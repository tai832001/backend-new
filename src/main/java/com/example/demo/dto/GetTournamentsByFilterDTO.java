package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;

public class GetTournamentsByFilterDTO {
	private String location;
	private String text;
	private int page;
	
	public GetTournamentsByFilterDTO(String location, String text, int page) {
		super();
		this.location = location;
		this.text = text;
		this.page = page;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
}
