package com.example.demo.dto;

import java.sql.Date;

public class RevenueStatisticsPitchDTOMapping {
	private String iDPitch;
	private String pitchName;
	private Date dateOrder;
	private Double totalPrice;
	private Long numOrder;

	public RevenueStatisticsPitchDTOMapping(String iDPitch, Date dateOrder, Double totalPrice, String pitchName, Long numOrder) {
		super();
		this.iDPitch = iDPitch;
		this.dateOrder = dateOrder;
		this.totalPrice = totalPrice;
		this.pitchName = pitchName;
		this.numOrder = numOrder;
	}
	
	public String getiDPitch() {
		return iDPitch;
	}
	public void setiDPitch(String iDPitch) {
		this.iDPitch = iDPitch;
	}

	public Date getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getPitchName() {
		return pitchName;
	}

	public void setPitchName(String pitchName) {
		this.pitchName = pitchName;
	}

	public Long getNumOrder() {
		return numOrder;
	}

	public void setNumOrder(Long numOrder) {
		this.numOrder = numOrder;
	}
}
