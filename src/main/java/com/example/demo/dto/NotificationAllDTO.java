package com.example.demo.dto;

import java.sql.Time;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class NotificationAllDTO {
	private String note;
	private String sender;
	private String receive;
	private int status;
	private boolean active;
	private boolean flag;
	private LocalDate date;
	private Time time;
}
