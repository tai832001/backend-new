package com.example.demo.dto;

public class StatisticBasicDTO {
	private double totalRevenue;
	private long totalOrder;
	
	public StatisticBasicDTO() {
		super();
	}
	public StatisticBasicDTO(double totalRevenue, long totalOrder) {
		super();
		this.totalRevenue = totalRevenue;
		this.totalOrder = totalOrder;
	}
	public double getTotalRevenue() {
		return totalRevenue;
	}
	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	public long getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(long totalOrder) {
		this.totalOrder = totalOrder;
	}
}
