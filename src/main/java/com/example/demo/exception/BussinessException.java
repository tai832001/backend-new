package com.example.demo.exception;

public class BussinessException extends RuntimeException{
	
	private static final long serialVersionUID = -5084463273085843966L;

	public BussinessException(String msg) {
		super("BussinessException-" + msg);
	}
}
