package com.example.demo.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler({BussinessException.class})
	public ResponseEntity<String> handleExceptionA(Exception e) {
		return ResponseEntity.status(500).body(e.getMessage());
	}
}
