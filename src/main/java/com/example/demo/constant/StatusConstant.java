package com.example.demo.constant;

public class StatusConstant {
	public static final String ACCEPT = "A";
	public static final String WAITING = "W";
	public static final String REJECT = "R";
	public static final String PASSWORD_FORMAT = "^[a-zA-Z0-9!@#$%^&*()\\-_=+{}\\[\\]:;\"'<>,.?/|\\\\]{7,20}$";
	public static final String EMAIL_FORMAT = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
	public static final String PHONE_FORMAT = "^0\\d{9}$";
}
