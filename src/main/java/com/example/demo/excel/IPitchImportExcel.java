package com.example.demo.excel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;


public interface IPitchImportExcel {
	public String readExcel(MultipartFile excelFilePath) throws IOException, SQLException;
	
	public void exportExcel(HttpServletResponse response,String iDPitch) throws IOException;
}
