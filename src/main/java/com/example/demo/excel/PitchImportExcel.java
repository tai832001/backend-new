package com.example.demo.excel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.DistrictReponsitory;
import com.example.demo.dao.PitchDetailRepository;
import com.example.demo.dao.PitchRepository;
import com.example.demo.dao.PitchTypeReponsitory;
import com.example.demo.dto.PitchExcelDTO;
import com.example.demo.entity.District;
import com.example.demo.entity.Pitch;
import com.example.demo.entity.PitchDetail;
import com.example.demo.entity.PitchType;
import com.example.demo.service.PitchService;
import com.example.demo.service.UserService;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class PitchImportExcel implements IPitchImportExcel {

	@Autowired
	private PitchRepository pitchRepository;

	@Autowired
	private PitchDetailRepository pitchDetailRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private PitchService pitchService;
	
	@Autowired
	private PitchTypeReponsitory pitchTypeReponsitory;

	@Autowired
	private DistrictReponsitory districtReponsitory;
	public static final int COLUMN_INDEX_IDPITCH = 0;
	public static final int COLUMN_INDEX_IDPITCH_DETAIL = 1;
	public static final int COLUMN_INDEX_NAME = 2;
	public static final int COLUMN_INDEX_DISTRICT = 3;
	public static final int COLUMN_INDEX_ADDRESS = 4;
	public static final int COLUMN_INDEX_NAME_PITCH = 5;
	public static final int COLUMN_INDEX_NAME_PITCH_TYPE = 6;

	@Override
	public String readExcel(MultipartFile excelFilePath) throws IOException, SQLException {
		Pitch pitch = new Pitch();
		List<PitchDetail> listPitchDetail = new ArrayList<>();
		int flag = 0;
		try {
			// Get workbook
			Workbook workbook = null;
			try {
				workbook = WorkbookFactory.create(excelFilePath.getInputStream());
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				// TODO Auto-generated catch block
			}
			// Get sheet
			Sheet sheet = workbook.getSheet("Pitch");

			// Get all rows
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
					continue;
				}

				// Get all cells
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				PitchDetail pitchDetail = new PitchDetail();
				while (cellIterator.hasNext()) {
					// Read cell
					Cell cell = cellIterator.next();
					Object cellValue = getCellValue(cell);
					if (cellValue == null || cellValue.toString().isEmpty()) {
						continue;
					}
					int columnIndex = cell.getColumnIndex();
					try {
						switch (columnIndex) {
						case COLUMN_INDEX_IDPITCH:
							if (StringUtils.isNoneEmpty((String) getCellValue(cell))) {
								pitch.setiDPitch((String) getCellValue(cell));
							}
							break;
						case COLUMN_INDEX_IDPITCH_DETAIL:
							if (StringUtils.isNoneEmpty((String) getCellValue(cell))) {
								pitchDetail.setiDPitchDetail((String) getCellValue(cell));
							}
							break;
						case COLUMN_INDEX_NAME:
							pitch.setName((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_DISTRICT:
							pitch.setiDDistrict((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_ADDRESS:
							pitch.setAddress((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_NAME_PITCH:
							pitchDetail.setNamePitch((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_NAME_PITCH_TYPE:
							pitchDetail.setiDPitchType((String) getCellValue(cell));
							break;
						default:
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						return "error";
					}
				}
				// Set value for pitch
				pitch.setFlag(true);
				pitch.setiDDayOff(null);
				pitch.setIdOwner(userService.getCurrentUser().getiDAccount());

				// Insert pitch into DB
				if (flag == 0) {
					pitchRepository.save(pitch);
					flag = 1;
				}
				// Set value for pitch detail
				pitchDetail.setFlag(true);
				pitchDetail.setiDPitch(pitch.getiDPitch());
				listPitchDetail.add(pitchDetail);
			}
			workbook.close();
			try {
				for (PitchDetail pitchAdd : listPitchDetail) {
					pitchDetailRepository.save(pitchAdd);
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "error";
			}
		} catch (Exception e) {
			return "error";
		}
		return "success";
	}

	// Get Workbook
	private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}

		return workbook;
	}

	// Get cell value
	private static Object getCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		Object cellValue = null;
		switch (cellType) {
		case BOOLEAN:
			cellValue = cell.getBooleanCellValue();
			break;
		case FORMULA:
			Workbook workbook = cell.getSheet().getWorkbook();
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			cellValue = evaluator.evaluate(cell).getNumberValue();
			break;
		case NUMERIC:
			cellValue = cell.getNumericCellValue();
			break;
		case STRING:
			cellValue = cell.getStringCellValue().trim();
			break;
		case _NONE:
		case BLANK:
		case ERROR:
		default:
			break;
		}
		return cellValue;
	}

	@Override
	public void exportExcel(HttpServletResponse response, String idPitch) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Pitch");
		List<PitchExcelDTO> list = pitchService.getPitchById(idPitch);
		List<District> listDistrict = districtReponsitory.getAllDistrict();
		List<String> district = new ArrayList<>();
		for (District dis : listDistrict) {
			district.add(dis.getiDDistrict());
		}
		String[] values = district.toArray(new String[district.size()]);
		PitchExcelDTO pitch = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID Sân");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("ID Sân nhỏ");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Tên sân");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(3);
	    headerCell.setCellValue("Quận/Huyện");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(4);
	    headerCell.setCellValue("Địa chỉ");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(5);
	    headerCell.setCellValue("Chi tiết sân");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(6);
	    headerCell.setCellValue("Loại sân");
	    headerCell.setCellStyle(headerCellStyle);
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 3, 3);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);
		for (int i = 0; i < list.size(); i++) {
			pitch = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(pitch.getIDPitch());
			row.createCell(1).setCellValue(pitch.getIDPitchDetail());
			row.createCell(2).setCellValue(pitch.getName());
			row.createCell(3).setCellValue(pitch.getIDDistrict());
			row.createCell(4).setCellValue(pitch.getAddress());
			row.createCell(5).setCellValue(pitch.getNamePitch());
			row.createCell(6).setCellValue(pitch.getIDPitchType());
		}

		this.exportDistrict(response,workbook);
		
		this.exportPitchType(response, workbook);
		
		// Set response headers to specify that we are sending an Excel file
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=\"Pitch.xlsx\"");

		workbook.write(response.getOutputStream());

		// Close the workbook to release resources
		workbook.close();
	}
	
	private void exportDistrict(HttpServletResponse response,Workbook workbook) throws IOException {
		String[] values = { "True", "False" };
		Sheet sheet = workbook.createSheet("District");
		List<District> list = districtReponsitory.findAll();
		District district = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Tên");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Flag");
	    headerCell.setCellStyle(headerCellStyle);
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 2, 2);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);
		for (int i = 0; i < list.size(); i++) {
			district = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(district.getiDDistrict());
			row.createCell(1).setCellValue(district.getNameDistrict());
			row.createCell(2).setCellValue(district.isFlag());
		}
	}

	private void exportPitchType(HttpServletResponse response,Workbook workbook) throws IOException {
		String[] values = { "5", "6", "7", "9", "11" };
		String[] flag = { "True", "False" };
		String[] grass = { "Cỏ tự nhiên", "Cỏ tự nhiên", "Cỏ nhân tạo", "Sàn gỗ", "Khác" };
		Sheet sheet = workbook.createSheet("PitchType");
		List<PitchType> list = pitchTypeReponsitory.findAll();
		PitchType pitchType = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Size");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Loại cỏ");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(3);
	    headerCell.setCellValue("Flag");
	    headerCell.setCellStyle(headerCellStyle);
	    
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Size
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 1, 1);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);

		// Flag
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList listFlag = new CellRangeAddressList(2, list.size() + 1, 3, 3);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint1 = validationHelper.createExplicitListConstraint(flag);

		// Create the data validation
		DataValidation dataValidation1 = validationHelper.createValidation(constraint1, listFlag);

		dataValidation1.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation1);

		// Flag
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList listGrass = new CellRangeAddressList(2, list.size() + 1, 2, 2);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint2 = validationHelper.createExplicitListConstraint(grass);

		// Create the data validation
		DataValidation dataValidation2 = validationHelper.createValidation(constraint2, listGrass);

		dataValidation2.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation2);
		for (int i = 0; i < list.size(); i++) {
			pitchType = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(pitchType.getIDPitchType());
			row.createCell(1).setCellValue(pitchType.getSize());
			row.createCell(2).setCellValue(pitchType.getGrassType());
			row.createCell(3).setCellValue(pitchType.isFlag());
		}
	}
}
