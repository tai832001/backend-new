package com.example.demo.excel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;

public interface ITimeRangeImportExcel {
	public Map<String,Object> readExcel(MultipartFile excelFilePath) throws IOException, SQLException;
	
	public void exportExcel(HttpServletResponse response) throws IOException;
}
