package com.example.demo.excel;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.apache.poi.EncryptedDocumentException;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletResponse;

public interface IAccountImportExcel{
	public Map<String,Object> readExcel(MultipartFile file) throws IOException, SQLException, EncryptedDocumentException;
	
	public void exportExcel(HttpServletResponse response) throws IOException;
	
	
}
