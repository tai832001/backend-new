package com.example.demo.excel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.constant.StatusConstant;
import com.example.demo.dao.RoleReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.Role;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class AccountImportExcel implements IAccountImportExcel {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserReponsitory userReponsitory;

	@Autowired
	private RoleReponsitory roleReponsitory;

	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_USERNAME = 1;
	public static final int COLUMN_INDEX_PASSWORD = 2;
	public static final int COLUMN_INDEX_NAME = 3;
	public static final int COLUMN_INDEX_EMAIL = 4;
	public static final int COLUMN_INDEX_PHONE = 5;
	public static final int COLUMN_INDEX_BIRTHDAY = 6;
	public static final int COLUMN_INDEX_ROLE = 7;
	public static final int COLUMN_INDEX_ADDRESS = 8;

	@Override
	public Map<String, Object> readExcel(MultipartFile excelFilePath)
			throws IOException, SQLException, EncryptedDocumentException {
		Map<String, Object> mapList = new HashMap<>();
		List<Account> listAccounts = new ArrayList<>();
		List<String> listUsername = new ArrayList<>();
		List<String> listEmail = new ArrayList<>();
		List<String> listPhone = new ArrayList<>();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		Workbook workbook = null;
		try {
			
			try {
				workbook = WorkbookFactory.create(excelFilePath.getInputStream());
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
			// Get sheet
			Sheet sheet = workbook.getSheet("Account");

			// Get all rows
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Account account = new Account();
				Row nextRow = iterator.next();
				if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
					continue;
				}

				// Get all cells
				Iterator<Cell> cellIterator = nextRow.cellIterator();

				while (cellIterator.hasNext()) {
					// Read cell
					Cell cell = cellIterator.next();
					Object cellValue = getCellValue(cell);
					if (cellValue == null || cellValue.toString().isEmpty()) {
						continue;
					}
					int columnIndex = cell.getColumnIndex();
					try {
						switch (columnIndex) {
						case COLUMN_INDEX_ID:
							if (StringUtils.isNoneEmpty((String) getCellValue(cell))) {
								account.setiDAccount((String) getCellValue(cell));
							}
							break;
						case COLUMN_INDEX_USERNAME:
							String username = (String) getCellValue(cell);
							if (listUsername.size() == 0) {
								listUsername.add(username);
								account.setUsername(username);
							} else {
								for (String checkUser : listUsername) {
									if (StringUtils.isNotEmpty(username) && !username.equals(checkUser)) {
										account.setUsername(username);
									} else {
										mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
										return mapList;
									}
								}
							}
							listUsername.add(username);
							break;
						case COLUMN_INDEX_PASSWORD:
							if (StringUtils.isNotEmpty((String) getCellValue(cell))
									&& !getCellValue(cell).toString().startsWith("$2a$10$")) {
								if (!getCellValue(cell).toString().matches(StatusConstant.PASSWORD_FORMAT)) {
									mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
									return mapList;
								}
								account.setPassword(passwordEncoder.encode((String) getCellValue(cell)));
							} else {
								account.setPassword((String) getCellValue(cell));
							}
							break;
						case COLUMN_INDEX_NAME:
							account.setFullname((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_EMAIL:
							String email = (String) getCellValue(cell);
							if (!email.matches(StatusConstant.EMAIL_FORMAT)) {
								mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
								return mapList;
							}
							if (listEmail.size() == 0) {
								listEmail.add(email);
								account.setEmail(email);
							} else {
								for (String checkEmail : listEmail) {
									if (StringUtils.isNotEmpty(email) && !email.equals(checkEmail)
											&& email.matches(StatusConstant.EMAIL_FORMAT)) {
										account.setEmail(email);
									} else {
										mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
										return mapList;
									}
								}
							}
							listEmail.add(email);
							break;
						case COLUMN_INDEX_PHONE:
							String phone = (String) getCellValue(cell);
							if (!phone.matches(StatusConstant.PASSWORD_FORMAT)) {
								mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
								return mapList;
							}
							if (listPhone.size() == 0) {
								listPhone.add(phone);
								account.setPhone(phone);
							} else {
								for (String checkPhone : listPhone) {
									if (StringUtils.isNoneEmpty(phone) && !phone.contains(checkPhone)
											&& phone.matches(StatusConstant.PHONE_FORMAT)) {
										account.setPhone(phone);
									} else {
										mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
										return mapList;
									}
								}
							}
							listPhone.add(phone);
							break;
						case COLUMN_INDEX_BIRTHDAY:
							String b = (String) getCellValue(cell);
							LocalDate localDate = LocalDate.parse(b, formatter);
							account.setBirthday(localDate);
							break;
						case COLUMN_INDEX_ROLE:
							String role = (String) getCellValue(cell);
//							boolean addRole = role.equals("ADMIN") ? roles.add(roleAdmin) : roles.add(roleUser) ;
							if ("ADMIN".equals(role)) {
								Role userRole = roleReponsitory.findByAuthority("ADMIN").get();
								Set<Role> authorities = new HashSet<>();
								authorities.add(userRole);
								account.setAuthorities(authorities);
							} else if ("USER".equals(role)) {
								Role userRole = roleReponsitory.findByAuthority("USER").get();
								Set<Role> authorities1 = new HashSet<>();
								authorities1.add(userRole);
								account.setAuthorities(authorities1);
							} else if ("OWNER".equals(role)) {
								Role userRole = roleReponsitory.findByAuthority("OWNER").get();
								Set<Role> authorities2 = new HashSet<>();
								authorities2.add(userRole);
								account.setAuthorities(authorities2);
							}
							break;
						case COLUMN_INDEX_ADDRESS:
							account.setAddress((String) getCellValue(cell));
							break;
						default:
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
						return mapList;
					}
				}
				account.setFlag(true);
				account.setReliability(100);
				account.setImageAccount(null);
				listAccounts.add(account);
			}
			workbook.close();
			try {
				for (Account listAcc : listAccounts) {
					userReponsitory.save(listAcc);
				}
			} catch (Exception e) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}

		} catch (Exception e) {
			mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
			return mapList;
		}
		mapList.put("status", "update");
		mapList.put("list", listAccounts);
		mapList.put("error", null);

		return mapList;
	}

	// Get Workbook
	private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}

		return workbook;
	}

	// Get cell value
	private Object getCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		Object cellValue = null;
		switch (cellType) {
		case BOOLEAN:
			cellValue = cell.getBooleanCellValue();
			break;
		case FORMULA:
			Workbook workbook = cell.getSheet().getWorkbook();
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			cellValue = evaluator.evaluate(cell).getNumberValue();
			break;
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				java.util.Date dateValue = cell.getDateCellValue();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String formattedDate = dateFormat.format(dateValue);
				cellValue = formattedDate;
			} else {
				cellValue = cell.getNumericCellValue();
			}
			break;
		case STRING:
			cellValue = cell.getStringCellValue().trim();
			break;
		case _NONE:
		case BLANK:
		case ERROR:
		default:
			break;
		}
		return cellValue;
	}

	@Override
	public void exportExcel(HttpServletResponse response) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Account");
		List<Account> list = userReponsitory.getAccount();
		String[] values = { "ADMIN", "USER", "OWNER" };
		Account account = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Username");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Password");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(3);
	    headerCell.setCellValue("Fullname");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(4);
	    headerCell.setCellValue("Email");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(5);
	    headerCell.setCellValue("Phone number");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(6);
	    headerCell.setCellValue("Birthday");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(7);
	    headerCell.setCellValue("Role");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(8);
	    headerCell.setCellValue("Address");
	    headerCell.setCellStyle(headerCellStyle);
		int rowNum = 2;
		Set<Role> role = new HashSet<>();
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 7, 7);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		// Apply the cell style to the data validation
//        dataValidation.se

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);
		for (int i = 0; i < list.size(); i++) {
			account = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(account.getiDAccount());
			row.createCell(1).setCellValue(account.getUsername());
			row.createCell(2).setCellValue(account.getPassword());
			row.createCell(3).setCellValue(account.getFullname());
			row.createCell(4).setCellValue(account.getEmail());
			row.createCell(5).setCellValue(account.getPhone());
			if (account.getBirthday() == null) {
				row.createCell(6).setCellValue("");
			} else {
				row.createCell(6).setCellValue(account.getBirthday().toString());
			}
			role = (Set<Role>) account.getAuthorities();
			row.createCell(7).setCellValue(role.toString().substring(1, role.toString().length() - 1));
			row.createCell(8).setCellValue(account.getAddress());
		}
		// Set response headers to specify that we are sending an Excel file
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=\"Account.xlsx\"");

		workbook.write(response.getOutputStream());

		// Close the workbook to release resources
		workbook.close();
	}
}
