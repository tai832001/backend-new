package com.example.demo.excel;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.PitchTypeReponsitory;
import com.example.demo.entity.PitchType;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class PitchTypeImportExcel implements IPitchTypeImportExcel {

	@Autowired
	private PitchTypeReponsitory pitchTypeReponsitory;
	public static final int COLUMN_INDEX_IDPITCHTYPE = 0;
	public static final int COLUMN_INDEX_SIZE = 1;
	public static final int COLUMN_INDEX_GRASS = 2;
	public static final int COLUMN_INDEX_FLAG = 3;

	@Override
	public Map<String, Object> readExcel(MultipartFile excelFilePath) throws IOException, SQLException {
		List<PitchType> listPitchType = new ArrayList<>();
		Map<String, Object> mapList = new HashMap<>();
		try {
			// Get sheet
			Workbook workbook = null;
			try {
				workbook = WorkbookFactory.create(excelFilePath.getInputStream());
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
			Sheet sheet = workbook.getSheet("PitchType");

			// Get all rows
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
					continue;
				}

				// Get all cells
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				PitchType pitchType = new PitchType();
				while (cellIterator.hasNext()) {
					// Read cell
					Cell cell = cellIterator.next();
					Object cellValue = getCellValue(cell);
					if (cellValue == null || cellValue.toString().isEmpty()) {
						continue;
					}
					int columnIndex = cell.getColumnIndex();
					try {
						switch (columnIndex) {
						case COLUMN_INDEX_IDPITCHTYPE:
							if (StringUtils.isNoneEmpty((String) getCellValue(cell))) {
								pitchType.setIDPitchType((String) getCellValue(cell));
							} else {
							}
							break;
						case COLUMN_INDEX_SIZE:
							pitchType.setSize(new BigDecimal((double) cellValue).intValue());
							break;
						case COLUMN_INDEX_GRASS:
							pitchType.setGrassType((String) getCellValue(cell));
							break;
						case COLUMN_INDEX_FLAG:
							pitchType.setFlag((Boolean) getCellValue(cell));
							break;
						default:
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
						return mapList;
					}
				}
				if (pitchType.getGrassType() != null) {
					listPitchType.add(pitchType);
				} else {
					mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
					return mapList;
				}
			}
			workbook.close();
			try {
				for (PitchType savePitch : listPitchType) {
					pitchTypeReponsitory.save(savePitch);
				}
			} catch (Exception e) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
		}catch (Exception e) {
			mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
			return mapList;
		}
		mapList.put("error", null);
		mapList.put("list", listPitchType);
		return mapList;
	}

	// Get Workbook
	private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}

		return workbook;
	}

	// Get cell value
	private static Object getCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		Object cellValue = null;
		switch (cellType) {
		case BOOLEAN:
			cellValue = cell.getBooleanCellValue();
			break;
		case FORMULA:
			Workbook workbook = cell.getSheet().getWorkbook();
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			cellValue = evaluator.evaluate(cell).getNumberValue();
			break;
		case NUMERIC:
			cellValue = cell.getNumericCellValue();
			break;
		case STRING:
			cellValue = cell.getStringCellValue().trim();
			break;
		case _NONE:
		case BLANK:
		case ERROR:
		default:
			break;
		}
		return cellValue;
	}

	@Override
	public void exportExcel(HttpServletResponse response) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		String[] values = { "5", "6", "7", "9", "11" };
		String[] flag = { "True", "False" };
		String[] grass = { "Cỏ tự nhiên", "Cỏ tự nhiên", "Cỏ nhân tạo", "Sàn gỗ", "Khác" };
		Sheet sheet = workbook.createSheet("PitchType");
		List<PitchType> list = pitchTypeReponsitory.findAll();
		PitchType pitchType = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Size");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Loại cỏ");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(3);
	    headerCell.setCellValue("Flag");
	    headerCell.setCellStyle(headerCellStyle);
	    
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Size
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 1, 1);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);

		// Flag
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList listFlag = new CellRangeAddressList(2, list.size() + 1, 3, 3);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint1 = validationHelper.createExplicitListConstraint(flag);

		// Create the data validation
		DataValidation dataValidation1 = validationHelper.createValidation(constraint1, listFlag);

		dataValidation1.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation1);

		// Flag
		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList listGrass = new CellRangeAddressList(2, list.size() + 1, 2, 2);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint2 = validationHelper.createExplicitListConstraint(grass);

		// Create the data validation
		DataValidation dataValidation2 = validationHelper.createValidation(constraint2, listGrass);

		dataValidation2.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation2);
		for (int i = 0; i < list.size(); i++) {
			pitchType = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(pitchType.getIDPitchType());
			row.createCell(1).setCellValue(pitchType.getSize());
			row.createCell(2).setCellValue(pitchType.getGrassType());
			row.createCell(3).setCellValue(pitchType.isFlag());
		}
		// Set response headers to specify that we are sending an Excel file
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=\"PitchType.xlsx\"");
        
        workbook.write(response.getOutputStream());

        // Close the workbook to release resources
        workbook.close();
	}

}
