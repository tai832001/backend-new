package com.example.demo.excel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.TimeRepository;
import com.example.demo.entity.TimeRange;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class TimeRangeImportExcel implements ITimeRangeImportExcel {

	@Autowired
	private TimeRepository timeRepository; 
	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_FROM = 1;
	public static final int COLUMN_INDEX_TO = 2;
	public static final int COLUMN_INDEX_FLAG = 3;

	@Override
	public Map<String,Object> readExcel(MultipartFile excelFilePath) throws IOException, SQLException {
		Map<String,Object> mapList = new HashMap<>();
		List<TimeRange> listTimeRange = new ArrayList<>();

		// Get workbook
		Workbook workbook = null;
		try {
			try {
				workbook = WorkbookFactory.create(excelFilePath.getInputStream());
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}

			// Get sheet
			Sheet sheet = workbook.getSheet("Time_Range");

			// Get all rows
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
					continue;
				}

				// Get all cells
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				TimeRange timeRange = new TimeRange();
				while (cellIterator.hasNext()) {
					// Read cell
					Cell cell = cellIterator.next();
					Object cellValue = getCellValue(cell);
					if (cellValue == null || cellValue.toString().isEmpty()) {
						continue;
					}
					int columnIndex = cell.getColumnIndex();
					try {
						switch (columnIndex) {
						case COLUMN_INDEX_ID:
							if(StringUtils.isNoneEmpty((String)getCellValue(cell))){
								timeRange.setiDTime((String)getCellValue(cell));
								mapList.put("status", "update");
							}else {
							mapList.put("status", "save");
							}
							break;
						case COLUMN_INDEX_FROM:
					        // Convert the date to string
					        String dateStringFrom = (String) getCellValue(cell);
					        timeRange.setTimeFrom(Time.valueOf(dateStringFrom));
							break;
						case COLUMN_INDEX_TO:
					        // Convert the date to string
					        String dateStringTo = (String) getCellValue(cell);
					        timeRange.setTimeTo(Time.valueOf(dateStringTo));
							break;
						case COLUMN_INDEX_FLAG:
							timeRange.setFlag((Boolean) getCellValue(cell));
							break;
						default:
							break;
						}
					} catch (Exception e) {
						e.printStackTrace();
						mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
						return mapList;
					}
				}
				if(timeRange.getTimeFrom() != null && timeRange.getTimeTo() != null) {				
					listTimeRange.add(timeRange);
				}else {
					mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
					return mapList;
				}
			}
			workbook.close();
			try {
				for (TimeRange time : listTimeRange) {
					timeRepository.save(time);
				}
			} catch (Exception e) {
				e.printStackTrace();
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
		}catch (Exception e) {
			mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
			return mapList;
		}
		
		mapList.put("error", null);
		mapList.put("list", listTimeRange);
		return mapList;
	}

	// Get Workbook
	private static Workbook getWorkbook(InputStream inputStream, String excelFilePath) throws IOException {
		Workbook workbook = null;
		if (excelFilePath.endsWith("xlsx")) {
			workbook = new XSSFWorkbook(inputStream);
		} else if (excelFilePath.endsWith("xls")) {
			workbook = new HSSFWorkbook(inputStream);
		} else {
			throw new IllegalArgumentException("The specified file is not Excel file");
		}

		return workbook;
	}

	// Get cell value
	private static Object getCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		Object cellValue = null;
		switch (cellType) {
		case BOOLEAN:
			cellValue = cell.getBooleanCellValue();
			break;
		case FORMULA:
			Workbook workbook = cell.getSheet().getWorkbook();
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			cellValue = evaluator.evaluate(cell).getNumberValue();
			break;
		case NUMERIC:
			cellValue = cell.getDateCellValue();
			break;
		case STRING:
			cellValue = cell.getStringCellValue().trim();
			break;
		case _NONE:
		case BLANK:
		case ERROR:
		default:
//			cellValue = cell.getDateCellValue();
			break;
		}
		return cellValue;
	}

	@Override
	public void exportExcel(HttpServletResponse response) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		String[] values = {"True", "False"};
		Sheet sheet = workbook.createSheet("Time_Range");
		List<TimeRange> list = timeRepository.findAll();
		TimeRange timeRange = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID Sân");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Bắt đầu");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Kết thúc");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(3);
	    headerCell.setCellValue("Flag");
	    headerCell.setCellStyle(headerCellStyle);
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
        DataValidationHelper validationHelper = sheet.getDataValidationHelper();

        // Set the data validation to column H (index 7) for all rows
        CellRangeAddressList addressList = new CellRangeAddressList(2, list.size()+1, 3, 3);

        // Create a data validation constraint for the drop-down list
        DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

        // Create the data validation
        DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

        dataValidation.setSuppressDropDownArrow(true);
        // Add the data validation to the sheet
        sheet.addValidationData(dataValidation);
        
        for (int i = 0; i < list.size(); i++) {
			timeRange = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(timeRange.getiDTime());
//			Time dateStringFrom = timeRange.getTimeFrom();
			// Create a SimpleDateFormat object with the desired time format
//			String timeStringFrom = timeFormat.format(dateStringFrom);
			row.createCell(1).setCellValue(timeRange.getTimeFrom().toString());
//			Time dateStringTo = timeRange.getTimeTo();
			// Create a SimpleDateFormat object with the desired time format
//			String timeStringTo = timeFormat.format(dateStringTo);
			row.createCell(2).setCellValue(timeRange.getTimeTo().toString());
			row.createCell(3).setCellValue(timeRange.isFlag());
		}
        
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Content-Disposition", "attachment; filename=\"TimeRange.xlsx\"");
        
        workbook.write(response.getOutputStream());

        // Close the workbook to release resources
        workbook.close();
	}

}
