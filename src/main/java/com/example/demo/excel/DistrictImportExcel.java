package com.example.demo.excel;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.DistrictReponsitory;
import com.example.demo.entity.District;

import jakarta.servlet.http.HttpServletResponse;

@Service
public class DistrictImportExcel implements IDistrictImportExcel {

	@Autowired
	private DistrictReponsitory districtReponsitory;

	public static final int COLUMN_INDEX_ID = 0;
	public static final int COLUMN_INDEX_NAME = 1;
	public static final int COLUMN_INDEX_FLAG = 2;

	@Override
	public Map<String, Object> readExcel(MultipartFile excelFilePath)
			throws IOException, SQLException, EncryptedDocumentException {
		StringBuilder note = new StringBuilder();
		// note.append("Thong tin nhap sai o vi tri: ");
		String column = null;
		List<District> listDistrict = new ArrayList<>();
		Map<String, Object> mapList = new HashMap<>();

		// Get workbook
		Workbook workbook = null;
		try {
			try {
				workbook = WorkbookFactory.create(excelFilePath.getInputStream());
			} catch (EncryptedDocumentException | InvalidFormatException | IOException e1) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
			// Get sheet
			Sheet sheet = workbook.getSheet("District");

			// Get all rows
			Iterator<Row> iterator = sheet.iterator();

			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				if (nextRow.getRowNum() == 0 || nextRow.getRowNum() == 1) {
					continue;
				}

				// Get all cells
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				District district = new District();
				while (cellIterator.hasNext()) {
					// Read cell
					Cell cell = cellIterator.next();
					Object cellValue = getCellValue(cell);
					if (cellValue == null || cellValue.toString().isEmpty()) {
						continue;
					}
					int columnIndex = cell.getColumnIndex();
					try {
						switch (columnIndex) {
							case COLUMN_INDEX_ID:
								if (StringUtils.isNoneEmpty((String) getCellValue(cell))) {
									district.setiDDistrict((String) getCellValue(cell));
									mapList.put("status", "update");
								} else {
									mapList.put("status", "save");
								}
								break;
							case COLUMN_INDEX_NAME:
								column = "B";
								district.setNameDistrict((String) getCellValue(cell));
								break;
							case COLUMN_INDEX_FLAG:
								column = "C";
								district.setFlag((Boolean) getCellValue(cell));
								break;
							default:
								break;
						}
					} catch (Exception e) {
						note.append((nextRow.getRowNum() + 1) + column + " | ");
						e.printStackTrace();
						mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
						return mapList;
					}
				}
				if (district.getNameDistrict() != null && !district.getNameDistrict().isBlank()) {
					listDistrict.add(district);
				} else {
					note.append((nextRow.getRowNum() + 1) + "B" + " | ");
					mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
					return mapList;
				}
			}
			workbook.close();
			// inputStream.close();
			try {
				for (District district : listDistrict) {
					districtReponsitory.save(district);
				}
			} catch (Exception e) {
				mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
				return mapList;
			}
		}catch (Exception e) {
			mapList.put("error", "Kiểm tra lại file và nội dung thông tin");
			return mapList;
		}
		mapList.put("error", null);
		mapList.put("list", listDistrict);
		return mapList;
	}


	// Get cell value
	private static Object getCellValue(Cell cell) {
		CellType cellType = cell.getCellTypeEnum();
		Object cellValue = null;
		switch (cellType) {
			case BOOLEAN:
				cellValue = cell.getBooleanCellValue();
				break;
			case FORMULA:
				Workbook workbook = cell.getSheet().getWorkbook();
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
				cellValue = evaluator.evaluate(cell).getNumberValue();
				break;
			case NUMERIC:
				cellValue = cell.getNumericCellValue();
				break;
			case STRING:
				cellValue = cell.getStringCellValue().trim();
				break;
			case _NONE:
			case BLANK:
			case ERROR:
			default:
				break;
		}
		return cellValue;
	}

	@Override
	public void exportExcel(HttpServletResponse response) throws IOException {
		Workbook workbook = new XSSFWorkbook();
		String[] values = { "True", "False" };
		Sheet sheet = workbook.createSheet("District");
		List<District> list = districtReponsitory.findAll();
		District district = null;
		Row headerRow = sheet.createRow(1);
		CellStyle headerCellStyle = workbook.createCellStyle();
	    Font headerFont = workbook.createFont();
	    headerFont.setBold(true);
	    headerCellStyle.setFont(headerFont);
	    headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
	    Cell headerCell;
	    
	    headerCell = headerRow.createCell(0);
	    headerCell.setCellValue("ID");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(1);
	    headerCell.setCellValue("Tên");
	    headerCell.setCellStyle(headerCellStyle);
	    
	    headerCell = headerRow.createCell(2);
	    headerCell.setCellValue("Flag");
	    headerCell.setCellStyle(headerCellStyle);
		int rowNum = 2;
		CellStyle style = workbook.createCellStyle();
		DataValidationHelper validationHelper = sheet.getDataValidationHelper();

		// Set the data validation to column H (index 7) for all rows
		CellRangeAddressList addressList = new CellRangeAddressList(2, list.size() + 1, 2, 2);

		// Create a data validation constraint for the drop-down list
		DataValidationConstraint constraint = validationHelper.createExplicitListConstraint(values);

		// Create the data validation
		DataValidation dataValidation = validationHelper.createValidation(constraint, addressList);

		dataValidation.setSuppressDropDownArrow(true);
		// Add the data validation to the sheet
		sheet.addValidationData(dataValidation);
		for (int i = 0; i < list.size(); i++) {
			district = list.get(i);
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(district.getiDDistrict());
			row.createCell(1).setCellValue(district.getNameDistrict());
			row.createCell(2).setCellValue(district.isFlag());
		}
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		response.setHeader("Content-Disposition", "attachment; filename=\"District.xlsx\"");

		workbook.write(response.getOutputStream());

		// Close the workbook to release resources
		workbook.close();
	}
}
