
package com.example.demo.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.OrderPitch;
import com.example.demo.entity.Price;

@Repository
@Transactional
public interface OrderPitchRepository extends JpaRepository<OrderPitch, String> {
	@Query("SELECT distinct o FROM OrderPitch o " + "Left JOIN o.orderPrices op "
			+ "WHERE op.price.pitchDetail.pitch.idOwner = :id AND op.price.pitchDetail.pitch.iDPitch = :pid AND o.status = 0 "
			+ "ORDER BY o.dateOrder DESC")
	Page<OrderPitch> getPageOrderPitchByOwnerID(@Param("id") String id, @Param("pid") String pid, Pageable pageable);

	@Query("SELECT op.orderPitch FROM OrderPrice op WHERE op.price = :price AND op.date = :date AND op.orderPitch.status <> 3")
	List<OrderPitch> findOrderPitch(@Param("price") Price price, @Param("date") LocalDate date);

	@Query("SELECT u FROM OrderPitch u WHERE u.flag = true AND u.account.iDAccount = :id order by u.dateOrder DESC")
	List<OrderPitch> findHistory(@Param("id") String id);
}
