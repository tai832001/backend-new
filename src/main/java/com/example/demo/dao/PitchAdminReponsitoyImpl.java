package com.example.demo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.PitchAdminDTO;
import com.example.demo.dto.PitchExcelDTO;

import jakarta.persistence.TypedQuery;
@Repository
public class PitchAdminReponsitoyImpl implements IPitchAdminReponsitory{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<PitchAdminDTO> getPitchFromDistrict(String idDistrict) {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new com.example.demo.dto.PitchAdminDTO(");
		hql.append("	p.iDPitch, ");
		hql.append("	p.name,");
		hql.append("	p.address,");
		hql.append("	acc.fullname, ");
		hql.append("	count(pd.iDPitchDetail) as countPitch) ");
		hql.append("FROM ");
		hql.append("	Pitch p ");
		hql.append("LEFT JOIN ");
		hql.append("	PitchDetail pd ");
		hql.append("ON ");
		hql.append("	p.iDPitch = pd.iDPitch ");
		hql.append("RIGHT JOIN ");
		hql.append("	Account acc ");
		hql.append("ON ");
		hql.append("	acc.iDAccount = p.idOwner ");
		hql.append("WHERE ");
		hql.append("	p.iDDistrict = :idDistrict ");
		hql.append("	AND p.flag = true ");
		hql.append("GROUP BY ");
		hql.append("	p.iDPitch, ");
		hql.append("	p.name, ");
		hql.append("	p.address, ");
		hql.append("	acc.fullname");
		Session session = sessionFactory.openSession();
		TypedQuery<PitchAdminDTO> query = session.createQuery(hql.toString(), PitchAdminDTO.class);
		query.setParameter("idDistrict", idDistrict);
		
		List<PitchAdminDTO> list = query.getResultList();
		session.close();
		return list;
	}

	@Override
	public List<PitchExcelDTO> getPitchById(String iDPitch) {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new com.example.demo.dto.PitchExcelDTO(");
		hql.append("	p.iDPitch, ");
		hql.append("	pd.iDPitchDetail, ");
		hql.append("	p.name, ");
		hql.append("	d.iDDistrict, ");
		hql.append("	p.address, ");
		hql.append("	pd.namePitch, ");
		hql.append("	pt.iDPitchType, ");
		hql.append("	p.idOwner) ");
		hql.append("FROM ");
		hql.append("	Pitch p ");
		hql.append("LEFT JOIN ");
		hql.append("	PitchDetail pd ");
		hql.append("ON ");
		hql.append("	p.iDPitch = pd.iDPitch ");
		hql.append("LEFT JOIN ");
		hql.append("	District d ");
		hql.append("ON ");
		hql.append("	p.iDDistrict = d.iDDistrict ");
		hql.append("LEFT JOIN ");
		hql.append("	PitchType pt ");
		hql.append("ON ");
		hql.append("	pd.iDPitchType = pt.iDPitchType ");
		hql.append("WHERE ");
		hql.append("	p.iDPitch = :iDPitch ");
		hql.append("	AND p.flag = true ");
		Session session = sessionFactory.openSession();
		TypedQuery<PitchExcelDTO> query = session.createQuery(hql.toString(), PitchExcelDTO.class);
		query.setParameter("iDPitch", iDPitch);
		List<PitchExcelDTO> list = query.getResultList();
		session.close();
		return list;
		
	}

}
