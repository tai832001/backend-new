package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Account;
import com.example.demo.entity.TournamentTeams;

import jakarta.persistence.TypedQuery;

@Repository
public class TournamentTeamDAOImpl implements TournamentTeamDAO {
	
	public static final String ACCEPT = "A";
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<TournamentTeams> getTeamInTournament(String id) {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" FROM ");
		hql.append("	TournamentTeams tnt");
		hql.append(" WHERE ");
		hql.append(" 	tnt.tournaments.iDTournaments = :tnID");
		hql.append("	AND tnt.status = :status");
		hql.append(	"	AND tnt.teams.flag = true");
		
		List<TournamentTeams> rs = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<TournamentTeams> query = session.createQuery(hql.toString(), TournamentTeams.class);
			query.setParameter("tnID", id);
			query.setParameter("status", ACCEPT);
			
			rs = query.getResultList();
		}
		return rs;
	}

	@Override
	public TournamentTeams checkTeamExistInTournament(String tmId, Account acc) {
		StringBuilder hql = new StringBuilder();
		
		hql.append(" FROM ");
		hql.append("	TournamentTeams tnt");
		hql.append(" WHERE ");
		hql.append(" 	tnt.tournaments.iDTournaments = :tmID");
		hql.append(	"	AND tnt.teams.account.username = :us");
		
		TournamentTeams rs;
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<TournamentTeams> query = session.createQuery(hql.toString(), TournamentTeams.class);
			query.setParameter("tmID", tmId);
			query.setParameter("us", acc.getUsername());
			
			rs = query.getResultList().stream().findFirst().orElse(new TournamentTeams());
		}
		return rs;
	}
}
