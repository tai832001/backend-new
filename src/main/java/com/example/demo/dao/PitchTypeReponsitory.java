package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.PitchType;

public interface PitchTypeReponsitory extends JpaRepository<PitchType, String>{
    @Query("SELECT pt FROM PitchType pt WHERE pt.iDPitchType = :id")
    PitchType getByID(@Param("id") String id);
}
