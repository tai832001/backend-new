package com.example.demo.dao;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Notification;

public interface NotificationReponsitory extends JpaRepository<Notification, Long> {

	@Query("select u from Notification u where u.receive.iDAccount = :id ORDER BY u.id DESC")
	public Page<Notification> findByIdAccount(Pageable page, @Param("id") String id);
	
	@Query("select count(u) from Notification u where u.active = true and u.receive.iDAccount = :id")
	public int getNumberNoti(@Param("id") String id);
	
	@Query("select u from Notification u where u.status = 2 and u.sender.iDAccount = :ids and u.receive.iDAccount = :idr")
	public Notification getNotiMess(@Param("ids") String ids,@Param("idr") String idr);
}
