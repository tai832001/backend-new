package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.OrderPitch;

public interface OrderReponsitory extends JpaRepository<OrderPitch, String>{
	
	@Query("SELECT op FROM OrderPitch op")
	public List<OrderPitch> listOrder();
}
