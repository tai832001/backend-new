package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.PitchDetailDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.PitchDetail;

import jakarta.persistence.TypedQuery; 

@Repository
public class PitchDetailDaoImpl implements PitchDetailDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<PitchDetail> getPitchDetailsByUser(Account account) {
		StringBuilder hql = new StringBuilder();
//		hql.append("SELECT ");
//		hql.append("	pd ");
		hql.append("FROM PitchDetail pd ");
		hql.append("WHERE pd.pitch.account.username = :username AND pd.flag = true ");

		List<PitchDetail> rs = new ArrayList<>();

		try (Session session = sessionFactory.openSession();) {
			TypedQuery<PitchDetail> query = session.createQuery(hql.toString(), PitchDetail.class);
			query.setParameter("username", account.getUsername());
			
			rs = query.getResultList();
		}
		return rs;
	}

}
