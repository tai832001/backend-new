package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Conversation;


public interface ConverReponsitory extends JpaRepository<Conversation, Integer> {
	Optional<Conversation> findByConversationName(String conversationName);
}
