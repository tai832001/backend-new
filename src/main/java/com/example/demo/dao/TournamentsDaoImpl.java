package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.controller.GetTournamentsByFilterRespDTO;

import jakarta.persistence.TypedQuery;

@Repository
public class TournamentsDaoImpl implements TournamentsDao {
	
	public static final int NUMBER_RECORDS = 12;
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<GetTournamentsByFilterRespDTO> getTournamentsByFilter(String location, String conditions, int page) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT ");
		hql.append(" 	new com.example.demo.controller.GetTournamentsByFilterRespDTO( ");
		hql.append("		t.image as tourImage, ");
		hql.append("		t.nameTouraments as nameTourament, ");
		hql.append("		t.iDTournaments as idTournament, ");
		hql.append("		t.pitch.name as pitchName, ");
		hql.append("		t.dateStart as dateStart, ");
		hql.append("		t.dateTo as dateTo, ");
		hql.append("		t.pitch.address as address, ");
		hql.append("		t.pitch.district.nameDistrict as nameDistrict, ");
		hql.append("		COUNT(t.iDTournaments) OVER() as records");
		hql.append("	) ");
		hql.append(" FROM ");
		hql.append("	Tournaments AS t ");
		hql.append(" WHERE ");
		hql.append(" 	t.flag = true ");

		if(location != null && location.length() > 0) {
			hql.append(" 	AND t.pitch.iDDistrict = :location ");
		}
		
		if(conditions != null && conditions.length() > 0) {
			hql.append(" 	AND (t.pitch.name LIKE CONCAT('%', :condition, '%') OR t.nameTouraments LIKE CONCAT('%', :condition, '%')) ");
		}
		
		hql.append(" ORDER BY ");
		hql.append(" CASE ");
		hql.append("     WHEN t.dateStart >= current_date() THEN 0 ");
		hql.append("     ELSE 1 ");
		hql.append(" END, ");
		hql.append(" CASE ");
		hql.append("     WHEN t.dateStart < current_date() THEN t.dateStart ");
		hql.append("     ELSE NULL ");
		hql.append(" END DESC, ");
		hql.append(" CASE ");
		hql.append("     WHEN t.dateStart >= current_date() THEN t.dateStart ");
		hql.append("     ELSE NULL ");
		hql.append(" END ASC ");
		
		List<GetTournamentsByFilterRespDTO> rs = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession();) {
			TypedQuery<GetTournamentsByFilterRespDTO> query = session.createQuery(hql.toString(), GetTournamentsByFilterRespDTO.class);
			if (conditions != null && conditions.length() > 0) {
		        query.setParameter("condition", conditions);
		    }

		    if (location != null && location.length() > 0) {
		        query.setParameter("location", location);
		    }
		    
		    query.setFirstResult((page-1) * NUMBER_RECORDS);
		    query.setMaxResults(NUMBER_RECORDS);
			rs = query.getResultList();
		}
		
		return rs;
	}

}
