package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.District;

public interface DistrictReponsitory extends JpaRepository<District, String> {

	@Query("select u from District u where u.flag = true")
	public List<District> getAllDistrict();

	@Query("SELECT DISTINCT d FROM District d " + "INNER JOIN Pitch p ON d.iDDistrict = p.iDDistrict "
			+ "INNER JOIN PitchDetail pd ON p.iDPitch = pd.iDPitch "
			+ "INNER JOIN Price pc ON pd.iDPitchDetail = pc.iDPitchDetail")
	public List<District> getDistrictsWithPitchDetailsAndPrices();
	

	@Query("select u from District u WHERE u.flag = true")
	public List<District> getAll();

}
