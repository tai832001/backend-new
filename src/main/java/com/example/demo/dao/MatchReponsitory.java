package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.entity.Match;

public interface MatchReponsitory extends JpaRepository<Match, String> {
	
	@Query("SELECT u,u.pitchDetail.pitch,u.pitchDetail.pitchType FROM Match u WHERE u.flag = true and u.pitch = false")
	public List<Match> getAll();
	
	@Query("SELECT u FROM Match u WHERE u.flag = true and u.pitch = false")
	public Page<Match> listMatch(Pageable pageable);
	
	@Query("SELECT u FROM Match u " +
		       "JOIN u.timeRange tr " +
		       "WHERE u.flag = true AND u.pitch = false and " +
		       "(u.dateStart LIKE %:search% OR " +
		       "CAST(u.pitchDetail.pitchType.size AS string) LIKE %:search% OR " +
		       "tr.timeFrom LIKE %:search% OR " +
		       "u.account.fullname LIKE %:search% OR " +
		       "u.description LIKE %:search%)")
	public Page<Match> listMatchSearch(Pageable pageable, @Param("search") String search);
	
	@Query("SELECT u FROM Match u WHERE u.flag = true and u.account.username <> :username")
	public Page<Match> listMatchHome(Pageable pageable,@Param("username") String username);
	
	@Query("SELECT u FROM Match u WHERE u.flag = true and (u.account.username = :username)")
	public Page<Match> listMyMatch(Pageable pageable,@Param("username") String username);

}
