package com.example.demo.dao;

import java.util.List;
import com.example.demo.entity.Account;
import com.example.demo.entity.PitchDetail;

public interface PitchDetailDao {
	public List<PitchDetail> getPitchDetailsByUser(Account account);
}
