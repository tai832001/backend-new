package com.example.demo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.PitchAdminDTO;
import com.example.demo.dto.TournamentAdminDTO;

import jakarta.persistence.TypedQuery;

@Repository
public class TournamentAdminImp implements ITournamentAdminDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<TournamentAdminDTO> getAll() {
		StringBuilder hql = new StringBuilder();
		hql.append("SELECT new com.example.demo.dto.TournamentAdminDTO( ");
		hql.append("	t.iDTournaments,");
		hql.append("	t.nameTouraments,");
		hql.append("	acc.fullname,");
		hql.append("	p.name,");
		hql.append("	t.dateStart,");
		hql.append("	t.dateTo, ");
		hql.append("	t.image, ");
		hql.append("	t.description, ");
		hql.append("	CASE ");
		hql.append("		WHEN t.dateStart > GETDATE() THEN '0' ");
		hql.append("		WHEN t.dateStart < GETDATE() AND t.dateTo > GETDATE() THEN '1' ");
		hql.append("	END as status) ");
		hql.append("FROM ");
		hql.append("	Tournaments t ");
		hql.append("LEFT JOIN ");
		hql.append("	Account acc ");
		hql.append("ON ");
		hql.append("	acc.iDAccount = t.iDAccount ");
		hql.append("LEFT JOIN ");
		hql.append("	Pitch p ");
		hql.append("ON ");
		hql.append("	p.iDPitch = t.iDPitch ");
		hql.append("WHERE ");
		hql.append("	acc.flag = true AND ");
		hql.append("	t.flag = true AND ");
		hql.append("	p.flag = true AND ");
		hql.append("	t.dateTo > GETDATE()");
		Session session = sessionFactory.openSession();
		TypedQuery<TournamentAdminDTO> query = session.createQuery(hql.toString(), TournamentAdminDTO.class);
		List<TournamentAdminDTO> list = query.getResultList();
		session.close();
		return list;
	}
}
