package com.example.demo.dao;

import java.util.List;

import com.example.demo.entity.Account;
import com.example.demo.entity.TournamentTeams;

public interface TournamentTeamDAO {
	public List<TournamentTeams> getTeamInTournament(String id);
	public TournamentTeams checkTeamExistInTournament(String tmId, Account account);
}
