package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.RecentOrderDTO;
import com.example.demo.dto.RevenueStatisticsPitchDTOMapping;
import com.example.demo.entity.Account;

public interface PitchDAO {
	List<RevenueStatisticsPitchDTOMapping> revenueStatisticsPitch(Account account, int year);
	List<RecentOrderDTO> getRecentOrder(int page, Account account);
	Double getTotalRevenue(Account account);
	Long getTotalOrder(Account account);
}
