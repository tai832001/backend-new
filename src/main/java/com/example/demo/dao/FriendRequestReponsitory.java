package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Account;
import com.example.demo.entity.FriendRequest;

public interface FriendRequestReponsitory extends JpaRepository<FriendRequest, Long> {
	List<FriendRequest> findBySender(Account id);
	List<FriendRequest> findByReceiver(Account id);
}
