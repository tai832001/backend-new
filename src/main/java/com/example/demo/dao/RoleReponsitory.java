package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Role;

public interface RoleReponsitory extends JpaRepository<Role, String> {
	Optional<Role> findByAuthority(String authority);
}
 