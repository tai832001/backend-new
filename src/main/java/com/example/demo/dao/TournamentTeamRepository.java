package com.example.demo.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.TournamentTeams;

@Transactional
public interface TournamentTeamRepository extends JpaRepository<TournamentTeams, String> {

    @Query("SELECT u FROM TournamentTeams u WHERE u.iDTournaments = :id and u.status = 'W'")
    Page<TournamentTeams> getByIdTournament(@Param("id") String id, Pageable pageable);
}
