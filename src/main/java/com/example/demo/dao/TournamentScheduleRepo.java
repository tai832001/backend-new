package com.example.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.TournamentSchedule;

public interface TournamentScheduleRepo extends JpaRepository<TournamentSchedule, String>{
	@Modifying
	@Query("UPDATE TournamentSchedule ts SET ts.flag = false WHERE ts.idMatch = :tmsId")
	void deleteTournamentScheduleById(@Param("tmsId") String tmsId);
}
