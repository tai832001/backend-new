package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.dto.MessDTO;
import com.example.demo.entity.Parti;
public interface PartiReponsitory extends JpaRepository<Parti, Integer> {
	
	@Query("SELECT u FROM Parti u WHERE u.account.iDAccount = :id")
	List<Parti> findByAccount(@Param("id") String id);
	
	@Query("SELECT u FROM Parti u WHERE u.conversation.conId = :id")
	List<Parti> findByConver(@Param("id") String id);
	
	@Query("select u.conversation.conId from Parti u where u.account.iDAccount in (:id, :idfriend) group by u.conversation.conId having COUNT(distinct u.account.iDAccount) = 2")
	int findConverByParty(@Param("id") String id ,@Param("idfriend") String idfriend);
	
	@Query("select new com.example.demo.dto.MessDTO(u.id, u.conversation.conId, u.account.iDAccount, COUNT(i.id) as number) from Parti u full join Message i on u.conversation.conId = i.conversation.conId group by u.id, u.conversation.conId, u.account.iDAccount having u.account.iDAccount = :id")
	List<MessDTO> listMess(@Param("id") String id);
}
