package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.GetTeamJoinUserDTO;
import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.dto.ScheduleOfUserDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.TournamentSchedule;

import jakarta.persistence.TypedQuery;

@Repository
public class TournamentScheduleDaoImpl implements TournamentScheduleDao {

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<ListScheduleBasicDTO> getTournamentSchedulesById(String id) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT NEW com.example.demo.dto.ListScheduleBasicDTO( ");
		hql.append("	ts.idMatch as idMatch");
		hql.append("	,ts.tournaments.nameTouraments as nameTouraments");
		hql.append("	,ts.dateStart as dateStart");
		hql.append("	,ts.timeStart as timeStart");
		hql.append("	,ts.team1.iDTeam as idTeam1");
		hql.append("	,ts.team1.nameTeam as nameTeam1");
		hql.append("	,ts.team2.iDTeam as idTeam2");
		hql.append("	,ts.team2.nameTeam as nameTeam2");
		hql.append("	,ts.result1 as result1");
		hql.append("	,ts.result2 as result2");
		hql.append("	,ts.pitchDetail.iDPitchDetail as idPitchDetail");
		hql.append("	,ts.pitchDetail.namePitch as namePitch");
		hql.append(" ) ");
		hql.append(" FROM ");
		hql.append("	TournamentSchedule ts");
		hql.append(" WHERE ");
		hql.append(" 	ts.tournaments.iDTournaments = :tmId");
		hql.append(" 	AND ts.flag = true");
		
		List<ListScheduleBasicDTO> tournamentSchedules = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<ListScheduleBasicDTO> query = session.createQuery(hql.toString(), ListScheduleBasicDTO.class);
			query.setParameter("tmId", id);
			
			tournamentSchedules = query.getResultList();
		}
		
		return tournamentSchedules;
	}

	
	@Override
	public TournamentSchedule save(TournamentSchedule tournamentSchedule) {
		try(Session session = sessionFactory.openSession()) {
			Transaction transaction = session.beginTransaction();
	        session.persist(tournamentSchedule);
	        session.flush();
	        transaction.commit();
		}
		
		return tournamentSchedule;
	}


	@Override
	public ListScheduleBasicDTO getTournamentScheduleByMatchId(String matchId) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT NEW com.example.demo.dto.ListScheduleBasicDTO( ");
		hql.append("	ts.idMatch as idMatch");
		hql.append("	,ts.tournaments.nameTouraments as nameTouraments");
		hql.append("	,ts.dateStart as dateStart");
		hql.append("	,ts.timeStart as timeStart");
		hql.append("	,ts.team1.iDTeam as idTeam1");
		hql.append("	,ts.team1.nameTeam as nameTeam1");
		hql.append("	,ts.team2.iDTeam as idTeam2");
		hql.append("	,ts.team2.nameTeam as nameTeam2");
		hql.append("	,ts.result1 as result1");
		hql.append("	,ts.result2 as result2");
		hql.append("	,ts.pitchDetail.iDPitchDetail as idPitchDetail");
		hql.append("	,ts.pitchDetail.namePitch as namePitch");
		hql.append(" ) ");
		hql.append(" FROM ");
		hql.append("	TournamentSchedule ts");
		hql.append(" WHERE ");
		hql.append(" 	ts.idMatch = :matchId");
		hql.append(" 	AND ts.flag = true");
		
		ListScheduleBasicDTO tournamentSchedules = new ListScheduleBasicDTO();
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<ListScheduleBasicDTO> query = session.createQuery(hql.toString(), ListScheduleBasicDTO.class);
			query.setParameter("matchId", matchId);
			
			tournamentSchedules = query.getSingleResult();
		}catch (Exception e) {
			// TODO: handle exception
		}
		
		return tournamentSchedules;
	}


	@Override
	public List<ScheduleOfUserDTO> getScheduleOfUser(String tmId, Account account) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT NEW com.example.demo.dto.ScheduleOfUserDTO( ");
		hql.append("	tc.idMatch,");
		hql.append("	tc.dateStart, ");
		hql.append("	tc.timeStart, ");
		hql.append("	tc.pitchDetail.namePitch, ");
		hql.append("	tc.team1.nameTeam, ");
		hql.append("	tc.team2.nameTeam, ");
		hql.append("	tc.result1, ");
		hql.append("	tc.result2");
		hql.append(" ) ");
		hql.append(" FROM ");
		hql.append("	TournamentSchedule tc");
		hql.append(" WHERE ");
		hql.append(" 	tc.tournaments.iDTournaments = :tmId");
		hql.append("	AND (tc.team1.account.username = :us OR tc.team2.account.username = :us)");
		hql.append(" ORDER BY tc.dateStart");
		
		List<ScheduleOfUserDTO> rs = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<ScheduleOfUserDTO> query = session.createQuery(hql.toString(), ScheduleOfUserDTO.class);
			query.setParameter("tmId", tmId);
			query.setParameter("us", account.getUsername());
			
			rs = query.getResultList();
		}
		
		
		return rs;
	}


	@Override
	public List<GetTeamJoinUserDTO> getTeamJoin(String tmId, Account account) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT new com.example.demo.dto.GetTeamJoinUserDTO( ");
		hql.append("	tt.teams.nameTeam as nameTeam, ");
		hql.append("	tt.teams.account.fullname as fullname, ");
		hql.append("	tt.teams.account.phone as phone, ");
		hql.append(" 	rs.wins as wins, ");
		hql.append(" 	rs.looses as looses, ");
		hql.append(" 	rs.draws as draws, ");
		hql.append(" 	tt.status as statusTeam");
		hql.append(" ) ");
		hql.append(" FROM ( ");
		hql.append(" 	SELECT ");
		hql.append(" 		SUM( ");
		hql.append(" 			CASE ");
		hql.append(" 				WHEN (ts.idTeam1 = tt.iDTeam AND ts.result1 > ts.result2) OR (ts.idTeam2 = tt.iDTeam AND ts.result2 > ts.result1) THEN 1 ");
		hql.append(" 				ELSE 0 ");
		hql.append(" 			END ");
		hql.append(" 		) AS wins, ");
		hql.append(" 		SUM( ");
		hql.append(" 			CASE ");
		hql.append(" 				WHEN (ts.idTeam1 = tt.iDTeam AND ts.result1 < ts.result2) OR (ts.idTeam2 = tt.iDTeam AND ts.result2 < ts.result1) THEN 1 ");
		hql.append(" 				ELSE 0 ");
		hql.append(" 			END ");
		hql.append(" 		) AS looses, ");
		hql.append(" 		SUM( ");
		hql.append(" 			CASE ");
		hql.append(" 				WHEN (ts.idTeam1 = tt.iDTeam AND ts.result1 = ts.result2) OR (ts.idTeam2 = tt.iDTeam AND ts.result2 = ts.result1) THEN 1 ");
		hql.append(" 				ELSE 0 ");
		hql.append(" 			END ");
		hql.append(" 		) AS draws ");
		hql.append(" 		, tt.iDTeam as idteam ");
		hql.append(" 	FROM ");
		hql.append(" 		TournamentSchedule ts ");
		hql.append(" 		right join TournamentTeams tt on ts.tournaments.iDTournaments = tt.tournaments.iDTournaments ");
		hql.append(" 	WHERE ");
		hql.append("		ts.tournaments.iDTournaments = :tmId");
		hql.append(" 	GROUP BY ");
		hql.append(" 		tt.iDTeam ");
		hql.append(" ) as rs right join TournamentTeams tt on rs.idteam = tt.teams.iDTeam ");
		hql.append(" WHERE tt.tournaments.iDTournaments = :tmId");
		
		List<GetTeamJoinUserDTO> rs = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession()) {
			TypedQuery<GetTeamJoinUserDTO> query = session.createQuery(hql.toString(), GetTeamJoinUserDTO.class);
			query.setParameter("tmId", tmId);
			
			rs = query.getResultList();
		}
		
		
		return rs;
	}
}
