package com.example.demo.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.demo.entity.Message;


public interface MessReponsitory extends JpaRepository<Message, Long> {
	
	@Query("SELECT u FROM Message u WHERE u.conversation.conId = :id")
	List<Message> findMessByConverID(@Param("id") String id);
	
	@Query("SELECT u FROM Message u WHERE u.conversation.conId = :id ORDER BY u.id DESC")
	public Page<Message> findMessByConverIDPage(Pageable pageable, @Param("id") String id);
}
