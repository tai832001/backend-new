package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Account;
import com.example.demo.entity.Friend;

public interface FriendReponsitory extends JpaRepository<Friend, Long> {
	public List<Friend> findByUser(Account id);
	
	@Query("select u.user.iDAccount FROM Friend u join Account i on u.friend.iDAccount = i.iDAccount where i.username = :username union select u.friend.iDAccount from Friend u join Account i on u.user.iDAccount = i.iDAccount where i.username = :username ")
	public List<String> findFriendByUser(@Param("username") String username);
}
