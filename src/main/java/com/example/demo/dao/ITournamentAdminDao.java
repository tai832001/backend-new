package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.TournamentAdminDTO;

public interface ITournamentAdminDao {

	public List<TournamentAdminDTO> getAll();

}
