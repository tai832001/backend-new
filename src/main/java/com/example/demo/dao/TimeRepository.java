package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.TimeRange;

@Repository
@Transactional
public interface TimeRepository extends JpaRepository<TimeRange, String>{
    @Query("SELECT t FROM TimeRange t ORDER BY t.timeFrom ASC")
    List<TimeRange> getAll ();
}
