package com.example.demo.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Teams;

public interface TeamReponsitory extends JpaRepository<Teams, String> {
    
    @Query("SELECT t FROM Teams t WHERE t.iDAccount = :id AND flag = true")
    List<Teams> getTeamByUserId(@Param("id") String id);
    
    @Query("SELECT t FROM Teams t WHERE t.iDTeam = :id")
    Teams getTeamById(@Param("id") String id);
	
	@Query("SELECT t FROM Teams t WHERE t.account.username = :us AND t.flag = true")
	List<Teams> getTeamByUser(@Param("us") String us);
	
	@Query("SELECT t FROM Teams t WHERE t.iDTeam = :tId")
	Teams getById(@Param("tId") String id);
	
	@Query("SELECT DISTINCT tt.teams FROM TournamentTeams tt " +
	           "JOIN tt.tournaments t " +
	           "JOIN tt.teams " +
	           "WHERE t.iDTournaments = :tournamentId AND tt.status = 'A'")
	Page<Teams> getTeamsByTournamentID(@Param("tournamentId") String tournamentId, Pageable pageable);
}
