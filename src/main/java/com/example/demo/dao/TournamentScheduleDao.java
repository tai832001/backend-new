package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.GetTeamJoinUserDTO;
import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.dto.ScheduleOfUserDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.TournamentSchedule;

public interface TournamentScheduleDao {
	public List<ListScheduleBasicDTO> getTournamentSchedulesById(String id);
	public TournamentSchedule save(TournamentSchedule tournamentSchedule);
	public ListScheduleBasicDTO getTournamentScheduleByMatchId(String matchId);
	public List<ScheduleOfUserDTO> getScheduleOfUser(String tmId, Account account);
	public List<GetTeamJoinUserDTO> getTeamJoin(String tmId, Account account);
}
