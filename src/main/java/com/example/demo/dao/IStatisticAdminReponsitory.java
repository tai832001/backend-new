package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.StatisticAdminDTO;

public interface IStatisticAdminReponsitory {

	public List<StatisticAdminDTO> getTop3Highest(String month, String year);
	
	public List<StatisticAdminDTO> getTop3Lowest(String month, String year);
}
