package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Pitch;

@Transactional
public interface PitchRepository extends JpaRepository<Pitch, String> {

	@Modifying
	@Query("UPDATE Pitch SET flag = false WHERE iDPitch = :iDPitch")
	public void deletePitch(@Param(value = "iDPitch") String iDPitch);

	@Query("SELECT u,u.district FROM Pitch u WHERE u.iDDistrict = :id")
	List<Pitch> findByIDDistrict(@Param("id") String iDDistrict);

	@Query("SELECT u FROM Pitch u")
	List<Pitch> findAllQuery();

	@Query("SELECT u FROM Pitch u WHERE u.idOwner = :id and u.flag = true")
	List<Pitch> getPitchByCurentUserId(@Param("id") String id);
	
	@Query("SELECT DISTINCT a FROM Pitch a INNER JOIN a.listPitchDetail b INNER JOIN b.listPrice c WHERE b.flag = true AND a.flag = true AND a.iDDistrict = :districtId")
    List<Pitch> findDistinctByDistrictId(@Param("districtId") String districtId);


}
