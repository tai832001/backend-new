package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.PitchAdminDTO;
import com.example.demo.dto.PitchExcelDTO;

public interface IPitchAdminReponsitory  {

	public List<PitchAdminDTO> getPitchFromDistrict(String idDistrict);
	
	public List<PitchExcelDTO> getPitchById(String iDPitch);
}
