package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Account;

@Repository
@Transactional
public interface UserReponsitory extends JpaRepository<Account, String> {
	Optional<Account> findByUsername(String username);

	Optional<Account> findByEmail(String email);

	Optional<Account> findByPhone(String phone);

	@Query("SELECT acc FROM Account acc " + "WHERE NOT EXISTS (" + "SELECT 1 " + "FROM Friend f "
			+ "   JOIN f.user u " + "JOIN f.friend fr "
			+ "   WHERE (u.iDAccount = :accountId AND fr.iDAccount = acc.iDAccount) OR (u.iDAccount = acc.iDAccount AND fr.iDAccount = :accountId)"
			+ ") and acc.iDAccount <> :accountId AND acc.flag = true AND acc.fullname LIKE CONCAT('%', :fullname, '%')")
	List<Account> searchFriend(@Param("accountId") String accountId, @Param("fullname") String fullname);

	@Query("SELECT u FROM Account u WHERE u.flag = true")
	List<Account> getAccount();

	@Modifying
	@Query("UPDATE Account SET flag = false WHERE iDAccount = :iDAccount")
	public void deleteAccount(@Param(value = "iDAccount") String iDAccount);

	@Query("select u from Account u where u.flag = true")
	List<Account> getAllAccount();
}
