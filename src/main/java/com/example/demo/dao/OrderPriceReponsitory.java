package com.example.demo.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.entity.OrderPrice;

public interface OrderPriceReponsitory extends JpaRepository<OrderPrice, Long> {
	@Query("SELECT op FROM OrderPrice op WHERE op.orderPitch.flag = true and op.orderPitch.status <> 3 AND (op.date BETWEEN :startDate AND :endDate)")
	List<OrderPrice> getOrderPricesBetweenDates(LocalDate startDate, LocalDate endDate);
}
