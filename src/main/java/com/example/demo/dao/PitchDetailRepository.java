package com.example.demo.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.PitchDetail;

@Repository
@Transactional
public interface PitchDetailRepository extends JpaRepository<PitchDetail, String> {

    @Query("SELECT u FROM PitchDetail u WHERE u.iDPitch = :id and u.flag = true")
    List<PitchDetail> getPitchDetailByPitchId(@Param("id") String id);
    
    @Query("SELECT u,u.pitchType,u.pitch FROM PitchDetail u WHERE u.iDPitchDetail = :id")
    PitchDetail getPitchDetailByPitchDetail(@Param("id") String id);

    @Query("SELECT u FROM PitchDetail u WHERE u.iDPitch = :id and u.flag = true and u.namePitch LIKE %:search%")
    Page<PitchDetail> getPagePitchDetailByPitchId(@Param("id") String id, @Param("search") String search, Pageable pageable);

    
    @Query("SELECT DISTINCT a FROM PitchDetail a INNER JOIN a.listPrice WHERE a.flag = true AND a.pitch.iDPitch = :id")
    List<PitchDetail> getPitchDetailByPitchIdDistinct(@Param("id") String id);
    

    @Query("SELECT lpd FROM Tournaments tm JOIN tm.pitch p JOIN p.listPitchDetail lpd WHERE tm.iDTournaments = :tournamentId AND lpd.flag = true")
    List<PitchDetail> getByTournamentId(@Param("tournamentId") String tournamentId);
}
