package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.dto.RecentOrderDTO;
import com.example.demo.dto.RevenueStatisticsPitchDTOMapping;
import com.example.demo.entity.Account;

import jakarta.persistence.TypedQuery;

@Repository
public class PitchDAOImpl implements PitchDAO {
	
	public static final int ORDER_SUCCESS = 4;
	public static final int PAGE_SIZE_RECENT_ORDER = 10;

	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<RevenueStatisticsPitchDTOMapping> revenueStatisticsPitch(Account account, int year) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT NEW com.example.demo.dto.RevenueStatisticsPitchDTOMapping(");
		hql.append("    p.iDPitch as iDPitch, ");
		hql.append("    rs.dateOrder as dateOrder, ");
		hql.append("    rs.totalPrice as totalPrice, ");
		hql.append("    p.name as pitchName, ");
		hql.append("    rs.numOrder as numOrder ");
		hql.append(" ) ");
		hql.append(" FROM Pitch p ");
		hql.append(" LEFT JOIN  ( ");
		hql.append("    SELECT opr.price.pitchDetail.pitch.iDPitch as iDPitch, ");
		hql.append(
				"        DATEADD(MONTH, DATEDIFF(MONTH, CAST('1900-01-01' as date), CAST(opr.date as date)), CAST('1900-01-01' as date)) AS dateOrder,    ");
		hql.append("	    SUM(opr.price.price) as totalPrice,  ");
		hql.append("	    COUNT(1) as numOrder  ");
		hql.append("    FROM     ");
		hql.append("        OrderPrice opr ");
		hql.append("    WHERE ");
		hql.append("		opr.orderPitch.status = :status");
		hql.append("        AND opr.price.pitchDetail.pitch.account.username = :us ");
		hql.append("        AND DATEPART(year, opr.orderPitch.dateOrder) = :year");
		hql.append("    GROUP BY  ");
		hql.append(
				"        DATEADD(MONTH, DATEDIFF(MONTH, CAST('1900-01-01' as date), CAST(opr.date as date)), CAST('1900-01-01' as date)), ");
		hql.append("        opr.price.pitchDetail.pitch.iDPitch ");
		hql.append(" ) as rs ON p.iDPitch = rs.iDPitch  ");
		hql.append(" WHERE p.account.username = :us ");

		List<RevenueStatisticsPitchDTOMapping> rs = new ArrayList<>();

		try (Session session = sessionFactory.openSession()) {
			TypedQuery<RevenueStatisticsPitchDTOMapping> query = session.createQuery(hql.toString(),
					RevenueStatisticsPitchDTOMapping.class);
			query.setParameter("us", account.getUsername());
			query.setParameter("year", year);
			query.setParameter("status", ORDER_SUCCESS);

			rs = query.getResultList();
		}

		return rs;
	}

	@Override
	public List<RecentOrderDTO> getRecentOrder(int page, Account account) {
		StringBuilder hql = new StringBuilder();

		hql.append("SELECT ");
		hql.append("	NEW com.example.demo.dto.RecentOrderDTO( ");
		hql.append("		opr.orderPitch.iDOrder as idOrder, ");
		hql.append("        opr.orderPitch.account.imageAccount as imageAccount, ");
		hql.append("        opr.orderPitch.account.fullname as fullname , ");
		hql.append("        opr.orderPitch.account.phone as phone, ");
		hql.append("        opr.price.pitchDetail.pitch.name as pitchName, ");
		hql.append("        opr.price.pitchDetail.namePitch as pitchDetailName, ");
		hql.append("        opr.orderPitch.status as status, ");
		hql.append("        opr.date as dateStart, ");
		hql.append("        opr.price.timeRange.timeFrom as timeFrom, ");
		hql.append("        opr.price.timeRange.timeTo as timeTo, ");
		hql.append("        opr.price.price as price, ");
		hql.append("        COUNT(opr.id) OVER() as numRecords ");
		hql.append("    ) ");
		hql.append("FROM ");
		hql.append("	OrderPrice opr ");
		hql.append("WHERE opr.price.pitchDetail.pitch.account.username = :us ");
		hql.append("ORDER BY opr.orderPitch.dateStart desc, opr.price.timeRange.timeFrom desc, opr.price.timeRange.timeTo desc  ");

		List<RecentOrderDTO> rs = new ArrayList<>();

		try (Session session = sessionFactory.openSession()) {
			TypedQuery<RecentOrderDTO> query = session.createQuery(hql.toString(), RecentOrderDTO.class);
			query.setParameter("us", account.getUsername());

			query.setFirstResult((page - 1) * PAGE_SIZE_RECENT_ORDER);
			query.setMaxResults(PAGE_SIZE_RECENT_ORDER);

			rs = query.getResultList();
		}

		return rs;
	}

	@Override
	public Double getTotalRevenue(Account account) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT ");
		hql.append(" 	sum(opr.price.price) ");
		hql.append(" FROM ");
		hql.append(" 	OrderPrice opr ");
		hql.append(" WHERE opr.orderPitch.status = :status ");
		hql.append("	AND opr.price.pitchDetail.pitch.account.username = :us ");

		Double rs;

		try (Session session = sessionFactory.openSession()) {
			TypedQuery<Double> query = session.createQuery(hql.toString(), Double.class);
			query.setParameter("us", account.getUsername());
			query.setParameter("status", ORDER_SUCCESS);

			rs = query.getSingleResult();
		}

		return rs;
	}

	@Override
	public Long getTotalOrder(Account account) {
		StringBuilder hql = new StringBuilder();

		hql.append(" SELECT ");
		hql.append(" 	COUNT(opr.id) ");
		hql.append(" FROM ");
		hql.append(" 	OrderPitch op ");
		hql.append(" JOIN op.orderPrices as opr ");
		hql.append(" WHERE ");
		hql.append("	op.status = :status");
		hql.append("	AND opr.price.pitchDetail.pitch.account.username = :us");

		Long rs;

		try (Session session = sessionFactory.openSession()) {
			TypedQuery<Long> query = session.createQuery(hql.toString(), Long.class);
			query.setParameter("us", account.getUsername());
			query.setParameter("status", ORDER_SUCCESS);

			rs = query.getSingleResult();
		}
		return rs;
	}
}
