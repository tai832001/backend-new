package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.OrderPitchDTO;
import com.example.demo.entity.Account;

import jakarta.persistence.TypedQuery;

@Repository
@Transactional
public class OrderPitchDaoImpl implements ManageFieldDao {
	
	public static final int DELETED = 3;
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public List<OrderPitchDTO> getByUser(Account account) {
		StringBuilder hql = new StringBuilder();
		hql.append(" SELECT new com.example.demo.dto.OrderPitchDTO( ");
		hql.append(" 	opr.orderPitch.iDOrder as iDOrder, ");
		hql.append("	opr.date as dateStart, ");
		hql.append("	opr.price.timeRange.timeFrom as timeFrom, ");
		hql.append("	opr.price.timeRange.timeTo as timeTo, ");
		hql.append("    opr.price.pitchDetail.namePitch as namePitch, ");
		hql.append("    opr.price.pitchDetail.iDPitchDetail as idPitchDetail, ");
		hql.append("    opr.orderPitch.account.fullname fullname, ");
		hql.append("	opr.orderPitch.status ");
		hql.append(" ) ");
		hql.append(" FROM OrderPrice AS opr ");
		hql.append(" WHERE ");
		hql.append(" 	opr.orderPitch.flag = true ");
		hql.append(" 	AND opr.price.pitchDetail.pitch.account.username = :username ");
		hql.append("	AND opr.orderPitch.status <> :status ");
		
		List<OrderPitchDTO> rs = new ArrayList<>();
		
		try(Session session = sessionFactory.openSession();) {
			TypedQuery<OrderPitchDTO> query = session.createQuery(hql.toString(), OrderPitchDTO.class);
			query.setParameter("username", account.getUsername());
			query.setParameter("status", DELETED);
			
			rs = query.getResultList();
		}
		
		return rs;
	}

}
