package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Event;

@Transactional
public interface EventReponsitory extends JpaRepository<Event, String>{

	@Query("SELECT u FROM Event u where u.flag = true")
	public List<Event> getAllByTrue();
	
	@Query("SELECT u FROM Event u where u.flag = true and u.outstanding = true")
	public List<Event> getOutstanding();
	
	@Modifying
	@Query("UPDATE Event SET flag = false where iDEvent = :iDEvent")
	public void deleteEvent(String iDEvent);
}
