package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.entity.Price;

public interface PriceRepository extends JpaRepository<Price, String> {
    @Query("SELECT pr, pr.timeRange FROM Price pr INNER JOIN pr.pitchDetail pd INNER JOIN pd.pitch p INNER JOIN p.account a WHERE pr.flag = true AND a.iDAccount = :id ORDER BY pr.timeRange.timeFrom ASC")
    List<Price> getByAccountID(@Param("id") String id);
    
    @Query("SELECT pr, pr.timeRange FROM Price pr INNER JOIN pr.pitchDetail pd INNER JOIN pd.pitch p WHERE pr.flag = true and pd.iDPitchDetail = :idpd and p.iDPitch = :idp ORDER BY pr.timeRange.timeFrom ASC")
    List<Price> getByPitch(@Param("idpd") String idpd,@Param("idp") String idp);
    @Query("SELECT pr, pr.timeRange FROM Price pr WHERE pr.flag = true AND pr.iDPitchDetail = :id ORDER BY pr.timeRange.timeFrom ASC")
    List<Price> getByPitchDetailID(@Param("id") String id);
}
