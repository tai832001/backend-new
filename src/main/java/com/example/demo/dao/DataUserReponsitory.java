package com.example.demo.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.DataUser;

@Repository
public interface DataUserReponsitory extends JpaRepository<DataUser, Long> {
	Optional<DataUser> findByEmail(String email);
	Optional<DataUser> findByUsername(String username);
}
