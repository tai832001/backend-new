package com.example.demo.dao;

import java.util.List;

import com.example.demo.dto.OrderPitchDTO;
import com.example.demo.entity.Account;

public interface ManageFieldDao {
	public List<OrderPitchDTO> getByUser(Account account);
}
