package com.example.demo.dao;

import java.util.List;

import com.example.demo.controller.GetTournamentsByFilterRespDTO;
import com.example.demo.entity.Tournaments;

public interface TournamentsDao {
	public List<GetTournamentsByFilterRespDTO> getTournamentsByFilter(String location, String conditons, int page);
}
