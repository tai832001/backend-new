package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.controller.GetTournamentUserJoinDTO;
import com.example.demo.entity.Tournaments;

public interface TournamentRepository extends JpaRepository<Tournaments, String>{
    @Query("SELECT u FROM Tournaments u WHERE u.iDAccount = :id and u.flag = true")
    List<Tournaments> getByAccountID(@Param("id") String id);
    
    @Query("SELECT "
    		+ "new com.example.demo.controller.GetTournamentUserJoinDTO("
    		+ "	t.iDTournaments, "
    		+ "	tt.status as statusTeam, "
    		+ "	t.dateStart, "
    		+ "	t.dateTo, "
    		+ "	t.nameTouraments, "
    		+ " tt.teams.nameTeam as nameTeam, "
    		+ " t.flag as flagTournament"
    		+ ") FROM Tournaments t "
    		+ "JOIN tournamentTeams tt "
    		+ "WHERE "
    		+ "	tt.teams.account.iDAccount = :id "
    		+ "	ORDER BY "
    		+ " CASE "
    		+ " 	WHEN t.dateTo >= current_date() THEN 0 "
    		+ "		ELSE 1"
    		+ " END, "
    		+ " CASE "
    		+ "		WHEN t.dateTo < current_date() THEN t.dateTo"
    		+ "		ELSE NULL "
    		+ " END DESC, "
    		+ " CASE "
    		+ "		WHEN t.dateTo >= current_date() THEN t.dateStart"
    		+ " 	ELSE NULL"
    		+ " END ASC ")
    List<GetTournamentUserJoinDTO> getTournamentUserJoin(@Param("id") String id);
    
    @Query("SELECT u FROM Tournaments u WHERE u.iDTournaments = :id")
    Tournaments getById(@Param("id") String id);
    
    @Modifying
    @Query("UPDATE Tournaments u SET u.flag = false WHERE u.iDTournaments = :iDTournaments")
    public void deleteTournament(@Param("iDTournaments") String iDTournaments);
}
