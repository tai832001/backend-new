package com.example.demo.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dto.StatisticAdminDTO;

import jakarta.persistence.TypedQuery;

@Repository
@Transactional
public class StatisticAdminReponsitory implements IStatisticAdminReponsitory{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public List<StatisticAdminDTO> getTop3Highest(String month, String year) {
	    Session session = sessionFactory.openSession();
	    TypedQuery<StatisticAdminDTO> query = session.createQuery(
	        "SELECT new com.example.demo.dto.StatisticAdminDTO(" +
	        "    p.name, " +
	        "    acc.fullname, " +
	        "    acc.iDAccount, " +
	        "    p.address, " +
	        "    d.nameDistrict, " +
	        "    SUM(pr.price) AS total) " +
	        "FROM Pitch p " +
	        "LEFT JOIN Account acc ON acc.iDAccount = p.idOwner " +
	        "LEFT JOIN District d ON d.iDDistrict = p.iDDistrict " +
	        "LEFT JOIN PitchDetail pd ON p.iDPitch = pd.iDPitch " +
	        "LEFT JOIN Price pr ON pr.iDPitchDetail = pd.iDPitchDetail " +
	        "LEFT JOIN OrderPrice op ON op.price.iDPrice = pr.iDPrice " +
	        "WHERE " +
	        "    acc.flag = true AND " +
	        "    d.flag = true AND " +
	        "    pd.flag = true AND " +
	        "    pr.flag = true AND " +
	        "    MONTH(op.date) = :month AND " +
	        "    YEAR(op.date) = :year " +
	        "GROUP BY " +
	        "    p.name, " +
	        "    acc.fullname, " +
	        "    p.address, " +
	        "    d.nameDistrict, " +
	        "    acc.iDAccount " +
	        "ORDER BY " +
	        "    SUM(pr.price) DESC", StatisticAdminDTO.class);
	    
	    query.setParameter("month", month);
	    query.setParameter("year", year);
	    query.setMaxResults(3); // Limit to top 3 results
	    List<StatisticAdminDTO> list = query.getResultList(); // Execute the query
	    session.close();
	    return list;
	}
	
	@Override
	public List<StatisticAdminDTO> getTop3Lowest(String month, String year) {
	    Session session = sessionFactory.openSession();
	    TypedQuery<StatisticAdminDTO> query = session.createQuery(
	        "SELECT new com.example.demo.dto.StatisticAdminDTO(" +
	        "    p.name, " +
	        "    acc.fullname, " +
	        "    acc.iDAccount, " +
	        "    p.address, " +
	        "    d.nameDistrict, " +
	        "    SUM(pr.price) AS total) " +
	        "FROM Pitch p " +
	        "LEFT JOIN Account acc ON acc.iDAccount = p.idOwner " +
	        "LEFT JOIN District d ON d.iDDistrict = p.iDDistrict " +
	        "LEFT JOIN PitchDetail pd ON p.iDPitch = pd.iDPitch " +
	        "LEFT JOIN Price pr ON pr.iDPitchDetail = pd.iDPitchDetail " +
	        "LEFT JOIN OrderPrice op ON op.price.iDPrice = pr.iDPrice " +
	        "WHERE " +
	        "    acc.flag = true AND " +
	        "    d.flag = true AND " +
	        "    pd.flag = true AND " +
	        "    pr.flag = true AND " +
	        "    MONTH(op.date) = :month AND " +
	        "    YEAR(op.date) = :year " +
	        "GROUP BY " +
	        "    p.name, " +
	        "    acc.fullname, " +
	        "    p.address, " +
	        "    d.nameDistrict, " +
	        "    acc.iDAccount " +
	        "ORDER BY " +
	        "    SUM(pr.price) ASC", StatisticAdminDTO.class);
	    
	    query.setParameter("month", month);
	    query.setParameter("year", year);
	    query.setMaxResults(3); // Limit to top 3 results
	    List<StatisticAdminDTO> list = query.getResultList(); // Execute the query
	    session.close();
	    return list;
	}
}
