package com.example.demo.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "Conversation")
public class Conversation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int conId;
	private String conversationName;
	private LocalDateTime createAt;
	public Conversation() {
		// TODO Auto-generated constructor stub
	}
	public Conversation( String conversationName, LocalDateTime createAt) {
		super();
		this.conversationName = conversationName;
		this.createAt = createAt;
	}
	public int getConId() {
		return conId;
	}
	public void setConId(int conId) {
		this.conId = conId;
	}
	public String getConversationName() {
		return conversationName;
	}
	public void setConversationName(String conversationName) {
		this.conversationName = conversationName;
	}
	public LocalDateTime getCreateAt() {
		return createAt;
	}
	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}
	
	
	
}
