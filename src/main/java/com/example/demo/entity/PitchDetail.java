package com.example.demo.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PitchDetail")
public class PitchDetail {

    @Id
    @Column(name = "IDPitchDetail", columnDefinition = "varchar(10)", unique = true, nullable = true)
    @GenericGenerator(name = "sequence_pitchdetail_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "PD"), @Parameter(name = "PK_Str", value = "iDPitchDetail")
    })
    @GeneratedValue(generator = "sequence_pitchdetail_id")
    private String iDPitchDetail;

    @Column(name = "IDPitch", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String iDPitch;

    @Column(name = "IDPitchType", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String iDPitchType;

    @Column(name = "NamePitch", columnDefinition = "nvarchar(10)", unique = false, nullable = true)
    private String namePitch;

    @Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
    private boolean flag;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDPitch", insertable = false, updatable = false)
    private Pitch pitch;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDPitchType", insertable = false, updatable = false)
    private PitchType pitchType;

    @OneToMany(mappedBy = "pitchDetail", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Price> listPrice;
    
    public PitchDetail() {
		// TODO Auto-generated constructor stub
	}
    
    

    public PitchDetail(String iDPitchDetail, String iDPitch, String iDPitchType, String namePitch, boolean flag,
			Pitch pitch, PitchType pitchType, List<Price> listPrice) {
		super();
		this.iDPitchDetail = iDPitchDetail;
		this.iDPitch = iDPitch;
		this.iDPitchType = iDPitchType;
		this.namePitch = namePitch;
		this.flag = flag;
		this.pitch = pitch;
		this.pitchType = pitchType;
		this.listPrice = listPrice;
	}

	public String getiDPitchDetail() {
        return iDPitchDetail;
    }

    public void setiDPitchDetail(String iDPitchDetail) {
        this.iDPitchDetail = iDPitchDetail;
    }

    public String getiDPitch() {
        return iDPitch;
    }

    public void setiDPitch(String iDPitch) {
        this.iDPitch = iDPitch;
    }

    public String getiDPitchType() {
        return iDPitchType;
    }

    public void setiDPitchType(String iDPitchType) {
        this.iDPitchType = iDPitchType;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public void setPitch(Pitch pitch) {
        this.pitch = pitch;
    }

    public PitchType getPitchType() {
        return pitchType;
    }

    public void setPitchType(PitchType pitchType) {
        this.pitchType = pitchType;
    }

    public List<Price> getListPrice() {
        return listPrice;
    }

    public void setListPrice(List<Price> listPrice) {
        this.listPrice = listPrice;
    }

    public String getNamePitch() {
        return namePitch;
    }

    public void setNamePitch(String namePitch) {
        this.namePitch = namePitch;
    }

	@Override
	public String toString() {
		return "PitchDetail [iDPitchDetail=" + iDPitchDetail + ", iDPitch=" + iDPitch + ", iDPitchType=" + iDPitchType
				+ ", namePitch=" + namePitch + ", flag=" + flag + ", pitch=" + pitch + ", pitchType=" + pitchType
				+ ", listPrice=" + listPrice + "]";
	}
}
