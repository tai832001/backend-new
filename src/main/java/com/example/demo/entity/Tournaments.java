package com.example.demo.entity;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Tournaments")
public class Tournaments {

	@Id
	@Column(name = "IDTournaments", columnDefinition = "varchar(10)",unique = true,nullable = true)
    @GenericGenerator(name = "sequence_tournaments_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "TM"), @Parameter(name = "PK_Str", value = "iDTournaments")
    })
    @GeneratedValue(generator = "sequence_tournaments_id")
	private String iDTournaments;
	
	@Column(name = "IDPitch", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDPitch;
	
	@Column(name = "IDAccount", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDAccount;
	
	@Column(name = "NameTouraments", columnDefinition = "nvarchar(100)",unique = false,nullable = true)
	private String nameTouraments;
	
	@Column(name = "Quality", columnDefinition = "int",unique = false,nullable = true)
	private int quality;
	
	@Column(name = "DateStart", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dateStart;
	
	@Column(name = "DateTo", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dateTo;
	
	@Column(name = "Fees", columnDefinition = "decimal",unique = false,nullable = true)
	private float fees;
	
	@Column(name = "Description", columnDefinition = "nvarchar(max)",unique = false,nullable = true)
	private String description;
	
	@Column(name = "LimitAge", columnDefinition = "int",unique = false,nullable = true)
	private int limitAge;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	private String image;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPitch", insertable = false, updatable = false)
	private Pitch pitch;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDAccount",insertable = false, updatable = false)
	private Account account;
	
	@OneToMany(mappedBy = "tournaments",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<TournamentTeams> tournamentTeams;

    public Tournaments(String iDTournaments, String iDPitch, String iDAccount, String nameTouraments, int quality,
            LocalDate dateStart, LocalDate dateTo, float fees, String description, int limitAge, boolean flag,
            String image, Pitch pitch, Account account, List<TournamentTeams> tournamentTeams) {
        this.iDTournaments = iDTournaments;
        this.iDPitch = iDPitch;
        this.iDAccount = iDAccount;
        this.nameTouraments = nameTouraments;
        this.quality = quality;
        this.dateStart = dateStart;
        this.dateTo = dateTo;
        this.fees = fees;
        this.description = description;
        this.limitAge = limitAge;
        this.flag = flag;
        this.image = image;
        this.pitch = pitch;
        this.account = account;
        this.tournamentTeams = tournamentTeams;
    }

    public Tournaments() {
    }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getiDTournaments() {
        return iDTournaments;
    }

    public void setiDTournaments(String iDTournaments) {
        this.iDTournaments = iDTournaments;
    }

    public String getiDPitch() {
        return iDPitch;
    }

    public void setiDPitch(String iDPitch) {
        this.iDPitch = iDPitch;
    }

    public String getiDAccount() {
        return iDAccount;
    }

    public void setiDAccount(String iDAccount) {
        this.iDAccount = iDAccount;
    }

    public String getNameTouraments() {
        return nameTouraments;
    }

    public void setNameTouraments(String nameTouraments) {
        this.nameTouraments = nameTouraments;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public LocalDate getDateStart() {
        return dateStart;
    }

    public void setDateStart(LocalDate dateStart) {
        this.dateStart = dateStart;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public float getFees() {
        return fees;
    }

    public void setFees(float fees) {
        this.fees = fees;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLimitAge() {
        return limitAge;
    }

    public void setLimitAge(int limitAge) {
        this.limitAge = limitAge;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public void setPitch(Pitch pitch) {
        this.pitch = pitch;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<TournamentTeams> getTournamentTeams() {
        return tournamentTeams;
    }

    public void setTournamentTeams(List<TournamentTeams> tournamentTeams) {
        this.tournamentTeams = tournamentTeams;
    }
	
	
}
