package com.example.demo.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Teams")

public class Teams {
    @Id
    @Column(name = "IDTeam", columnDefinition = "varchar(10)", unique = true, nullable = true)
    @GenericGenerator(name = "sequence_teams_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "T"), @Parameter(name = "PK_Str", value = "iDTeam")
    })
    @GeneratedValue(generator = "sequence_teams_id")
    private String iDTeam;

    @Column(name = "IDAccount", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String iDAccount;

    @Column(name = "NameTeam", columnDefinition = "nvarchar(50)", unique = false, nullable = true)
    private String nameTeam;

    @Column(name = "QualityPlayer", columnDefinition = "int", unique = false, nullable = true)
    private int qualityPlayer;

    @Column(name = "IDGroup", columnDefinition = "varchar(5)", unique = false, nullable = true)
    private String iDGroup;

    @Column(name = "Point", columnDefinition = "int", unique = false, nullable = true)
    private int point;

    @Column(name = "GoalDifference", columnDefinition = "int", unique = false, nullable = true)
    private int goalDifference;

    @Column(name = "Goal", columnDefinition = "int", unique = false, nullable = true)
    private int goal;

    @Column(name = "Conceded", columnDefinition = "int", unique = false, nullable = true)
    private int conceded;

    @Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
    private boolean flag;

    @OneToMany(mappedBy = "teams", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<TournamentTeams> tournamentTeams;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IDAccount", insertable = false, updatable = false)
    private Account account;

    public Teams() {
    }

    public Teams(String iDTeam, String iDAccount, String nameTeam, int qualityPlayer, String iDGroup, int point,
            int goalDifference, int goal, int conceded, boolean flag, List<TournamentTeams> tournamentTeams,
            Account account) {
        this.iDTeam = iDTeam;
        this.iDAccount = iDAccount;
        this.nameTeam = nameTeam;
        this.qualityPlayer = qualityPlayer;
        this.iDGroup = iDGroup;
        this.point = point;
        this.goalDifference = goalDifference;
        this.goal = goal;
        this.conceded = conceded;
        this.flag = flag;
        this.tournamentTeams = tournamentTeams;
        this.account = account;
    }

    public String getiDTeam() {
        return iDTeam;
    }

    public void setiDTeam(String iDTeam) {
        this.iDTeam = iDTeam;
    }

    public String getiDAccount() {
        return iDAccount;
    }

    public void setiDAccount(String iDAccount) {
        this.iDAccount = iDAccount;
    }

    public String getNameTeam() {
        return nameTeam;
    }

    public void setNameTeam(String nameTeam) {
        this.nameTeam = nameTeam;
    }

    public int getQualityPlayer() {
        return qualityPlayer;
    }

    public void setQualityPlayer(int qualityPlayer) {
        this.qualityPlayer = qualityPlayer;
    }

    public String getiDGroup() {
        return iDGroup;
    }

    public void setiDGroup(String iDGroup) {
        this.iDGroup = iDGroup;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalDifference) {
        this.goalDifference = goalDifference;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getConceded() {
        return conceded;
    }

    public void setConceded(int conceded) {
        this.conceded = conceded;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public List<TournamentTeams> getTournamentTeams() {
        return tournamentTeams;
    }

    public void setTournamentTeams(List<TournamentTeams> tournamentTeams) {
        this.tournamentTeams = tournamentTeams;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

}
