package com.example.demo.entity;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "OrderPitch")
public class OrderPitch {

	@Id
	@Column(name = "IDOrder", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_orderpitch_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "OD"), @Parameter(name = "PK_Str", value = "iDOrder") })
	@GeneratedValue(generator = "sequence_orderpitch_id")
	private String iDOrder;

	@Column(name = "IDAccount", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String iDAccount;

	@Column(name = "IDPrice", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String iDPrice;

	@Column(name = "Deposit", columnDefinition = "decimal", unique = false, nullable = true)
	private float deposit;

	@Column(name = "DateStart", columnDefinition = "date", unique = false, nullable = true)
	private LocalDate dateStart;

	@Column(name = "DateOrder", columnDefinition = "date", unique = false, nullable = true)
	private LocalDate dateOrder;

	@Column(name = "Status", columnDefinition = "int", unique = false, nullable = true)
	private int status;

	@Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
	private boolean flag;

	@OneToMany(mappedBy = "orderPitch", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JsonIgnore
	private List<OrderPrice> orderPrices;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDAccount", insertable = false, updatable = false)
	private Account account;

	public OrderPitch() {
		// TODO Auto-generated constructor stub
	}

	public OrderPitch(String iDOrder, String iDAccount, String iDPrice, float deposit, LocalDate dateStart,
			LocalDate dateOrder, int status, boolean flag, List<OrderPrice> orderPrices, Account account) {
		super();
		this.iDOrder = iDOrder;
		this.iDAccount = iDAccount;
		this.iDPrice = iDPrice;
		this.deposit = deposit;
		this.dateStart = dateStart;
		this.dateOrder = dateOrder;
		this.status = status;
		this.flag = flag;
		this.orderPrices = orderPrices;
		this.account = account;
	}

	public OrderPitch(String iDAccount, String iDPrice, float deposit, LocalDate dateStart, LocalDate dateOrder,
			int status, boolean flag, List<OrderPrice> orderPrices, Account account) {
		super();
		this.iDAccount = iDAccount;
		this.iDPrice = iDPrice;
		this.deposit = deposit;
		this.dateStart = dateStart;
		this.dateOrder = dateOrder;
		this.status = status;
		this.flag = flag;
		this.orderPrices = orderPrices;
		this.account = account;
	}

	public String getiDOrder() {
		return iDOrder;
	}


	// Getter và setter tương ứng
		public List<OrderPrice> getOrderPrices() {
		    return orderPrices;
		}

		public void setOrderPrices(List<OrderPrice> orderPrices) {
		    this.orderPrices = orderPrices;
		}

	public void setiDOrder(String iDOrder) {
		this.iDOrder = iDOrder;
	}

	public String getiDAccount() {
		return iDAccount;
	}

	public void setiDAccount(String iDAccount) {
		this.iDAccount = iDAccount;
	}

	public String getiDPrice() {
		return iDPrice;
	}

	public void setiDPrice(String iDPrice) {
		this.iDPrice = iDPrice;
	}

	public float getDeposit() {
		return deposit;
	}

	public void setDeposit(float deposit) {
		this.deposit = deposit;
	}

	public LocalDate getDateStart() {
		return dateStart;
	}

	public void setDateStart(LocalDate dateStart) {
		this.dateStart = dateStart;
	}

	public LocalDate getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(LocalDate dateOrder) {
		this.dateOrder = dateOrder;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}
}
