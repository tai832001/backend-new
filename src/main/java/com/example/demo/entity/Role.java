package com.example.demo.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;


@Entity
@Table(name = "roles")
public class Role implements GrantedAuthority{
	
	public Role(String roleId, String authority) {
		super();
		this.roleId = roleId;
		this.authority = authority;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(
		name = "sequence_role_id"
		, strategy = "com.example.demo.customID.GenerateCustomId"
		, parameters = {
            @Parameter(name = "PREFIX", value = "RO")
            , @Parameter(name = "PK_Str", value = "roleId")
        }
	)
	@GeneratedValue(generator = "sequence_role_id")
	@Column(name = "role_Id")
	private String roleId;
	
	private String authority;
	
	public Role() {
		super();
	}
	
	public Role(String authority) {
		this.authority = authority;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}



	@Override
	public String getAuthority() {
		// TODO Auto-generated method stub
		return this.authority;
	}

	@Override
	public String toString() {
		return this.getAuthority();
	}
}
