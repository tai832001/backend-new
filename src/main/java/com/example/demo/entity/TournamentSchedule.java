package com.example.demo.entity;

import java.sql.Time;
import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "TournamentSchedule")
public class TournamentSchedule {

	@Id
	@Column(name = "IDMatch", columnDefinition = "varchar(10)",unique = true,nullable = true)
	@GenericGenerator(name = "sequence_tournamentSchedule_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "TS"), @Parameter(name = "PK_Str", value = "idMatch")
	})
	@GeneratedValue(generator = "sequence_tournamentSchedule_id")
	private String idMatch;
	
	@Column(name = "IDTeam_1", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String idTeam1;
	
	@Column(name = "IDTeam_2", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String idTeam2;
	
	@Column(name = "DateStart", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dateStart;
	
	@Column(name = "TimeStart", columnDefinition = "time",unique = false,nullable = true)
	private Time timeStart;
	
	@Column(name = "IDTournaments", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String idTournaments;
	
	@Column(name = "result_1", columnDefinition = "int",unique = false,nullable = true)
	private Integer result1;
	
	@Column(name = "result_2", columnDefinition = "int",unique = false,nullable = true)
	private Integer result2;
	
	@Column(name = "IDPitchDetail", columnDefinition = "varchar(10)",unique = false,nullable = false)
	private String idPitchDetail;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private Boolean flag;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDTournaments", unique = false, insertable = false, updatable = false)
	private Tournaments tournaments;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDTeam_1",insertable = false, updatable = false, unique = false)
	private Teams team1;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDTeam_2",insertable = false, updatable = false, unique = false)
	private Teams team2;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDPitchDetail",insertable = false, updatable = false, unique = false)
	private PitchDetail pitchDetail;
}
