package com.example.demo.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "PitchType")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PitchType {

	@Id
	@Column(name = "IDPitchType", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_pitchType_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "PT"), @Parameter(name = "PK_Str", value = "iDPitchType")
	})
	@GeneratedValue(generator = "sequence_pitchType_id")
	private String iDPitchType;
	
	@Column(name = "Size", columnDefinition = "int",unique = false,nullable = true)
	private int size;
	
	@Column(name = "GrassType", columnDefinition = "nvarchar(50)",unique = false,nullable = true)
	private String grassType;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	@OneToMany(mappedBy = "pitchType",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<PitchDetail> listPitchDetail;

	public PitchType(String iDPitchType, int size, String grassType, boolean flag) {
		super();
		this.iDPitchType = iDPitchType;
		this.size = size;
		this.grassType = grassType;
		this.flag = flag;
	}
	
}
