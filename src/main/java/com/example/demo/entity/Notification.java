package com.example.demo.entity;

import java.sql.Time;
import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Notification")
public class Notification {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "note", columnDefinition = "nvarchar(100)", unique = false, nullable = true)
	private String note;

	@ManyToOne
	@JoinColumn(name = "sender_id")
	private Account sender;

	@ManyToOne
	@JoinColumn(name = "receive_id")
	private Account receive;
	private int status;
	private boolean active;
	private boolean flag;
	private LocalDate date;
	private Time time;

	public Notification() {
		// TODO Auto-generated constructor stub
	}

	public Notification(String note, Account sender, Account receive, int status, boolean active, boolean flag,
			LocalDate date, Time time) {
		super();
		this.note = note;
		this.sender = sender;
		this.receive = receive;
		this.status = status;
		this.active = active;
		this.flag = flag;
		this.date = date;
		this.time = time;
	}

	public Notification(long id, String note, Account sender, Account receive, int status, boolean active, boolean flag,
			LocalDate date, Time time) {
		super();
		this.id = id;
		this.note = note;
		this.sender = sender;
		this.receive = receive;
		this.status = status;
		this.active = active;
		this.flag = flag;
		this.date = date;
		this.time = time;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Account getSender() {
		return sender;
	}

	public void setSender(Account sender) {
		this.sender = sender;
	}

	public Account getReceive() {
		return receive;
	}

	public void setReceive(Account receive) {
		this.receive = receive;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}
}
