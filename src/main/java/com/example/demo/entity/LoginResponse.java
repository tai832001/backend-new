package com.example.demo.entity;

import lombok.ToString;

@ToString
public class LoginResponse {
	private Account user;
    private String jwt;
    public LoginResponse() {
		// TODO Auto-generated constructor stub
	}
	public LoginResponse(Account user, String jwt) {
		super();
		this.user = user;
		this.jwt = jwt;
	}
	public Account getUser() {
		return user;
	}
	public void setUser(Account user) {
		this.user = user;
	}
	public String getJwt() {
		return jwt;
	}
	public void setJwt(String jwt) {
		this.jwt = jwt;
	}
    
}

