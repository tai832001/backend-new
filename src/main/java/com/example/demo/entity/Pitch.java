package com.example.demo.entity;

import java.util.List;
import java.util.Set;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Pitch")
public class Pitch {

	@Id
	@Column(name = "IDPitch", columnDefinition = "varchar(10)", unique = true, nullable = true)
	   @GenericGenerator(name = "sequence_pitch_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
	            @Parameter(name = "PREFIX", value = "PI"), @Parameter(name = "PK_Str", value = "iDPitch")
	    })
	    @GeneratedValue(generator = "sequence_pitch_id")
	private String iDPitch;
	
	@Column(name = "name", columnDefinition = "nvarchar(100)", unique = false, nullable = true)
    private String name;
	
	@Column(name = "address", columnDefinition = "nvarchar(50)", unique = false, nullable = true)
    private String address;
	
	@Column(name = "IDOwner", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String idOwner;

	@Column(name = "IDDistrict", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String iDDistrict;
	
	@Column(name = "IDDayOff", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String iDDayOff;
	
	private String pitchImage;

	@Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
	private boolean flag;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDDistrict", insertable = false, updatable = false)
	private District district;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IDOwner",  insertable = false, updatable = false)
	private Account account;
	
	@OneToMany(mappedBy = "pitch",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<PitchDetail> listPitchDetail;
	
	@OneToMany(mappedBy = "pitch",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Tournaments> tournaments;
	
	@OneToMany(mappedBy = "pitch",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<DayOffPitch> dayOffPitch;

    @Override
    public String toString() {
        return "Pitch [iDPitch=" + iDPitch + ", name=" + name + ", address=" + address + ", idOwner=" + idOwner
                + ", iDDistrict=" + iDDistrict + ", iDDayOff=" + iDDayOff + ", flag=" + flag + ", district=" + district
                + ", account=" + account + ", listPitchDetail=" + listPitchDetail + ", tournaments=" + tournaments
                + ", dayOffPitch=" + dayOffPitch + "]";
    }

    public String getiDPitch() {
        return iDPitch;
    }
    
    public String getPitchImage() {
		return pitchImage;
	}

	public void setPitchImage(String pitchImage) {
		this.pitchImage = pitchImage;
	}

	public void setiDPitch(String iDPitch) {
        this.iDPitch = iDPitch;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIdOwner() {
        return idOwner;
    }

    public void setIdOwner(String idOwner) {
        this.idOwner = idOwner;
    }

    public String getiDDistrict() {
        return iDDistrict;
    }

    public void setiDDistrict(String iDDistrict) {
        this.iDDistrict = iDDistrict;
    }

    public String getiDDayOff() {
        return iDDayOff;
    }

    public void setiDDayOff(String iDDayOff) {
        this.iDDayOff = iDDayOff;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public List<PitchDetail> getListPitchDetail() {
        return listPitchDetail;
    }

    public void setListPitchDetail(List<PitchDetail> listPitchDetail) {
        this.listPitchDetail = listPitchDetail;
    }

    public List<Tournaments> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournaments> tournaments) {
        this.tournaments = tournaments;
    }

    public List<DayOffPitch> getDayOffPitch() {
        return dayOffPitch;
    }

    public void setDayOffPitch(List<DayOffPitch> dayOffPitch) {
        this.dayOffPitch = dayOffPitch;
    }
}
