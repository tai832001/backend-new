package com.example.demo.entity;

import java.sql.Date;

public class RegisterData {
	private String username;
	private String password;
	private String fullname;
	private Date birthday;
	private String email;
	private String phone;
	private String address;
	
	public RegisterData() {
		// TODO Auto-generated constructor stub
	}

	public RegisterData(String username, String password, String fullname, Date birthday, String email, String phone,
			String address) {
		super();
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.birthday = birthday;
		this.email = email;
		this.phone = phone;
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "RegisterData [username=" + username + ", password=" + password + ", fullname=" + fullname
				+ ", birthday=" + birthday + ", email=" + email + ", phone=" + phone + ", address=" + address + "]";
	}
	
}
