package com.example.demo.entity;

public class AuthenRegister {
	private String username;
	private String password;
	public AuthenRegister() {
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public AuthenRegister(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	@Override
	public String toString() {
		return "AuthenRegister [username=" + username + ", password=" + password + "]";
	}

}
