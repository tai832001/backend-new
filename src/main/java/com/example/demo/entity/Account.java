package com.example.demo.entity;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.ToString;


@Entity
@Table(name = "Account")
@ToString
public class Account implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "IDAccount", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_account_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "AC"), @Parameter(name = "PK_Str", value = "iDAccount")
	})
	@GeneratedValue(generator = "sequence_account_id")
	private String iDAccount;

	@Column(name = "Username", columnDefinition = "varchar(24)", unique = true, nullable = false)
	private String username;

	@Column(name = "Password", columnDefinition = "varchar(max)", unique = false, nullable = false)
	private String password;

	@Column(name = "Phone", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String phone;

	@Column(name = "Email", columnDefinition = "varchar(50)",unique = true,nullable = false)
	private String email;
	@Column(name = "Address", columnDefinition = "nvarchar(50)",unique = false,nullable = true)
	private String address;

	@Column(name = "FullName", columnDefinition = "nvarchar(50)", unique = false, nullable = true)
	private String fullname;

	@Column(name = "Reliability", columnDefinition = "float", unique = false, nullable = true)
	private float reliability;

	@Column(name = "Birthday", columnDefinition = "date", unique = false, nullable = true)
	private LocalDate birthday;
	
	private String imageAccount;
	
	@Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
	private boolean flag;
	
	@Column(name = "Status", columnDefinition = "bit", unique = false, nullable = true)
	private boolean status;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = {
			@JoinColumn(name = "role_id") })
	private Set<Role> authorities;

//    @OneToMany(mappedBy = "account", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JsonIgnore
//    private List<Event> listEvent;
	
	public Account() {
		super();
		this.authorities = new HashSet<Role>();
	}

	public Account(String iDAccount, String username, String password, String fullname, String email,
			String phone, String address, float reliability, LocalDate birthday, boolean flag, Set<Role> authorities) {
		super();
		this.iDAccount = iDAccount;
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.reliability = reliability;
		this.birthday = birthday;
		this.flag = flag;
		this.authorities = authorities;
	}

	public Account(String username, String password, String fullname, String email,
			String phone, String address, float reliability, LocalDate birthday, boolean flag, Set<Role> authorities) {
		super();
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.reliability = reliability;
		this.birthday = birthday;
		this.flag = flag;
		this.authorities = authorities;
	}
	
	public Account(String username, String password, String fullname, String email,
			String phone, String address, float reliability, LocalDate birthday, boolean flag,boolean status , Set<Role> authorities) {
		super();
		this.username = username;
		this.password = password;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.address = address;
		this.reliability = reliability;
		this.birthday = birthday;
		this.flag = flag;
		this.status = status;
		this.authorities = authorities;
	}


	public Account(String iDAccount, String username, String password, String phone, String email, String address,
			String fullname, float reliability, LocalDate birthday, String imageAccount, boolean flag,
			Set<Role> authorities) {
		super();
		this.iDAccount = iDAccount;
		this.username = username;
		this.password = password;
		this.phone = phone;
		this.email = email;
		this.address = address;
		this.fullname = fullname;
		this.reliability = reliability;
		this.birthday = birthday;
		this.imageAccount = imageAccount;
		this.flag = flag;
		this.authorities = authorities;
	}
	
	

	public String getImageAccount() {
		return imageAccount;
	}

	public void setImageAccount(String imageAccount) {
		this.imageAccount = imageAccount;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public float getReliability() {
		return reliability;
	}

	public void setReliability(float reliability) {
		this.reliability = reliability;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public String getiDAccount() {
		return iDAccount;
	}

	public void setiDAccount(String iDAccount) {
		this.iDAccount = iDAccount;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.authorities;
	}

	public void setAuthorities(Set<Role> authorities) {
		this.authorities = authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

}
