package com.example.demo.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.annotation.Nonnull;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "District")
public class District {

	@Id
	@Column(name = "IDDistrict", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_district_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "DI"), @Parameter(name = "PK_Str", value = "iDDistrict") })
	@GeneratedValue(generator = "sequence_district_id")
	private String iDDistrict;

	@Column(name = "NameDistrict", columnDefinition = "nvarchar(50)", unique = false, nullable = true)
	@NotBlank
	@NotNull
	private String nameDistrict;

	@Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
	private boolean flag;

	@OneToMany(mappedBy = "district", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Pitch> listPitch;

	public District() {
		// TODO Auto-generated constructor stub
	}

	public District(String iDDistrict, @NotBlank @NotNull String nameDistrict, boolean flag, List<Pitch> listPitch) {
		super();
		this.iDDistrict = iDDistrict;
		this.nameDistrict = nameDistrict;
		this.flag = flag;
		this.listPitch = listPitch;
	}

	public String getiDDistrict() {
		return iDDistrict;
	}

	public void setiDDistrict(String iDDistrict) {
		this.iDDistrict = iDDistrict;
	}

	public String getNameDistrict() {
		return nameDistrict;
	}

	public void setNameDistrict(String nameDistrict) {
		this.nameDistrict = nameDistrict;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public List<Pitch> getListPitch() {
		return listPitch;
	}

	public void setListPitch(List<Pitch> listPitch) {
		this.listPitch = listPitch;
	}

}
