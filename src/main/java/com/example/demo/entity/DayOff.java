package com.example.demo.entity;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "DayOff")
public class DayOff {
	
	@Id
	@Column(name = "IDDayOff", columnDefinition = "varchar(10)",unique = true,nullable = true)
	private String iDDayOff;
	
	@Column(name = "DayOff", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dayOff;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	@OneToMany(mappedBy = "dayOff",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<DayOffPitch> dayOffPitch;

}
