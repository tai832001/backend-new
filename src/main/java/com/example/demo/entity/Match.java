package com.example.demo.entity;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Set;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Match")
public class Match {

	@Id
	@Column(name = "IDMatch", columnDefinition = "varchar(10)",unique = true,nullable = false)
	@GenericGenerator(name = "sequence_match_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "MA"), @Parameter(name = "PK_Str", value = "iDMatch")
    })
    @GeneratedValue(generator = "sequence_match_id")
	private String iDMatch;
	
	@Column(name = "IDMatchOwner", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDMatchOwner;
	
	@Column(name = "DateStart", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dateStart;
	
	@Column(name = "TimeStart", columnDefinition = "time",unique = false,nullable = true)
	private Time timeStart;
	
	@Column(name = "TimeEnd", columnDefinition = "time",unique = false,nullable = true)
	private Time timeEnd;
	
	@Column(name = "IDOpponent", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDOpponent;
	
	@Column(name = "Pitch", columnDefinition = "bit",unique = false,nullable = true)
	private boolean pitch;
	
	@Column(name = "IDPitchDetail", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDPitchDetail;
	
	@Column(name = "DateCreated", columnDefinition = "date",unique = false,nullable = true)
	private LocalDate dateCreated;
	
	@Column(name = "Description", columnDefinition = "nvarchar(255)",unique = false,nullable = true)
	private String description;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDMatchOwner",  insertable = false, updatable = false)
	private Account account;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDOpponent",  insertable = false, updatable = false)
	private Account accountOpponent;
	
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPitchDetail",  insertable = false, updatable = false)
	private PitchDetail pitchDetail;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "match_time", joinColumns = { @JoinColumn(name = "match_id") }, inverseJoinColumns = {
			@JoinColumn(name = "IDTime") })
	private Set<TimeRange> timeRange;

	public Match(String iDMatchOwner, LocalDate dateStart, Time timeStart, Time timeEnd, String iDOpponent,
			boolean pitch, String iDPitchDetail, LocalDate dateCreated, String description, boolean flag,
			Account account, Account accountOpponent, PitchDetail pitchDetail, Set<TimeRange> timeRange) {
		super();
		this.iDMatchOwner = iDMatchOwner;
		this.dateStart = dateStart;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.iDOpponent = iDOpponent;
		this.pitch = pitch;
		this.iDPitchDetail = iDPitchDetail;
		this.dateCreated = dateCreated;
		this.description = description;
		this.flag = flag;
		this.account = account;
		this.accountOpponent = accountOpponent;
		this.pitchDetail = pitchDetail;
		this.timeRange = timeRange;
	}
	public Match() {
		// TODO Auto-generated constructor stub
	}
	public String getiDMatch() {
		return iDMatch;
	}
	public void setiDMatch(String iDMatch) {
		this.iDMatch = iDMatch;
	}
	public String getiDMatchOwner() {
		return iDMatchOwner;
	}
	public void setiDMatchOwner(String iDMatchOwner) {
		this.iDMatchOwner = iDMatchOwner;
	}
	public LocalDate getDateStart() {
		return dateStart;
	}
	public void setDateStart(LocalDate dateStart) {
		this.dateStart = dateStart;
	}
	public Time getTimeStart() {
		return timeStart;
	}
	public void setTimeStart(Time timeStart) {
		this.timeStart = timeStart;
	}
	public Time getTimeEnd() {
		return timeEnd;
	}
	public void setTimeEnd(Time timeEnd) {
		this.timeEnd = timeEnd;
	}
	public String getiDOpponent() {
		return iDOpponent;
	}
	public void setiDOpponent(String iDOpponent) {
		this.iDOpponent = iDOpponent;
	}
	public boolean isPitch() {
		return pitch;
	}
	public void setPitch(boolean pitch) {
		this.pitch = pitch;
	}
	public String getiDPitchDetail() {
		return iDPitchDetail;
	}
	public void setiDPitchDetail(String iDPitchDetail) {
		this.iDPitchDetail = iDPitchDetail;
	}
	public LocalDate getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(LocalDate dateCreated) {
		this.dateCreated = dateCreated;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	public Account getAccountOpponent() {
		return accountOpponent;
	}
	public void setAccountOpponent(Account accountOpponent) {
		this.accountOpponent = accountOpponent;
	}
	public PitchDetail getPitchDetail() {
		return pitchDetail;
	}
	public void setPitchDetail(PitchDetail pitchDetail) {
		this.pitchDetail = pitchDetail;
	}
	public Set<TimeRange> getTimeRange() {
		return timeRange;
	}
	public void setTimeRange(Set<TimeRange> timeRange) {
		this.timeRange = timeRange;
	}
	public Match(String iDMatch, String iDMatchOwner, LocalDate dateStart, Time timeStart, Time timeEnd,
			String iDOpponent, boolean pitch, String iDPitchDetail, LocalDate dateCreated, String description,
			boolean flag, Account account, Account accountOpponent, PitchDetail pitchDetail, Set<TimeRange> timeRange) {
		super();
		this.iDMatch = iDMatch;
		this.iDMatchOwner = iDMatchOwner;
		this.dateStart = dateStart;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
		this.iDOpponent = iDOpponent;
		this.pitch = pitch;
		this.iDPitchDetail = iDPitchDetail;
		this.dateCreated = dateCreated;
		this.description = description;
		this.flag = flag;
		this.account = account;
		this.accountOpponent = accountOpponent;
		this.pitchDetail = pitchDetail;
		this.timeRange = timeRange;
	}
	@Override
	public String toString() {
		return "Match [iDMatch=" + iDMatch + ", iDMatchOwner=" + iDMatchOwner + ", dateStart=" + dateStart
				+ ", timeStart=" + timeStart + ", timeEnd=" + timeEnd + ", iDOpponent=" + iDOpponent + ", pitch="
				+ pitch + ", iDPitchDetail=" + iDPitchDetail + ", dateCreated=" + dateCreated + ", description="
				+ description + ", flag=" + flag + ", account=" + account + ", accountOpponent=" + accountOpponent
				+ ", pitchDetail=" + pitchDetail + ", timeRange=" + timeRange + "]";
	}
	
}
