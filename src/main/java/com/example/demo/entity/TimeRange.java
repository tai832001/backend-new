package com.example.demo.entity;

import java.sql.Time;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "TimeRange")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TimeRange {

	@Id
	@Column(name = "IDTime", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_time_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "TI"), @Parameter(name = "PK_Str", value = "iDTime")
	})
	@GeneratedValue(generator = "sequence_time_id")
	private String iDTime;
	
	@Column(name = "TimeFrom", columnDefinition = "time",unique = false,nullable = true)
	private Time timeFrom;
	
	@Column(name = "TimeTo", columnDefinition = "time",unique = false,nullable = true)
	private Time timeTo;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	@OneToMany(mappedBy = "timeRange",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Price> listPrice;

	public String getiDTime() {
		return iDTime;
	}

	public void setiDTime(String iDTime) {
		this.iDTime = iDTime;
	}

	public Time getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Time timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Time getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Time timeTo) {
		this.timeTo = timeTo;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public List<Price> getListPrice() {
		return listPrice;
	}

	public void setListPrice(List<Price> listPrice) {
		this.listPrice = listPrice;
	}
}
