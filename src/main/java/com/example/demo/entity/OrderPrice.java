package com.example.demo.entity;

import java.time.LocalDate;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.ToString;

@Entity
@Table(name = "OrderPrice")
@ToString
public class OrderPrice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "orderPitch_id")
	private OrderPitch orderPitch;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "price_id")
	private Price price;

	private LocalDate date;
	
	public OrderPrice() {
		// TODO Auto-generated constructor stub
	}

	public OrderPrice(long id, OrderPitch orderPitch, Price price, LocalDate date) {
		super();
		this.id = id;
		this.orderPitch = orderPitch;
		this.price = price;
		this.date = date;
	}

	public OrderPrice(OrderPitch orderPitch, Price price, LocalDate date) {
		super();
		this.orderPitch = orderPitch;
		this.price = price;
		this.date = date;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public OrderPitch getOrderPitch() {
		return orderPitch;
	}

	public void setOrderPitch(OrderPitch orderPitch) {
		this.orderPitch = orderPitch;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}
	@Override
	public boolean equals(Object o) {
	    if (this == o) return true;
	    if (o == null || getClass() != o.getClass()) return false;
	    OrderPrice orderPrice = (OrderPrice) o;
	    return id == orderPrice.id;
	}

	@Override
	public int hashCode() {
	    return Objects.hash(id);
	}


}
