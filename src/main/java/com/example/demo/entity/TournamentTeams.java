package com.example.demo.entity;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "TournamentTeams")
public class TournamentTeams {

    @Id
    @Column(name = "IDTournamentTeams", columnDefinition = "varchar(10)", unique = true, nullable = true)
    @GenericGenerator(name = "sequence_tournamentteams_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "TT"), @Parameter(name = "PK_Str", value = "iDTournamentTeams")
    })
    @GeneratedValue(generator = "sequence_tournamentteams_id")
    private String iDTournamentTeams;

    @Column(name = "IDTeam", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String iDTeam;

    @Column(name = "IDTournaments", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String iDTournaments;

    @Column(name = "Status", columnDefinition = "varchar(10)", unique = false, nullable = true)
    private String status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDTournaments", insertable = false, updatable = false)
    private Tournaments tournaments;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDTeam", insertable = false, updatable = false)
    private Teams teams;

    public TournamentTeams() {
    }

    public TournamentTeams(String iDTournamentTeams, String iDTeam, String iDTournaments, String status,
            Tournaments tournaments, Teams teams) {
        this.iDTournamentTeams = iDTournamentTeams;
        this.iDTeam = iDTeam;
        this.iDTournaments = iDTournaments;
        this.status = status;
        this.tournaments = tournaments;
        this.teams = teams;
    }

    public String getiDTournamentTeams() {
        return iDTournamentTeams;
    }

    public void setiDTournamentTeams(String iDTournamentTeams) {
        this.iDTournamentTeams = iDTournamentTeams;
    }

    public String getiDTeam() {
        return iDTeam;
    }

    public void setiDTeam(String iDTeam) {
        this.iDTeam = iDTeam;
    }

    public String getiDTournaments() {
        return iDTournaments;
    }

    public void setiDTournaments(String iDTournaments) {
        this.iDTournaments = iDTournaments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Tournaments getTournaments() {
        return tournaments;
    }

    public void setTournaments(Tournaments tournaments) {
        this.tournaments = tournaments;
    }

    public Teams getTeams() {
        return teams;
    }

    public void setTeams(Teams teams) {
        this.teams = teams;
    }

}
