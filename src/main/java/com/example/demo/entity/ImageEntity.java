package com.example.demo.entity;

import org.springframework.web.multipart.MultipartFile;

public class ImageEntity {
	private String username;
	private MultipartFile imageData;

	public ImageEntity() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public MultipartFile getImageData() {
		return imageData;
	}

	public void setImageData(MultipartFile imageData) {
		this.imageData = imageData;
	}

	public ImageEntity(String username, MultipartFile imageData) {
		super();
		this.username = username;
		this.imageData = imageData;
	}

	@Override
	public String toString() {
		return "ImageEntity [username=" + username + ", imageData=" + imageData + "]";
	}

}
