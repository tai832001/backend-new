package com.example.demo.entity;

import java.time.LocalDate;

public class OwnData {
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private LocalDate birthday;
	private String email;
	private String phone;
	private String address;
	public OwnData() {
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public OwnData(String username, String password, String firstname, String lastname, LocalDate birthday, String email,
			String phone, String address) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthday = birthday;
		this.email = email;
		this.phone = phone;
		this.address = address;
	}
	@Override
	public String toString() {
		return "OwnData [username=" + username + ", password=" + password + ", firstname=" + firstname + ", lastname="
				+ lastname + ", birthday=" + birthday + ", email=" + email + ", phone=" + phone + ", address=" + address
				+ "]";
	}
	
}
