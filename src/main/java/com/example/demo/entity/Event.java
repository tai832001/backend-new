package com.example.demo.entity;

import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.springframework.stereotype.Service;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "Event")
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Event {

	@Id
	@Column(name = "IDEvent", columnDefinition = "varchar(10)", unique = true, nullable = false)
	@GenericGenerator(name = "sequence_event_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
			@Parameter(name = "PREFIX", value = "EV"), @Parameter(name = "PK_Str", value = "iDEvent")
	})
	@GeneratedValue(generator = "sequence_event_id")
	private String iDEvent;
	
	@Column(name = "NameEvent", columnDefinition = "nvarchar(100)", unique = false, nullable = true)
	private String nameEvent;
	
	@Column(name = "FromEvent", columnDefinition = "date", unique = false, nullable = true)
	private LocalDate from;
	
	@Column(name = "ToEvent", columnDefinition = "date", unique = false, nullable = true)
	private LocalDate to;
	
	@Column(name = "Address", columnDefinition = "nvarchar(100)", unique = false, nullable = true)
	private String address;
	
	@Column(name = "Description", columnDefinition = "nvarchar(max)", unique = false, nullable = true)
	private String description;
	
	@Column(name = "Flag", columnDefinition = "bit", unique = false, nullable = true)
	private boolean flag;
	
	@Column(name = "Outstanding", columnDefinition = "bit", unique = false, nullable = true)
	private boolean outstanding; // tin silde trang chu
	
	@Column(name = "Image", unique = false, nullable = true)
	private String image;
	
	@Column(name = "IDAccount", columnDefinition = "varchar(10)", unique = false, nullable = true)
	private String iDAccount;
	
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "IDAccount", insertable = false, updatable = false)
    private Account account;

	public Event(String nameEvent, LocalDate from, LocalDate to, String address, String description, boolean flag,
			boolean outstanding) {
		super();
		this.nameEvent = nameEvent;
		this.from = from;
		this.to = to;
		this.address = address;
		this.description = description;
		this.flag = flag;
		this.outstanding = outstanding;
	}

	public Event(String iDEvent, String nameEvent, LocalDate from, LocalDate to, String address, String description,
			boolean flag, boolean outstanding) {
		super();
		this.iDEvent = iDEvent;
		this.nameEvent = nameEvent;
		this.from = from;
		this.to = to;
		this.address = address;
		this.description = description;
		this.flag = flag;
		this.outstanding = outstanding;
	}

	
    
    
    
}
