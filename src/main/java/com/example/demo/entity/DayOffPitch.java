package com.example.demo.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "DayOffPitch")
public class DayOffPitch {
	@Id
	@Column(name = "IDDayOffPitch", columnDefinition = "varchar(10)",unique = true,nullable = true)
	private String iDTournamentTeams;
	
	@Column(name = "IDDayOff", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDDayOff;
	
	@Column(name = "IDPitch", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDPitch;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDDayOffPitch", insertable = false, updatable = false)
	private DayOff dayOff;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "IDDayOff", insertable = false, updatable = false)
	private Pitch pitch;
}
