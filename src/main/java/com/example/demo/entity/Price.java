package com.example.demo.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Price")
public class Price {

	@Id
	@Column(name = "IDPrice", columnDefinition = "varchar(10)",unique = true,nullable = true)
    @GenericGenerator(name = "sequence_price_id", strategy = "com.example.demo.customID.GenerateCustomId", parameters = {
            @Parameter(name = "PREFIX", value = "PR"), @Parameter(name = "PK_Str", value = "iDPrice") })
    @GeneratedValue(generator = "sequence_price_id")
	private String iDPrice;
	
	@Column(name = "IDPitchDetail", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDPitchDetail;
	
	@Column(name = "IDTime", columnDefinition = "varchar(10)",unique = false,nullable = true)
	private String iDTime;
	
	@Column(name = "Price", columnDefinition = "decimal",unique = false,nullable = true)
	private float price;
	
	@Column(name = "Flag", columnDefinition = "bit",unique = false,nullable = true)
	private boolean flag;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDTime", insertable = false, updatable = false)
	private TimeRange timeRange;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPitchDetail", insertable = false, updatable = false)
	private PitchDetail pitchDetail;
	
	@OneToMany(mappedBy = "price", cascade = CascadeType.ALL)
    private List<OrderPrice> orderPrices;
	
	

	public List<OrderPrice> getOrderPrices() {
		return orderPrices;
	}

	public void setOrderPrices(List<OrderPrice> orderPrices) {
		this.orderPrices = orderPrices;
	}

	public String getiDPrice() {
		return iDPrice;
	}

	public void setiDPrice(String iDPrice) {
		this.iDPrice = iDPrice;
	}

	public String getiDPitchDetail() {
		return iDPitchDetail;
	}

	public void setiDPitchDetail(String iDPitchDetail) {
		this.iDPitchDetail = iDPitchDetail;
	}

	public String getiDTime() {
		return iDTime;
	}

	public void setiDTime(String iDTime) {
		this.iDTime = iDTime;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}

	public TimeRange getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(TimeRange timeRange) {
		this.timeRange = timeRange;
	}

	public PitchDetail getPitchDetail() {
		return pitchDetail;
	}

	public void setPitchDetail(PitchDetail pitchDetail) {
		this.pitchDetail = pitchDetail;
	}

	@Override
	public String toString() {
		return "Price [iDPrice=" + iDPrice + ", iDPitchDetail=" + iDPitchDetail + ", iDTime=" + iDTime + ", price="
				+ price + ", flag=" + flag + ", timeRange=" + timeRange + ", pitchDetail=" + pitchDetail
				+ ", orderPrices=" + orderPrices + "]";
	}
	
	
}
