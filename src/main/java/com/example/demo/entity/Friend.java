package com.example.demo.entity;

import java.time.LocalDateTime;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Entity
@Table(name = "friends")
public class Friend {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Account user;

    @ManyToOne
    @JoinColumn(name = "friend_id")
    private Account friend;

    private LocalDateTime createdAt;
    
    public Friend() {
    	this.createdAt = LocalDateTime.now();
	}

	public Friend(Account user, Account friend, LocalDateTime createdAt) {
		super();
		this.user = user;
		this.friend = friend;
		this.createdAt = createdAt;
	}
    
    
}
