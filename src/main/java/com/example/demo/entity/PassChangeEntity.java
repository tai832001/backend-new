package com.example.demo.entity;

public class PassChangeEntity {
	private String username;
	private String password;
	private String newpass;
	public PassChangeEntity() {
		// TODO Auto-generated constructor stub
	}
	public PassChangeEntity(String username, String password, String newpass) {
		super();
		this.username = username;
		this.password = password;
		this.newpass = newpass;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewpass() {
		return newpass;
	}
	public void setNewpass(String newpass) {
		this.newpass = newpass;
	}
	@Override
	public String toString() {
		return "PassChangeEntity [username=" + username + ", password=" + password + ", newpass=" + newpass + "]";
	}
	
}
