package com.example.demo;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.example.demo.dao.ConverReponsitory;
import com.example.demo.dao.DataUserReponsitory;
import com.example.demo.dao.DistrictReponsitory;
import com.example.demo.dao.MessReponsitory;
import com.example.demo.dao.PartiReponsitory;
import com.example.demo.dao.PitchDetailRepository;
import com.example.demo.dao.RoleReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.Conversation;
import com.example.demo.entity.DataUser;
import com.example.demo.entity.District;
import com.example.demo.entity.Message;
import com.example.demo.entity.Parti;
import com.example.demo.entity.PitchDetail;
import com.example.demo.entity.Role;
import com.example.demo.excel.DistrictImportExcel;

@SpringBootApplication
public class JwtTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtTestApplication.class, args);
	}

	@Bean
	CommandLineRunner run(RoleReponsitory roleReponsitory, UserReponsitory userReponsitory,
			PasswordEncoder passwordEncoder, ConverReponsitory converReponsitory, MessReponsitory messReponsitory,
			PartiReponsitory partiReponsitory, DistrictReponsitory districtReponsitory,
			DataUserReponsitory dataUserReponsitory) {
		return args -> {
			if (roleReponsitory.findByAuthority("ADMIN").isPresent()) {
				return;
			}
			if (roleReponsitory.findByAuthority("OWNER").isPresent()) {
				return;
			}
			Role adminRole = roleReponsitory.save(new Role("ADMIN"));
			roleReponsitory.save(new Role("USER"));
			Role adminOwner = roleReponsitory.save(new Role("OWNER"));
			Set<Role> roles = new HashSet<>();
			roles.add(adminRole);

			Set<Role> roleso = new HashSet<>();
			roleso.add(adminOwner);

			Account admin = new Account("admin", passwordEncoder.encode("password"), "tai nguyen", "admin@gmail.com",
					"0905505505", "nhacuatai", 0, null, true, roles);
			Account admin2 = new Account("tainguyen", passwordEncoder.encode("password"), "quoc nguyen",
					"tai832001@gmail.com", "0902201013", "hkjhk", 0, null, true, roles);

			Account owner = new Account("owner", passwordEncoder.encode("password"), "vi nguyen", "vi832001@gmail.com",
					"0928297328", "hkjhk", 0, null, true, roleso);

			DataUser dataUser = new DataUser(admin.getUsername(), "password", admin.getEmail());
			DataUser dataUser2 = new DataUser(admin2.getUsername(), "password", admin2.getEmail());
			DataUser dataUser3 = new DataUser(owner.getUsername(), "password", owner.getEmail());
			dataUserReponsitory.save(dataUser);
			dataUserReponsitory.save(dataUser2);
			dataUserReponsitory.save(dataUser3);
			

			userReponsitory.save(admin);
			userReponsitory.save(admin2);
			userReponsitory.save(owner);
			Conversation conversation = new Conversation("Admin1vsAdmin2", LocalDateTime.now());
			Parti parti = new Parti(1, conversation, admin);
			Parti parti2 = new Parti(2, conversation, admin2);
			converReponsitory.save(conversation);
			partiReponsitory.save(parti);
			partiReponsitory.save(parti2);
			Message message = new Message(conversation, admin2, "alo", LocalDateTime.now());
			Message message2 = new Message(conversation, admin, "hi", LocalDateTime.now());
			messReponsitory.save(message);
			messReponsitory.save(message2);

//			District district = new District("D003", "Hoa Vang", true,null);
//			districtReponsitory.save(district);
//			
//			DistrictImportExcel a = new DistrictImportExcel();
//			System.out.println(a.readExcel("D:/MFDN/data/District.xlsx"));
		};
	}
}
