package com.example.demo.customID;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import jakarta.persistence.TypedQuery;

/**
 * generate id follow format: prefix+idIncrease
 * @author Dell
 *
 */
@Component
public class GenerateCustomId implements IdentifierGenerator, Configurable{

	private static final long serialVersionUID = 1L;
	
	String prefix;
	String primaryKey;

	@Override
	public Object generate(SharedSessionContractImplementor session, Object object) {
		SessionFactory sessionFactory = session.getSessionFactory();
		
		StringBuilder hql = new StringBuilder();
		
		hql.append("SELECT ");
		hql.append(" obj.").append(primaryKey);
		hql.append("	FROM     ").append(object.getClass().getSimpleName()).append(" AS obj ");
		hql.append("	ORDER BY LEN(");
		hql.append(primaryKey);
		hql.append(") DESC, ");
		hql.append(primaryKey);
		hql.append(" DESC ");
		
		Session mySession = sessionFactory.openSession();

		TypedQuery<String> query = mySession.createQuery(hql.toString(), String.class);
		query.setMaxResults(1);
		
		int nextId = 1;
		
		try {
			if(query.getResultList() != null && query.getResultList().size() > 0) {
				String rs = String.valueOf(query.getSingleResult());
				String[] tmp = rs.split(prefix);
				int lastId = Integer.parseInt(tmp[1]);
				nextId = lastId + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		mySession.close();
		return prefix + String.valueOf(nextId);
	}
	
	@Override
	public void configure(Type type, Properties parameters, ServiceRegistry serviceRegistry) {
		prefix = parameters.getProperty("PREFIX");
		primaryKey = parameters.getProperty("PK_Str");
	}

}
