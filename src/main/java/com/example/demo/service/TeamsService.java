package com.example.demo.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dao.TeamReponsitory;
import com.example.demo.entity.Teams;

@Service
public class TeamsService {
    @Autowired
    private TeamReponsitory teamReponsitory;
    
    @Autowired
    private UserService userService;
    //team
    public Teams saveTeam(Teams team){
       teamReponsitory.save(team);
        return team;
    }
    
    public List<Teams> getTeamByUserId(){
        return teamReponsitory.getTeamByUserId(userService.getCurrentUser().getiDAccount());
    }
    
    public Teams getTeamById(String id) {
        return teamReponsitory.getTeamById(id);
    }
    
    public Map<String, Object> getTeamsByTournamentID(String id, int page){
        Map<String, Object> result = new HashMap<>();
        Pageable pageable = PageRequest.of(page, 10);
        Page<Teams> teams = teamReponsitory.getTeamsByTournamentID(id, pageable);
        result.put("teams", teams.getContent());
        result.put("maxpage", teams.getTotalPages());
        return result;
    }
}
