package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.FriendRequestReponsitory;
import com.example.demo.dto.PartyDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Friend;
import com.example.demo.entity.FriendRequest;

@Service
@Transactional
public class FriendRequestService {

	@Autowired
	private FriendRequestReponsitory friendRequestReponsitory;

	public FriendRequest addFriend(FriendRequest friendRequest) {
		return friendRequestReponsitory.save(friendRequest);
	}

	public boolean checkFriendRequest(Account id, String username) {
		List<FriendRequest> list = friendRequestReponsitory.findBySender(id);
		for (FriendRequest friendRequest : list) {
			if (username.equals(friendRequest.getReceiver().getUsername())) {
				return true;
			}
			;
		}
		return false;
	}
	
	public List<Account> findFriend(Account id){
		List<Account> list = new ArrayList<>();
		List<FriendRequest> listRequests = friendRequestReponsitory.findByReceiver(id);
		for (FriendRequest friendRequest : listRequests) {
			list.add(friendRequest.getSender());
		}
		return list;
	}
	
	public boolean checkFriendAccept(List<Account> list, String username) {
		for (Account account : list) {
			if (username.equals(account.getUsername())) {
				return true;
			}
		}
		return false;
	}
	
	public FriendRequest findFriendRequest(Account account,String username) {
		List<FriendRequest> list = friendRequestReponsitory.findByReceiver(account);
		for (FriendRequest friendRequest : list) {
			return friendRequest;
		}
		return null;
	}
	
	public boolean rejectFriend(FriendRequest friendRequest) {
		try {
			friendRequestReponsitory.delete(friendRequest);
			return true;
		} catch (Exception e) {
			return false;
		}
		
	}
	
}
