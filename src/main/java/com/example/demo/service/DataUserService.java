package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.DataUserReponsitory;
import com.example.demo.entity.DataUser;

@Service
public class DataUserService {
	@Autowired
	private DataUserReponsitory dataUserReponsitory;

	public DataUser findUserByEmail(String email) {
		try {
			return dataUserReponsitory.findByEmail(email).get();
		} catch (Exception e) {
			return null;
		}
	}

	public DataUser saveUser(DataUser dataUser) {
		return dataUserReponsitory.save(dataUser);
	}
}
