package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.TeamReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.Teams;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class TeamService {
	
	@Autowired
	UserService userService;
	
	@Autowired
	TeamReponsitory teamReponsitory;
	
	public List<Teams> getTeamByUser() {
		Account account = userService.getCurrentUser();
		
		List<Teams> rs = new ArrayList<>();
		if(account != null) {
			rs = teamReponsitory.getTeamByUser(account.getUsername());
		}
		
		return rs;
	}
}
