package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.TimeRepository;
import com.example.demo.entity.TimeRange;

@Service
public class TimeRangeService {
    @Autowired
    TimeRepository timeRepisitory;
    
    public List<TimeRange> getAll(){
        return timeRepisitory.getAll();
    }
}
