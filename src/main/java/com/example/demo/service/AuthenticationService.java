package com.example.demo.service;

import java.io.File;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.example.demo.dao.RoleReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.LoginResponse;
import com.example.demo.entity.Role;

import jakarta.mail.internet.MimeMessage;

@Service
@Transactional
public class AuthenticationService {

	@Autowired
	private UserReponsitory userReponsitory;

	@Autowired
	private RoleReponsitory roleReponsitory;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private ResourceLoader resourceLoader;

	@Autowired
	private TokenService tokenService;

	public Account registerUser(String username, String password, String fullname, String phone, String address,
			LocalDate birthday, String email) {
		String encodePassword = passwordEncoder.encode(password);
		Role userRole = roleReponsitory.findByAuthority("USER").get();

		Set<Role> authorities = new HashSet<>();
		authorities.add(userRole);
		return userReponsitory.save(new Account(username, encodePassword, fullname, email, phone, address, 0, birthday,
				true, authorities));
	}

	public Account registerUserGoogle(String username, String password, String fullname, String phone, String address,
			LocalDate birthday, String email, Boolean status) {
		String encodePassword = passwordEncoder.encode(password);
		Role userRole = roleReponsitory.findByAuthority("USER").get();

		Set<Role> authorities = new HashSet<>();
		authorities.add(userRole);
		return userReponsitory.save(new Account(username, encodePassword, fullname, email, phone, address, 0, birthday,
				true, status, authorities));
	}

	public LoginResponse loginUser(String username, String password) {

		try {
			Authentication auth = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));

			String token = tokenService.generateJwt(auth);

			return new LoginResponse(userReponsitory.findByUsername(username).get(), token);

		} catch (AuthenticationException e) {
			return new LoginResponse(null, "");
		}
	}

	public int generateRandomSixDigitNumber() {
		Random random = new Random();
		return random.nextInt(900000) + 100000;
	}

	@Value("${spring.mail.username}")
	private String fromEmail;

	@Autowired
	private JavaMailSender javaMailSender;

	public int sendMail(String mail, int number) {
		try {
			Resource resource = resourceLoader.getResource("classpath:" + "static/img/Head-Soccer-2022.png");
			MimeMessage message = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(fromEmail);
			helper.setTo(mail);
			helper.setSubject("[MFDN] mã code xác nhận email: " + mail);
			String html = "<html>\r\n" + "  <p>Dear anh chị,</p>\r\n"
					+ "  <p>Chúng tôi từ bên hệ thống MDFN gửi anh,chị mã code để xác thực email !</p>\r\n"
					+ "  <div style=\"display: flex\">\r\n" + "    <p>Mã là:</p>\r\n" + "    <div\r\n"
					+ "      style=\"\r\n" + "        display: flex;\r\n" + "        justify-content: center;\r\n"
					+ "        align-items: center;\r\n" + "        border: 2px solid #d7d7d7;\r\n"
					+ "        padding: 5px 10px;\r\n" + "        font-size: x-large;\r\n"
					+ "        margin-left: 10px;\r\n" + "      \"\r\n" + "    >\r\n" + "      " + number + "\r\n"
					+ "    </div>\r\n" + "  </div>\r\n" + "</html>";

			helper.setText(html, true);
//			helper.addAttachment("image.png", resource.getFile());
			javaMailSender.send(message);
			return number;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return 0;
		}
	}

	public int updatePassword(String email, String password) {
		try {
			Account account = userReponsitory.findByEmail(email).get();
			account.setPassword(passwordEncoder.encode(password));
			userReponsitory.save(account);
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}
}
