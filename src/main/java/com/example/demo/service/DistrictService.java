package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.DistrictReponsitory;
import com.example.demo.dto.DistrictDTO;
import com.example.demo.entity.District;

@Service
public class DistrictService {
	
    @Autowired
    DistrictReponsitory districtReponsitory;

    public List<District> getAll() {
        return districtReponsitory.findAll();
    }
    
    public List<District> getAllDistrict() {
        return districtReponsitory.getAllDistrict();
    }
    
    public List<DistrictDTO> listDTO(List<District> list){
    	List<DistrictDTO> ls = new ArrayList<>();
    	for (District district : list) {
			ls.add(new DistrictDTO(district.getiDDistrict(), district.getNameDistrict(), true, district.getListPitch()));
		}
    	return ls;
    }
    
    public List<District> getAllDistrictHome() {
        return districtReponsitory.getDistrictsWithPitchDetailsAndPrices();
    }
    

}
