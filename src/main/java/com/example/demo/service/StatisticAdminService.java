package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IStatisticAdminReponsitory;
import com.example.demo.dto.StatisticAdminDTO;

@Service
public class StatisticAdminService {

	@Autowired
	private IStatisticAdminReponsitory statisticAdminReponsitory;
	
	public List<StatisticAdminDTO> getTop3Highest(String month, String year){
		return statisticAdminReponsitory.getTop3Highest(month, year);
	}
	
	public List<StatisticAdminDTO> getTop3Lowest(String month, String year){
		return statisticAdminReponsitory.getTop3Lowest(month, year);
	}
}
