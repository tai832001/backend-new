package com.example.demo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.ConverReponsitory;
import com.example.demo.entity.Conversation;

@Service
@Transactional
public class ConversationService {
	
	@Autowired
	private ConverReponsitory converReponsitory;
	
	public Conversation findConverByName(String name) {
		return converReponsitory.findByConversationName(name).get();
	}
	public Optional<Conversation> findById(String id) {
		return converReponsitory.findById(Integer.valueOf(id));
	}
	
	public Conversation saveConver(Conversation conversation) {
		return converReponsitory.save(conversation);
	}
}
