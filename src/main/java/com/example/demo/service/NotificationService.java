package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dao.NotificationReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.Notification;

@Service
public class NotificationService {
	
	@Autowired
	private NotificationReponsitory notificationReponsitory;
	
	public Notification save(Notification notification) {
		return notificationReponsitory.save(notification);
	}
	
	public Map<String, Object> listNoti(int page, String id){
		Map<String, Object> result = new HashMap<>();
		Pageable pageable = PageRequest.of(page, 10);
		Page<Notification> listpage = notificationReponsitory.findByIdAccount(pageable, id);
		
		result.put("listNotification", listpage.getContent());
        result.put("maxpage", listpage.getTotalPages());
        return result;
	}
	
	public long getByID(long id) {
		Notification notification = notificationReponsitory.findById(id).get();
		notification.setActive(false);
		Notification newnotification = notificationReponsitory.save(notification);
		return newnotification.getId();
	}
	
	public int getNumNoti(String id) {
		return notificationReponsitory.getNumberNoti(id);
	}
	public Notification getNotiMess(String idsender, String idreceive) {
		return notificationReponsitory.getNotiMess(idsender, idreceive);
	}
	
	public void deleteNoti(Notification noti) {
		notificationReponsitory.delete(noti);
	}
}
