package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.MatchReponsitory;
import com.example.demo.dto.MatchDTO;
import com.example.demo.dto.MatchHomeDTO;
import com.example.demo.entity.Match;

@Service
public class MatchService {

	@Autowired
	private MatchReponsitory matchReponsitory;

	public Match saveMatch(Match match) {
		return matchReponsitory.save(match);
	}

	public List<Match> getAll() {
		return matchReponsitory.getAll();
	}

	public List<MatchDTO> listMatch(List<Match> list) {
		List<MatchDTO> ls = new ArrayList<>();
		for (Match match : list) {
			if (match.getiDOpponent() != null) {
				MatchDTO dto = new MatchDTO(match.getiDMatch(), match.getDateStart(), match.getAccount().getUsername(),
						match.getDescription(), match.getAccount().getFullname(), match.getPitchDetail().getNamePitch(),
						match.getPitchDetail().getPitch().getAddress(), match.getPitchDetail().getPitch().getName(),
						match.getPitchDetail().getPitchType().getSize(),
						match.getPitchDetail().getPitchType().getGrassType(), match.getTimeRange(),
						match.getTimeStart(), match.getAccountOpponent().getUsername(),
						match.getAccountOpponent().getFullname());
				ls.add(dto);
			} else {
				MatchDTO dto = new MatchDTO(match.getiDMatch(), match.getDateStart(), match.getAccount().getUsername(),
						match.getDescription(), match.getAccount().getFullname(), match.getPitchDetail().getNamePitch(),
						match.getPitchDetail().getPitch().getAddress(), match.getPitchDetail().getPitch().getName(),
						match.getPitchDetail().getPitchType().getSize(),
						match.getPitchDetail().getPitchType().getGrassType(), match.getTimeRange(), null, null, null);
				ls.add(dto);
			}
		}
		return ls;
	}

	public List<MatchHomeDTO> listHomeMatch(List<Match> list) {
		List<MatchHomeDTO> ls = new ArrayList<>();
		for (Match match : list) {
			if (match.getiDOpponent() == null) {
				MatchHomeDTO homeDTO = new MatchHomeDTO(match.getiDMatch(),
						match.getPitchDetail().getPitch().getAddress(), match.getDateStart(),
						match.getAccount().getUsername(), match.getAccount().getFullname(),
						match.getPitchDetail().getNamePitch(), match.getDescription(),
						match.getPitchDetail().getPitch().getName(), match.getPitchDetail().getPitchType().getSize(),
						match.getPitchDetail().getPitchType().getGrassType(), match.getTimeRange());
				ls.add(homeDTO);
			}
		}
		return ls;
	}

	public List<MatchHomeDTO> listMyMatch(List<Match> list) {
		List<MatchHomeDTO> ls = new ArrayList<>();
		for (Match match : list) {
			MatchHomeDTO homeDTO = new MatchHomeDTO(match.getiDMatch(), match.getPitchDetail().getPitch().getAddress(),
					match.getDateStart(), match.getAccount().getUsername(), match.getAccount().getFullname(),
					match.getPitchDetail().getNamePitch(), match.getDescription(),
					match.getPitchDetail().getPitch().getName(), match.getPitchDetail().getPitchType().getSize(),
					match.getPitchDetail().getPitchType().getGrassType(), match.getTimeRange());
			ls.add(homeDTO);
		}
		return ls;
	}

	public Page<Match> listMatchPyPage(int pageNumber) {
		int pageSize = 5;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return matchReponsitory.listMatch(pageable);
	}

	public Page<Match> listMatchPyPageSearch(int pageNumber, String search) {
		int pageSize = 5;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return matchReponsitory.listMatchSearch(pageable, search);
	}

	public int numberOfPage() {
		int pageNumber = 1;
		int pageSize = 5;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return matchReponsitory.listMatch(pageable).getTotalPages();
	}

	public Page<Match> homePageMatch(String username) {
		int pageNumber = 0;
		int pageSize = 10;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return matchReponsitory.listMatchHome(pageable, username);
	}

	public Page<Match> myPageMatch(int pageNumber, String username) {
		int pageSize = 10;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return matchReponsitory.listMyMatch(pageable, username);
	}

	public Match findById(String id) {
		return matchReponsitory.findById(id).get();
	}

	public boolean deleteMatch(String id) {
		Match match = matchReponsitory.findById(id).get();
		if (!matchReponsitory.findById(id).equals(Optional.empty())) {
			match.setFlag(false);
			matchReponsitory.save(match);
			return true;
		}
		return false;
	}

}
