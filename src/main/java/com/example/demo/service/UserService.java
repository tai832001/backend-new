package com.example.demo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;

import com.example.demo.dao.DataUserReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.DataUser;
import com.example.demo.entity.EditEntity;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Autowired
	private UserReponsitory userReponsitory;

	@Autowired
	private DataUserReponsitory dataUserReponsitory;

	@Autowired
	private JwtDecoder jwtDecoder;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		System.out.println("In the user detail service");
		return userReponsitory.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException("user is not valid"));
	}

	public Account findUserByUsername(String username) throws UsernameNotFoundException {
		try {
			return userReponsitory.findByUsername(username).get();
		} catch (Exception e) {
			return null;
		}
	}

	public Account saveUser(Account account) {
		System.out.println(account);
		return userReponsitory.save(account);
	}

	public Account findUserByEmail(String email) {
		try {
			return userReponsitory.findByEmail(email).get();
		} catch (Exception e) {
			return null;
		}
	}

	public Account findUserByPhone(String phone) {
		try {
			return userReponsitory.findByPhone(phone).get();
		} catch (Exception e) {
			return null;
		}
	}

	public Account saveEditForUser(EditEntity editEntity) {
		Account account = userReponsitory.findByUsername(editEntity.getUsername()).get();
		account.setAddress(editEntity.getAddress());
		account.setEmail(editEntity.getEmail());
		account.setBirthday(editEntity.getBirthday());
		account.setFullname(editEntity.getFullname());
		account.setPhone(editEntity.getPhone());
		return account;

	}

	public int validateEditUser(EditEntity editEntity) {
		return 0;
	}

	public int validateUser(String username, String password) {
		try {
			Account user = findUserByUsername(username);
			boolean isValid = passwordEncoder.matches(password, user.getPassword());
			if (isValid != true) {
				return 2;
			} else {
				return 0;
			}
		} catch (Exception e) {
			return 1;
		}
	}

	public int changePassword(String username, String password, String newpass) {
		try {
			Account user = findUserByUsername(username);
			if (user != null) {
				boolean isValid = passwordEncoder.matches(password, user.getPassword());
				if (isValid != true) {
					return 1;
				} else {
					DataUser dataUser = dataUserReponsitory.findByUsername(username).get();
					dataUser.setPassword(newpass);
					dataUserReponsitory.save(dataUser);
					user.setPassword(passwordEncoder.encode(newpass));
					userReponsitory.save(user);
					return 0;
				}
			}
			return 2;
		} catch (Exception e) {
			return 2;
		}
	}
	
	public int checkPassword(String password) {
		try {
			Account user = getCurrentUser();
			if (user != null) {
				boolean isValid = passwordEncoder.matches(password, user.getPassword());
				if (isValid != true) {
					return 1;
				} else {
					return 0;
				}
			}
			return 2;
		} catch (Exception e) {
			return 2;
		}

	}

	public int updateImage(String username, String image) {
		try {
			Account user = findUserByUsername(username);
			if (user != null) {
				user.setImageAccount(image);
				userReponsitory.save(user);
				return 0;
			}
			return 1;
		} catch (Exception e) {
			return 1;
		}
	}

	public Account getCurrentUser() throws UsernameNotFoundException {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			Jwt jwt = (Jwt) authentication.getPrincipal();
			Map<String, Object> claims = jwtDecoder.decode(jwt.getTokenValue()).getClaims();

			return userReponsitory.findByUsername(claims.get("sub").toString()).get();
		} catch (Exception e) {
			return null;
		}
	}

	public List<Account> listAccount(String username, String fullname) {
		Account account = userReponsitory.findByUsername(username).get();
		return userReponsitory.searchFriend(account.getiDAccount(), fullname);
	}

	public List<Account> getAccount() {
		return userReponsitory.getAccount();
	}

	public void deleteAccount(String iDAccount) {
		userReponsitory.deleteAccount(iDAccount);
	}

	public List<Account> getAll() {
		return userReponsitory.getAllAccount();
	}
	public Account getByEmailNotMy(String email,String myemail) {
		if (!email.equals(myemail)) {
			return userReponsitory.findByEmail(email).get();
		} else {
			return null;
		}
	}
	
}
