package com.example.demo.service;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Value;


import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import com.azure.storage.blob.specialized.BlockBlobClient;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.google.cloud.storage.*;



@Service
public class SendImageToAzureService {
	String connectionString = "DefaultEndpointsProtocol=https;AccountName=datamfdn;AccountKey=AG3zoh89Zb8y14Wq5gLd7gmqjd7dea6FAm14/o514gMCsjjMhdEmi74omSRJsY7nyFrw3OIdBW4s+AStcNl5Sw==;EndpointSuffix=core.windows.net";
    String containerName = "images";
    
    @Value("${firebase.bucketName}")
    private String bucketName;
    @Value("${firebase.credentialsPath}")
    private String credentialsPath;

	public String getLinkFromAzure(MultipartFile file, String nameimage) {
		try {
	        byte[] data = file.getBytes();
	        BlobServiceClient blobServiceClient = new BlobServiceClientBuilder()
	                .connectionString(connectionString)
	                .buildClient();
	        BlobContainerClient containerClient = blobServiceClient.getBlobContainerClient(containerName);
	        String blobName = "account" + nameimage+"_" + UUID.randomUUID().toString();;
	        BlobClient blobClient = containerClient.getBlobClient(blobName);
	        
	        if (blobClient.exists()) {
	            blobClient.delete();
	        }

	        BlockBlobClient blockBlobClient = blobClient.getBlockBlobClient();
	        blockBlobClient.upload(new ByteArrayInputStream(data), data.length);
	        return blockBlobClient.getBlobUrl();
	    } catch (IOException e) {
	        return "không thể upload ảnh";
	    }
		
	}
	
	public String sendImageToFireBas(MultipartFile file) {
		try (InputStream inputStream = file.getInputStream()) {
            StorageOptions storageOptions = StorageOptions.newBuilder()
                    .setCredentials(GoogleCredentials.fromStream(
                            getClass().getResourceAsStream(credentialsPath)))
                    .build();
            Storage storage = storageOptions.getService();
            String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
            BlobId blobId = BlobId.of(bucketName, fileName);
            BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType(file.getContentType()).build();
            Blob blob = storage.create(blobInfo, inputStream);
            return blob.getMediaLink();
        } catch (Exception e) {
            e.printStackTrace();
            return "Lỗi khi upload file: " + e.getMessage();
        }
	}

}
