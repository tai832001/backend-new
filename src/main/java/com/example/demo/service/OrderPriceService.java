package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.OrderPriceReponsitory;
import com.example.demo.entity.OrderPitch;
import com.example.demo.entity.OrderPrice;
import com.example.demo.entity.Price;

@Service
public class OrderPriceService {
	
	@Autowired
	private PriceService priceService;
	
	@Autowired
	private OrderPriceReponsitory orderPriceReponsitory;
	
	public OrderPrice saveOrder(OrderPrice orderPrice) {
		return orderPriceReponsitory.save(orderPrice);
	}
	
	public List<OrderPrice> listPrice(List<String> price, List<LocalDate> date, OrderPitch orderPitch){
		List<OrderPrice> list = new ArrayList<>();
		int index = 0;
		for (String string : price) {
			Price priceI = priceService.getByID(string);
			list.add(new OrderPrice(orderPitch, priceI, date.get(index)));
			index++;
		}
		return list;
	}
	public List<OrderPrice> getOrder( LocalDate date, LocalDate date2) {
		return orderPriceReponsitory.getOrderPricesBetweenDates(date, date2);
	}
	
}
