package com.example.demo.service;

import java.util.List;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.constant.StatusConstant;
import com.example.demo.dao.TeamReponsitory;
import com.example.demo.dao.TournamentRepository;
import com.example.demo.dao.TournamentTeamDAO;
import com.example.demo.entity.Notification;
import com.example.demo.entity.Teams;
import com.example.demo.entity.TournamentTeams;
import com.example.demo.entity.Tournaments;
import com.example.demo.exception.BussinessException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import com.example.demo.dao.TournamentTeamRepository;

@Transactional
@Service
public class TournamentTeamService {
	@Autowired
	TournamentTeamDAO tournamentTeamDAO;

    @Autowired 
    TournamentTeamRepository tournamentTeamRepository;
    
    @Autowired
    TournamentRepository tournamentRepository;
    
    @Autowired
    TeamReponsitory teamReponsitory;
    
    @Autowired
    NotificationService notificationService;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	public List<TournamentTeams> getTeamInTournament(String id) {
		return tournamentTeamDAO.getTeamInTournament(id);
	}
    
    public Map<String, Object> getByIdTournament(String id, int page){
        Map<String, Object> result = new HashMap<>();
        Pageable pageable = PageRequest.of(page, 10);
        Page<TournamentTeams> tournamentTeams = tournamentTeamRepository.getByIdTournament(id, pageable);
        result.put("tournamentteams", tournamentTeams );
        result.put("maxpage", tournamentTeams.getTotalPages());
        
        return result;
    }
    
    public TournamentTeams save(TournamentTeams tournamentTeams) {
        return tournamentTeamRepository.save(tournamentTeams);
    }
    
    public TournamentTeams saveTournamentTeamWithId(String tmId, String idTeam) throws Exception {
    	Tournaments tournament = tournamentRepository.getById(tmId);
    	Teams team = teamReponsitory.getById(idTeam);
    	
    	if(!tournament.isFlag()) {
    		throw new BussinessException("Giải đấu không còn tồn tại");
    	}
    	
    	if(!team.isFlag()) {
    		throw new BussinessException("Đội không còn tồn tại");
    	}
    	
    	TournamentTeams tournamentTeam = new TournamentTeams();
    	tournamentTeam.setTournaments(tournament);
    	tournamentTeam.setTeams(team);
    	tournamentTeam.setiDTournaments(tmId);
    	tournamentTeam.setiDTeam(idTeam);
    	tournamentTeam.setStatus(StatusConstant.WAITING);
    	
    	Notification notification = new Notification();
        
        StringBuilder note = new StringBuilder();
        note.append(tournament.getiDTournaments());
        note.append("-");
        note.append("Đội ");
        note.append(team.getNameTeam());
        note.append(" ");
        note.append("đang muốn tham gia giải đấu");
        note.append(" ");
        note.append(tournament.getNameTouraments());
        note.append(" của bạn");
        
        notification.setNote(note.toString());
        notification.setActive(true);
        notification.setDate(LocalDate.now());
        notification.setFlag(true);
        notification.setReceive(tournament.getAccount());
        notification.setSender(team.getAccount());
        notification.setStatus(7);
        
        Notification notificationSaved = notificationService.save(notification);
		String destination = "/topic/" + tournament.getAccount().getUsername() +"notification";
		messagingTemplate.convertAndSend(destination, notificationSaved);
    	
        return tournamentTeamRepository.save(tournamentTeam);
    }
}
