package com.example.demo.service;

import java.util.List;

import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSmartTagRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.PitchTypeReponsitory;
import com.example.demo.entity.PitchType;

@Service
@Transactional
public class PitchTypeService {
    @Autowired
    PitchTypeReponsitory pitchTypeReponsitory;
    
    public List<PitchType> getAll() {
        return pitchTypeReponsitory.findAll();
    }
    
    public PitchType findByID(String id) {
        return pitchTypeReponsitory.getByID(id);
    }
}
