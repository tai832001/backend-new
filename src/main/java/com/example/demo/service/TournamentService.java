package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.constant.StatusConstant;
import com.example.demo.controller.GetTournamentUserJoinDTO;
import com.example.demo.controller.GetTournamentsByFilterRespDTO;
import com.example.demo.dao.DistrictReponsitory;
import com.example.demo.dao.ITournamentAdminDao;
import com.example.demo.dao.TournamentRepository;
import com.example.demo.dao.TournamentScheduleDao;
import com.example.demo.dao.TournamentTeamDAO;
import com.example.demo.dao.TournamentTeamRepository;
import com.example.demo.dao.TournamentsDao;
import com.example.demo.dto.GetTeamJoinUserDTO;
import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.dto.ScheduleOfUserDTO;
import com.example.demo.dto.TournamentAdminDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.District;
import com.example.demo.entity.Notification;
import com.example.demo.entity.Teams;
import com.example.demo.entity.TournamentTeams;
import com.example.demo.entity.Tournaments;
import com.example.demo.exception.BussinessException;

@Service
@Transactional
public class TournamentService {
	public static final String TOURNAMENT_TEAM_STATUS_REJECT = "R";
	public static final String TOURNAMENT_TEAM_STATUS_CANCEL = "C";
    @Autowired
    TournamentRepository tournamentRepository;

    @Autowired
    TournamentsDao tournamentsDao;
    
    @Autowired
    TournamentScheduleDao tournamentScheduleDao;
    
    @Autowired
    TournamentTeamDAO tournamentTeamDAO;

    @Autowired
    DistrictReponsitory districtReponsitory;
    
    @Autowired
    private ITournamentAdminDao tournamentAdminDao;
    
    @Autowired
    private TournamentTeamRepository tournamentTeamRepository;
    
    @Autowired
    TeamService teamService;

    @Autowired
    UserService userService;
    
    @Autowired
    NotificationService notificationService;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;

    public Tournaments save(Tournaments tournaments) {
        return tournamentRepository.save(tournaments);
    }

    public List<Tournaments> getByAccountID(String id) {
        return tournamentRepository.getByAccountID(id);
    }
    
    public List<ListScheduleBasicDTO>  getTournamentSchedulesById (String id){
        return tournamentScheduleDao.getTournamentSchedulesById(id);
    }

    public Tournaments getByID(String id) {
        return tournamentRepository.getById(id);
    }

    public List<TournamentAdminDTO> getAll() {
        return tournamentAdminDao.getAll();
    }
    
    public List<TournamentAdminDTO> getTournamentNotYet() {
    	List<TournamentAdminDTO> listAll = tournamentAdminDao.getAll();
    	List<TournamentAdminDTO> listTrue = new ArrayList<>();
    	for (TournamentAdminDTO tournamentAdminDTO : listAll) {
			if("0".equals(tournamentAdminDTO.getStatus())) {
				listTrue.add(tournamentAdminDTO);
			}
		}
    	return listTrue;
    }

    public void deleteTournament(String iDTournaments) {
    	tournamentRepository.deleteTournament(iDTournaments);
    }
    public String checkTeamExistInTournament(String timId) throws Exception {
        Account currentUser = userService.getCurrentUser();
        
        if(currentUser == null) {
        	return "";
        }
        
        TournamentTeams tournamentTeams = tournamentTeamDAO.checkTeamExistInTournament(timId, currentUser);

        if (StatusConstant.ACCEPT.equals(tournamentTeams.getStatus())) {
            return "Bạn đã tham gia giải này.";
        } else if (StatusConstant.WAITING.equals(tournamentTeams.getStatus())) {
            return "Bạn đã gửi yêu cầu, hãy đợi người tạo chấp nhận.";
        } else if (StatusConstant.REJECT.equals(tournamentTeams.getStatus())) {
            return "Người tạo đã từ chối yêu cầu này của bạn.";
        }
        return "";
    }

    public Object[] fetchDataInit(String tmId) throws Exception {
        // kiểm tra giữa account hiện tại với giải đấu
        String check = checkTeamExistInTournament(tmId);

        Tournaments tournaments = getByID(tmId);
        List<Teams> teams = teamService.getTeamByUser();

        return new Object[] { tournaments, teams, check };
    }
    
    public Object[] getTournamentById(String tmId) throws Exception {
        Tournaments tournaments = getByID(tmId);
        
        Account currentUser = userService.getCurrentUser();
        
        boolean isTeamLead = false;
        for(TournamentTeams tournamentTeams : tournaments.getTournamentTeams()) {
        	if(tournamentTeams.getTeams().getAccount().getUsername().equals(currentUser.getUsername())) {
        		isTeamLead = true;
        	}
        }
        
        TournamentTeams tournamentTeams = tournamentTeamDAO.checkTeamExistInTournament(tmId, currentUser);

        return new Object[] { tournaments, isTeamLead, tournamentTeams};
    }
    
    public List<Tournaments> getAllTournament() {
    	List<Tournaments> rs = tournamentRepository.findAll();
    	
    	return rs;
    }
    
    /**
     * Lấy thông tin khi khởi tạo màn hình eventAndTournament
     * @return
     */
    public List<District> getDistricts() {
    	List<District> districts = districtReponsitory.getAll();
    	
    	return districts;
    }
    
    public List<GetTournamentsByFilterRespDTO> getTournamentFilter(String location, String conditions, int page) {
    	List<GetTournamentsByFilterRespDTO> rs = tournamentsDao.getTournamentsByFilter(location, conditions, page);
    	
    	return rs;
    }
    
    public List<GetTournamentUserJoinDTO> getTournamentUserJoin(String id) {
        return tournamentRepository.getTournamentUserJoin(id);
    }
    
    public void cancelTournament(String tmtId) {
    	TournamentTeams tournamentTeams = tournamentTeamRepository.findById(tmtId).get();
    	if(TOURNAMENT_TEAM_STATUS_REJECT.equals(tournamentTeams.getStatus())) {
    		throw new BussinessException("Người tổ chức giải đấu đã từ chối tham gia của bạn");
    	}
    	
    	if(!tournamentTeams.getTournaments().isFlag()) {
    		throw new BussinessException("Người tổ chức giải đấu đã xóa bỏ giải đấu này");
    	}
    	
    	tournamentTeams.setStatus(TOURNAMENT_TEAM_STATUS_CANCEL);
        tournamentTeamRepository.save(tournamentTeams);
        
        Notification notification = new Notification();
        
        StringBuilder note = new StringBuilder();
        note.append(tournamentTeams.getTournaments().getiDTournaments());
        note.append("-");
        note.append("Đội ");
        note.append(tournamentTeams.getTeams().getNameTeam());
        note.append(" ");
        note.append("đã hủy tham gia giải đấu");
        note.append(" ");
        note.append(tournamentTeams.getTournaments().getNameTouraments());
        
        notification.setNote(note.toString());
        notification.setActive(true);
        notification.setDate(LocalDate.now());
        notification.setFlag(true);
        notification.setReceive(tournamentTeams.getTournaments().getAccount());
        notification.setSender(tournamentTeams.getTeams().getAccount());
        notification.setStatus(6);
        
        Notification notificationSaved = notificationService.save(notification);
		String destination = "/topic/" + "owner" + "notification";
		messagingTemplate.convertAndSend(destination, notificationSaved);
    }
    
    public List<ScheduleOfUserDTO> getScheduleOfUser(String tmId) {
    	Account currentAccount = userService.getCurrentUser();
    	return tournamentScheduleDao.getScheduleOfUser(tmId, currentAccount);
    }
    
    public List<GetTeamJoinUserDTO> getTeamJoin(String tmId) {
    	Account currentAccount = userService.getCurrentUser();
    	return tournamentScheduleDao.getTeamJoin(tmId, currentAccount);
    }
}
