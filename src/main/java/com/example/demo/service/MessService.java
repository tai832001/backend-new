package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.MessReponsitory;
import com.example.demo.entity.Message;
import com.example.demo.entity.ResponseMess;

@Service
@Transactional
public class MessService {
	@Autowired
	private MessReponsitory messReponsitory;
	
	public Message addMess(Message message) {
		return messReponsitory.save(message);
	}
	
	public List<Message> getMessByConversation(String id, int pageNumber){
		int pageSize = 20;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return messReponsitory.findMessByConverIDPage(pageable, id).getContent();
	}
	
	public int numberOfPage(String id){
		int pageNumber = 0;
		int pageSize = 20;
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		return messReponsitory.findMessByConverIDPage(pageable, id).getTotalPages();
	}
	
	public List<ResponseMess> listMess(List<Message> list){
		List<ResponseMess> listres = new ArrayList<>();
		for (Message message : list) {
			listres.add(new ResponseMess(message.getSender().getUsername(), message.getContent()));
		}
		return listres;
	}
}
