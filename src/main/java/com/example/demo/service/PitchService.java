package com.example.demo.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.IPitchAdminReponsitory;
import com.example.demo.dao.PitchDAO;
import com.example.demo.dao.PitchRepository;
import com.example.demo.dto.PitchAdminDTO;
import com.example.demo.dto.PitchDTO;
import com.example.demo.dto.PitchExcelDTO;
import com.example.demo.dto.RecentOrderDTO;
import com.example.demo.dto.RevenueStatisticsPitchDTO;
import com.example.demo.dto.RevenueStatisticsPitchDTO1;
import com.example.demo.dto.RevenueStatisticsPitchDTOMapping;
import com.example.demo.dto.StatisticBasicDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Pitch;

@Service
public class PitchService {
	@Autowired
	private PitchRepository pitchRepository;

	@Autowired
	IPitchAdminReponsitory pitchAdminReponsitory;

	@Autowired
	private PitchDAO pitchDAO;

	@Autowired
	UserService userService;

	public List<Pitch> getPitchByDistrict(String id) {
		return pitchRepository.findByIDDistrict(id);

	}

	public Pitch Save(Pitch pitch) {
		return pitchRepository.save(pitch);
	}

	public List<Pitch> getPitchByCurentUserId(String id) {
		return pitchRepository.getPitchByCurentUserId(id);

	}

	public List<RevenueStatisticsPitchDTO> revenueStatisticsPitch(int year) {

		Account currentAccount = userService.getCurrentUser();

		List<RevenueStatisticsPitchDTOMapping> revenueStatisticsPitchDao = pitchDAO
				.revenueStatisticsPitch(currentAccount, year);

		Map<String, RevenueStatisticsPitchDTO> map = new HashMap<>();

		for (RevenueStatisticsPitchDTOMapping revenueStatisticsPitchDTOMapping : revenueStatisticsPitchDao) {

			RevenueStatisticsPitchDTO1 revenueStatisticsPitchDTO1 = new RevenueStatisticsPitchDTO1();
			revenueStatisticsPitchDTO1.setDateOrder(revenueStatisticsPitchDTOMapping.getDateOrder());
			revenueStatisticsPitchDTO1.setTotalPrice(revenueStatisticsPitchDTOMapping.getTotalPrice());
			revenueStatisticsPitchDTO1.setNumOrder(revenueStatisticsPitchDTOMapping.getNumOrder());

			if (!map.containsKey(revenueStatisticsPitchDTOMapping.getiDPitch())) {
				map.put(revenueStatisticsPitchDTOMapping.getiDPitch(), new RevenueStatisticsPitchDTO());
				map.get(revenueStatisticsPitchDTOMapping.getiDPitch())
						.setPitchName(revenueStatisticsPitchDTOMapping.getPitchName());
				map.get(revenueStatisticsPitchDTOMapping.getiDPitch())
						.setiDPitch(revenueStatisticsPitchDTOMapping.getiDPitch());
			}

			List<RevenueStatisticsPitchDTO1> listDateAndSum = map.get(revenueStatisticsPitchDTOMapping.getiDPitch())
					.getListDateAndSum();
			listDateAndSum.add(revenueStatisticsPitchDTO1);

			map.get(revenueStatisticsPitchDTOMapping.getiDPitch()).setListDateAndSum(listDateAndSum);
		}

		List<RevenueStatisticsPitchDTO> rs = new ArrayList<>(map.values());

		return rs;
	}

	public List<RecentOrderDTO> getRecentOrder(int page) {
		Account currentAccount = userService.getCurrentUser();
		List<RecentOrderDTO> recentOrder = pitchDAO.getRecentOrder(page, currentAccount);

		return recentOrder;
	}

	public List<PitchDTO> getAllPitch() {
		List<Pitch> ls = pitchRepository.findAll();

		List<PitchDTO> lsDto = new ArrayList<>();

		for (Pitch pitch : ls) {
			lsDto.add(new PitchDTO(pitch.getiDPitch(), pitch.getName(), pitch.getAddress(), false, pitch.getDistrict(),pitch.getPitchImage(),
					pitch.getAccount(), pitch.getListPitchDetail()));
			System.out.println(pitch);
		}
		return lsDto;
	}

	public List<PitchDTO> findPitchByDistrict(String district) {
		List<Pitch> ls = pitchRepository.findDistinctByDistrictId(district);
		List<PitchDTO> lsDto = new ArrayList<>();
		for (Pitch pitch : ls) {
			lsDto.add(new PitchDTO(pitch.getiDPitch(), pitch.getName(), pitch.getAddress(), false, pitch.getDistrict(),pitch.getPitchImage(),
					pitch.getAccount(), pitch.getListPitchDetail()));
		}
		return lsDto;
	}

	public StatisticBasicDTO getStatisticBasic() {
		Account currentAccount = userService.getCurrentUser();
		Double totalRevenue = pitchDAO.getTotalRevenue(currentAccount);
		Long totalOrder = pitchDAO.getTotalOrder(currentAccount);

		StatisticBasicDTO statisticBasicDTO = new StatisticBasicDTO();

		if (totalRevenue == null) {
			statisticBasicDTO.setTotalRevenue(0);
		} else {
			statisticBasicDTO.setTotalRevenue(totalRevenue);
		}

		statisticBasicDTO.setTotalOrder(totalOrder);

		return statisticBasicDTO;
	}

	public List<PitchAdminDTO> getPitchFromDistrict(String iDDistrict) {
		return pitchAdminReponsitory.getPitchFromDistrict(iDDistrict);
	}

	public void deletePitch(String iDPitch) {
		pitchRepository.deletePitch(iDPitch);
	}

	public List<PitchExcelDTO> getPitchById(String iDPitch) {
		return pitchAdminReponsitory.getPitchById(iDPitch);
	}
}
