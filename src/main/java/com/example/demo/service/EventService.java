package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.EventReponsitory;
import com.example.demo.entity.Event;
import com.example.demo.entity.Pitch;

@Service
public class EventService {
	
	@Autowired
	private EventReponsitory eventReponsitory;
	
	@Autowired
	private SendImageToAzureService sendImageToAzureService;
	
	public Event save(Event event,MultipartFile file) {
		event.setImage(sendImageToAzureService.sendImageToFireBas(file));
		return eventReponsitory.save(event);
	}
	
	public List<Event> getAll() {
		return eventReponsitory.getAllByTrue();

	}
	
	public List<Event> getOuststanding() {
		return eventReponsitory.getOutstanding();

	}
	
	public void deleteEvent(String id) {
		eventReponsitory.deleteEvent(id);
	}
	
	public Optional<Event> findById(String id) {	
		return eventReponsitory.findById(id);
	}
	
	public void updateEvent(Event event, MultipartFile file,String image) {
		if(file == null) {
			event.setImage(image);
		}else {			
			event.setImage(sendImageToAzureService.sendImageToFireBas(file));
		}
		eventReponsitory.save(event);
	}
}
