package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.demo.dao.PitchDetailRepository;
import com.example.demo.dto.PitchDetailDTO;
import com.example.demo.dao.PitchDetailDao;
import com.example.demo.entity.PitchDetail;

import jakarta.persistence.EntityManager;

import com.example.demo.entity.Account;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PitchDetailService {

	@Autowired
	PitchDetailRepository pitchDetailRepository;

	@Autowired
	PitchDetailDao pitchDetailDao;

	@Autowired
	UserService userService;
	@Autowired
	EntityManager entityManager;

	public PitchDetail save(PitchDetail pitchDetail) {
		return pitchDetailRepository.save(pitchDetail);
	}

	public List<PitchDetail> getPitchDetailByPitchId(String id) {
		return pitchDetailRepository.getPitchDetailByPitchId(id);
	}

	public List<PitchDetail> getByUser() {
		Account currentUser = userService.getCurrentUser();
		List<PitchDetail> rs = pitchDetailDao.getPitchDetailsByUser(currentUser);

		return rs;
	}

	public Map<String, Object> getPagePitchDetailByPitchId(String id, String search, int page, int size) {
		Map<String, Object> result = new HashMap<>();
		Pageable pageable = PageRequest.of(page, size);
		Page<PitchDetail> pitchDetails = pitchDetailRepository.getPagePitchDetailByPitchId(id, search, pageable);
		result.put("pitchdetails", pitchDetails);
		result.put("maxpage", pitchDetails.getTotalPages());
		return result;

	}

	public PitchDetail getPitchDetailByID(String id) {
		return pitchDetailRepository.getPitchDetailByPitchDetail(id);
	}

	public List<PitchDetailDTO> getPitchDetailByPitchIdDTO(String id) {
		List<PitchDetail> ls = pitchDetailRepository.getPitchDetailByPitchId(id);
		List<PitchDetailDTO> lsDTO = new ArrayList<>();
		for (PitchDetail pitchDetail : ls) {
			lsDTO.add(new PitchDetailDTO(pitchDetail.getiDPitchDetail(), pitchDetail.getNamePitch(), false,
					pitchDetail.getPitch(), pitchDetail.getPitchType(), pitchDetail.getListPrice()));
		}
		return lsDTO;
	}

	public List<PitchDetail> getByIDPD() {
		return pitchDetailRepository.getPitchDetailByPitchId("PI1");
	}

	public List<PitchDetailDTO> getPitchDetailByPitchIdDTODistinct(String id) {
		List<PitchDetail> ls = pitchDetailRepository.getPitchDetailByPitchIdDistinct(id);
		List<PitchDetailDTO> lsDTO = new ArrayList<>();
		for (PitchDetail pitchDetail : ls) {
			lsDTO.add(new PitchDetailDTO(pitchDetail.getiDPitchDetail(), pitchDetail.getNamePitch(), false,
					pitchDetail.getPitch(), pitchDetail.getPitchType(), pitchDetail.getListPrice()));
		}

		for (PitchDetailDTO pitchDetailDTO : lsDTO) {
			pitchDetailDTO.getListPrice().sort(Comparator.comparing(price -> price.getTimeRange().getTimeFrom()));
		}

		return lsDTO;
	}

	public List<PitchDetail> getPitchDetailByPitchIdDistinct(String id) {
		List<PitchDetail> ls = pitchDetailRepository.getPitchDetailByPitchIdDistinct(id);
		return ls;
	}

	public List<PitchDetail> getByTournamentId(String tournamentId) {
		List<PitchDetail> rs = pitchDetailRepository.getByTournamentId(tournamentId);

		return rs;
	}
}
