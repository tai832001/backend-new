package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.FriendReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.entity.Account;
import com.example.demo.entity.Friend;

@Service
@Transactional
public class FriendService {
	
	@Autowired
	private FriendReponsitory friendReponsitory;
	
	@Autowired
	private UserReponsitory userReponsitory;
	
	public List<Account> findFriend(Account id){
		List<Account> list = new ArrayList<>();
		List<Friend> listFR = friendReponsitory.findByUser(id);
		for (Friend friend : listFR) {
			list.add(friend.getFriend());
		}
		return list;
	}
	
	public boolean checkFriend(Account id , String username) {
		List<String> list = friendReponsitory.findFriendByUser(id.getUsername());
		for (String string : list) {
			Account account = userReponsitory.findById(string).get();
			if (username.equals(account.getUsername())) {
				return true;
			}
		}
		return false;
	}
	public Friend addFriend(Friend friend) {
		return friendReponsitory.save(friend);
	}
	
	public List<Account> findFriendByUser(String username){
		List<String> list = friendReponsitory.findFriendByUser(username);
		List<Account> listAccount = new ArrayList<>();
		for (String string : list) {
			listAccount.add(userReponsitory.findById(string).get());
		}
		return listAccount;
	}
	
}
