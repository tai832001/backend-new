package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demo.dao.PartiReponsitory;
import com.example.demo.dao.UserReponsitory;
import com.example.demo.dto.MessDTO;
import com.example.demo.dto.PartyDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.Parti;

@Service
@Transactional
public class PartiServiceImp{
	
	@Autowired
	private PartiReponsitory partiReponsitory;
	
	@Autowired
	private UserReponsitory userReponsitory;

	public List<Parti> listParty(String id) {
		// TODO Auto-generated method stub
		return partiReponsitory.findByAccount(id);
	}

	public List<Parti> listUserInConver(String id){
		return partiReponsitory.findByConver(id);
	}
	
	public Parti saveParty(Parti parti) {
		return partiReponsitory.save(parti);
	}
	
	public List<MessDTO> listMess(String id) {
		return partiReponsitory.listMess(id);
	}
	
	public int findConverIdByParty(PartyDTO partyDTO) {
		Account user = userReponsitory.findByUsername(partyDTO.getUser()).get();
		Account friend = userReponsitory.findByUsername(partyDTO.getFriend()).get();
		try {
			System.out.println(user.getiDAccount()+ friend.getiDAccount());
			System.out.println(partiReponsitory.findConverByParty(user.getiDAccount(), friend.getiDAccount()));
			return partiReponsitory.findConverByParty(user.getiDAccount(), friend.getiDAccount());
		} catch (Exception e) {
			return -1;
		}
		
	}
}
