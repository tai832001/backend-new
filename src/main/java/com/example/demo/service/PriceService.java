package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.customID.GenerateCustomId;
import com.example.demo.dao.PriceRepository;
import com.example.demo.entity.Price;

@Service
public class PriceService {
	@Autowired
	PriceRepository priceRepository;

	@Autowired
	UserService userService;

	public List<Price> saveAll(List<Price> listPrice) {
		for (Price price : listPrice) {
			priceRepository.save(price);
		}
		return listPrice;
	}

	public List<Price> getByAccountID() {
		return priceRepository.getByAccountID(userService.getCurrentUser().getiDAccount());
	}

	public List<Price> getByPitchAndPD(String idpd, String idp) {
		return priceRepository.getByPitch(idpd, idp);
	}

	public List<Price> getByPitchDetailID(String id) {
		return priceRepository.getByPitchDetailID(id);
	}

	public Price getByID(String id) {
		return priceRepository.findById(id).get();
	}
}
