package com.example.demo.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.dao.ManageFieldDao;
import com.example.demo.dao.OrderPitchRepository;
import com.example.demo.dao.OrderReponsitory;
import com.example.demo.dto.OrderPitchDTO;
import com.example.demo.dto.OrderWithPriceDTO;
import com.example.demo.entity.Account;
import com.example.demo.entity.OrderPitch;
import com.example.demo.entity.OrderPrice;
import com.example.demo.entity.Price;

@Service
@Transactional
public class OrderPitchService {

	@Autowired
	private OrderReponsitory orderReponsitory;

	@Autowired
	ManageFieldDao manageFieldDao;

	@Autowired
	OrderPitchRepository orderPitchRepository;

	public List<OrderPitchDTO> getByUser(Account account) {

		List<OrderPitchDTO> orderPitchDTOs = manageFieldDao.getByUser(account);
		return orderPitchDTOs;
	}

	public OrderPitch save(OrderPitch orderPitch) {
		return orderReponsitory.save(orderPitch);
	}

	public OrderPitch find(String id) {
		return orderReponsitory.findById(id).get();
	}

	public OrderWithPriceDTO findDto(String id) {
		OrderPitch orderPitch = orderReponsitory.findById(id).get();
		return new OrderWithPriceDTO(orderPitch.getiDOrder(), orderPitch.getiDAccount(), orderPitch.getiDPrice(),
				orderPitch.getDeposit(), orderPitch.getDateStart(), orderPitch.getDateOrder(), orderPitch.getStatus(),
				orderPitch.isFlag(), orderPitch.getOrderPrices(), orderPitch.getAccount());
	}

	public List<OrderWithPriceDTO> listOrder() {
		List<OrderWithPriceDTO> list2 = new ArrayList<OrderWithPriceDTO>();
		List<OrderPitch> list = orderReponsitory.listOrder();
		for (OrderPitch orderPitch : list) {
			list2.add(new OrderWithPriceDTO(orderPitch.getiDOrder(), orderPitch.getiDAccount(), orderPitch.getiDPrice(),
					orderPitch.getDeposit(), orderPitch.getDateStart(), orderPitch.getDateOrder(),
					orderPitch.getStatus(), true, orderPitch.getOrderPrices(), orderPitch.getAccount()));
		}
		return list2;
	}

	public Map<String, Object> getPageOrderPitchByOwnerID(String id, String pid, int page) {
		Map<String, Object> result = new HashMap<>();
		Pageable pageable = PageRequest.of(page, 5);
		Page<OrderPitch> OrderPitchs = orderPitchRepository.getPageOrderPitchByOwnerID(id, pid, pageable);
		List<OrderWithPriceDTO> list2 = new ArrayList<OrderWithPriceDTO>();
		for (OrderPitch orderPitch : OrderPitchs.getContent()) {
			list2.add(new OrderWithPriceDTO(orderPitch.getiDOrder(), orderPitch.getiDAccount(), orderPitch.getiDPrice(),
					orderPitch.getDeposit(), orderPitch.getDateStart(), orderPitch.getDateOrder(),
					orderPitch.getStatus(), true, orderPitch.getOrderPrices(), orderPitch.getAccount()));
		}
		result.put("orderpitchs", list2);
		result.put("maxpage", OrderPitchs.getTotalPages());
		return result;

	}

	public String getNamePitchById(String id) {
		OrderPitch orderPitch = orderPitchRepository.findById(id).get();
		return orderPitch.getOrderPrices().get(0).getPrice().getPitchDetail().getPitch().getName();
	}

	public boolean checkOrderPitch(Price listprice, LocalDate date) {
		List<OrderPitch> list = orderPitchRepository.findOrderPitch(listprice, date);
		if (list.size()!=0) {
			return false;
		}else {
			return true;
		}
		
	}
	public List<OrderWithPriceDTO> findHistory(String id) {
		List<OrderWithPriceDTO> list2 = new ArrayList<OrderWithPriceDTO>();
		List<OrderPitch> list = orderPitchRepository.findHistory(id);
		for (OrderPitch orderPitch : list) {
			list2.add(new OrderWithPriceDTO(orderPitch.getiDOrder(), orderPitch.getiDAccount(), orderPitch.getiDPrice(),
					orderPitch.getDeposit(), orderPitch.getDateStart(), orderPitch.getDateOrder(),
					orderPitch.getStatus(), true, orderPitch.getOrderPrices(), orderPitch.getAccount()));
		}
		return list2;
	}
}
