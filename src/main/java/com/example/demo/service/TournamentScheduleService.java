package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.dao.PitchDetailRepository;
import com.example.demo.dao.TeamReponsitory;
import com.example.demo.dao.TournamentRepository;
import com.example.demo.dao.TournamentScheduleDao;
import com.example.demo.dao.TournamentScheduleRepo;
import com.example.demo.dto.ListScheduleBasicDTO;
import com.example.demo.entity.PitchDetail;
import com.example.demo.entity.Teams;
import com.example.demo.entity.TournamentSchedule;
import com.example.demo.entity.Tournaments;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class TournamentScheduleService {
	
	@Autowired
	TournamentScheduleDao tournamentScheduleDao;
	
	@Autowired
	TournamentRepository tournamentRepository;
	
	@Autowired
	TeamReponsitory teamReponsitory;
	
	@Autowired
	PitchDetailRepository pitchDetailRepository;
	
	@Autowired
	TournamentScheduleRepo tournamentScheduleRepo;
	
	public TournamentSchedule save(TournamentSchedule tournamentSchedule) {
		Tournaments tournaments = tournamentRepository.findById(tournamentSchedule.getIdTournaments()).get();
		Teams team1 = teamReponsitory.findById(tournamentSchedule.getIdTeam1()).get();
		Teams team2 = teamReponsitory.findById(tournamentSchedule.getIdTeam2()).get();
		PitchDetail pitchDetail = pitchDetailRepository.findById(tournamentSchedule.getIdPitchDetail()).get();
		
		tournamentSchedule.setFlag(true);
		tournamentSchedule.setTournaments(tournaments);
		tournamentSchedule.setTeam1(team1);
		tournamentSchedule.setTeam2(team2);
		tournamentSchedule.setPitchDetail(pitchDetail);
		
		TournamentSchedule saved = tournamentScheduleRepo.save(tournamentSchedule);
		
		return saved;
	}
	
	public void deleteTournamentScheduleById(String tmsId) {
		tournamentScheduleRepo.deleteTournamentScheduleById(tmsId);
	}
	
	public ListScheduleBasicDTO getTournamentScheduleByMatchId(String matchId) {
		return tournamentScheduleDao.getTournamentScheduleByMatchId(matchId);
	}
}
